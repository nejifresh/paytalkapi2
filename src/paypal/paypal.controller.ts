import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Req,
  Res,
  UseFilters,
  UsePipes,
} from '@nestjs/common';
import { PaypalService } from './paypal.service';
import { CreatePaypalOrderDto } from './dto/create-paypal-order.dto';
import { Request, response, Response } from 'express';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { HttpExceptionFilter } from '../commons/filters/http-exception.filter';
import { logger, adminTypes } from '../commons/firebaseConfig';
import { PaymentSaleResponse } from './interfaces/payment-sale-response.interface';
import {
  PaypalBatchPayoutsEvent,
  PaypalSaleEvent,
} from './enums/webhook-events.enum';
import { PaypalUtils } from './utils/paypal-utils.class';
import { ResourceType } from './enums/resource-types.enum';
import { BatchPayoutResponse } from './interfaces/batch-payout-response.interface';
import { BraintreeService } from './braintree.service';
import { ClientToken, Transaction, ValidatedResponse } from 'braintree';
import { BraintreeCheckoutDto } from './dto/braintree-checkout.dto';
import { CommonUtils } from '../commons/utils/common-utils.class';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../auth/auth-guard.service';
import { SendGridService } from '@anchan828/nest-sendgrid';

@Controller('paypal')
@ApiBearerAuth()
@ApiTags('Paypal')
export class PaypalController {
  paymentReceiverEmail: string;
  paymentReceiverId: string;
  paymentSenderEmail: string;
  paymentSenderId: string;
  tCampaignId: string;
  tOrganizationId: string;
  savedTransactionRef: string;

  constructor(
    private readonly paypalService: PaypalService,
    private readonly braintreeService: BraintreeService,
    private readonly sendGrid: SendGridService,
  ) {}

  @Post('order')
  @ApiOperation({ summary: 'Create Order' })
  @UsePipes(DTOValidationPipe)
  create(
    @Body() createPaypalDto: CreatePaypalOrderDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.paypalService.create(createPaypalDto, response);
  }

  /**
   *
   * @param req: Braintree Customer ID
   * @returns Braintree Client Token
   */
  @Post('client-token')
  @UseGuards(AuthGuard)
  getClientToken(@Req() req: Request): Promise<ValidatedResponse<ClientToken>> {
    const { customerId } = req.body;

    if (customerId && customerId.length > 0) {
      return this.braintreeService.generateClientToken(customerId);
    } else {
      return this.braintreeService.generateClientToken();
    }
  }

  /**
   * BRAINTREE CHECKOUT
   * @param req: Express Request Object
   * @returns Braintree validated transaction promise
   */
  @Post('checkout')
  @UseGuards(AuthGuard)
  async checkout(
    @Body() payload: BraintreeCheckoutDto,
  ): Promise<FirebaseFirestore.WriteResult> {
    const {
      currency,
      receiver,
      sender,
      paymentMethodNonce,
      amount,
      deviceData,
      campaignId,
      organizationId,
    } = payload;

    const amountAsString = CommonUtils.numToString(amount);

    this.paymentReceiverEmail = receiver['email'];
    this.paymentReceiverId = receiver['uid'];
    this.paymentSenderEmail = sender['email'];
    this.paymentSenderId = sender['uid'];
    this.tCampaignId = campaignId;
    this.tOrganizationId = organizationId;
    const beneficiary = {
      name: null,
      profileId: this.paymentReceiverId,
    };
    const gateway = 'paypal';
    const status = 'pending';
    const type = 'donation';
    const campaign = { title: 'Test Campaign', uid: this.tCampaignId }; //TODO Remove hard-coded value
    const transactionTimeStamp = adminTypes.firestore.Timestamp.now();

    logger.info(`sender => ${this.paymentSenderEmail}`);
    logger.info(`receiver => ${this.paymentReceiverEmail}`);
    logger.info(`senderUid => ${this.paymentSenderId}`);
    logger.info(`receiverUid => ${this.paymentReceiverId}`);
    logger.info(`campaignId => ${this.tCampaignId}`);
    logger.info(`organizationId => ${this.tOrganizationId}`);

    return this.braintreeService
      .initiateTransaction(amountAsString, paymentMethodNonce, deviceData)
      .then(btResp => {
        logger.info(btResp.transaction.paypalAccount);

        return this.paypalService
          .saveTransaction(
            amountAsString,
            beneficiary,
            this.paymentSenderEmail,
            this.paymentSenderId,
            currency,
            gateway,
            btResp.transaction.paypalAccount.authorizationId,
            transactionTimeStamp,
            null,
            status,
            type,
            this.tOrganizationId,
            campaign,
          )
          .catch(e => {
            logger.error(e);
            throw new HttpException(
              'An error occured!',
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          });
      });
  }

  /**
   *  PAYPAL WEBHOOK
   * @param req
   * @param res
   * @returns
   */
  @Post('webhook')
  async webhook(@Req() req: Request, @Res() res: Response) {
    const { resource_type } = req.body;

    if (resource_type === ResourceType.SALE) {
      const resp = req.body as PaymentSaleResponse;
      const curr = resp.resource.amount.currency;
      const amountPaid = resp.resource.amount.total;
      const paypalCharges = resp.resource.transaction_fee.value;
      const amountCredited =
        CommonUtils.stringToNum(amountPaid) -
        CommonUtils.stringToNum(paypalCharges);
      const payoutAmount = PaypalUtils.calculatePayout(amountCredited);
      const transactionId = resp.resource.id;

      logger.info(resp);
      logger.info(`currency => ${curr}`);
      logger.info(`amountPaid => ${amountPaid}`);
      logger.info(`paypalCharges => ${paypalCharges}`);
      logger.info(`amountCredited => ${amountCredited}`);
      logger.info(`payoutAmount => ${payoutAmount}`);
      logger.info(
        `saved transaction uid inside webhook handler => ${this.savedTransactionRef}`,
      );

      if (resp.event_type === PaypalSaleEvent.completed) {
        // Update transaction status
        this.paypalService
          .updateTransactionStatus(transactionId)
          .then(res => logger.info(res))
          .catch(e => logger.error(e));

        // Send out payout
        return this.paypalService
          .payout(
            this.paymentSenderEmail,
            this.paymentReceiverEmail,
            payoutAmount,
            curr,
          )
          .then(resp => {
            return res.status(HttpStatus.OK).json(resp);
          })
          .catch(e => {
            logger.error(e);
            throw new HttpException(e.message, e.statusCode);
          });
      } else if (resp.event_type === PaypalSaleEvent.denied) {
        return res.status(HttpStatus.OK).json(resp.summary);
      }
    } else if (resource_type === ResourceType.PAYOUTS) {
      const resp = req.body as BatchPayoutResponse;

      logger.info(resp);

      if (resp.event_type === PaypalBatchPayoutsEvent.success) {
        const resp = req.body as BatchPayoutResponse;
        const amountSent = resp.resource.batch_header.amount.value;

        this.sendGrid.send({
          to: this.paymentReceiverEmail,
          from: 'info@paytalk.ng',
          subject: 'PayTalk: You have just received money',
          html: `You have just received ${amountSent} from ${this.paymentSenderEmail}`,
        });

        return res.status(HttpStatus.OK).json('Payout Success!!');
      }
    }
  }

  /**
   * BRAINTREE WEBHOOK
   * @param req
   * @param res
   * @returns
   */
  @Post('braintree/webhook')
  braintreeWebhook(@Req() req: Request, @Res() res: Response) {
    logger.info(req.body);
    return res.status(HttpStatus.OK).json('Event Received!');
  }
}
