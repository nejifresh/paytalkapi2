import { Module } from '@nestjs/common';
import { PaypalService } from './paypal.service';
import { PaypalController } from './paypal.controller';
import { BraintreeService } from './braintree.service';
import { AuthModule } from '../auth/auth.module';

@Module({
  controllers: [PaypalController],
  providers: [PaypalService, BraintreeService],
  imports: [AuthModule],
})
export class PaypalModule {}
