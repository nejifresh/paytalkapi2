export interface PaymentSaleResponse {
  resource_type: string;
  id: string;
  create_time: string;
  event_version: string;
  summary: string;
  event_type: string;
  resource: Resource;
  links: Link[];
}

interface Resource {
  application_context: Applicationcontext;
  state: string;
  create_time: string;
  transaction_fee: Transactionfee;
  update_time: string;
  protection_eligibility: string;
  parent_payment: string;
  id: string;
  invoice_number: string;
  protection_eligibility_type: string;
  payment_mode: string;
  links: Link[];
  amount: Amount;
}

interface Amount {
  currency: string;
  total: string;
  details: Details;
}

interface Details {
  subtotal: string;
}

interface Link {
  method: string;
  rel: string;
  href: string;
}

interface Transactionfee {
  value: string;
  currency: string;
}

interface Applicationcontext {
  related_qualifiers: Relatedqualifier[];
}

interface Relatedqualifier {
  id: string;
  type: string;
}
