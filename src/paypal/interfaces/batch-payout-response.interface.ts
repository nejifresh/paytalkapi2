export interface BatchPayoutResponse {
  summary: string;
  resource: Resource;
  id: string;
  links: Link2[];
  event_type: string;
  event_version: string;
  resource_type: string;
  create_time: string;
}

interface Link2 {
  rel: string;
  method: string;
  href: string;
}

interface Resource {
  batch_header: Batchheader;
  links: Link[];
}

interface Link {
  href: string;
  encType: string;
  rel: string;
  method: string;
}

interface Batchheader {
  funding_source: string;
  batch_status: string;
  time_closed: string;
  payout_batch_id: string;
  time_created: string;
  amount: Amount;
  sender_batch_header: Senderbatchheader;
  fees: Amount;
  time_completed: string;
  payments: number;
}

interface Senderbatchheader {
  sender_batch_id: string;
}

interface Amount {
  currency: string;
  value: string;
}
