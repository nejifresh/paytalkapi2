import { Injectable } from '@nestjs/common';
import { braintreeGateway } from '../commons/braintree.config';
import {
  ValidatedResponse,
  ClientToken,
  TransactionRequest,
  Transaction,
} from 'braintree';

@Injectable()
export class BraintreeService {
  generateClientToken(
    customerId?: string,
  ): Promise<ValidatedResponse<ClientToken>> {
    if (customerId && customerId.length > 0) {
      return braintreeGateway.clientToken.generate({ customerId });
    } else {
      return braintreeGateway.clientToken.generate({});
    }
  }

  initiateTransaction(
    amount: string,
    paymentMethodNonce: any,
    deviceData: any,
  ): Promise<ValidatedResponse<Transaction>> {
    const transactionRequest: TransactionRequest = {
      amount,
      paymentMethodNonce,
      deviceData,
      options: {
        submitForSettlement: true,
      },
    };
    return braintreeGateway.transaction.sale(transactionRequest);
  }
}
