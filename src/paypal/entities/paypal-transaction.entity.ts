import firebase from 'firebase';
// import Timestamp = firebase.firestore.Timestamp;
export class PaypalTransaction {
  constructor(
    public amount: number,
    public beneficiary: PaypalBeneficiary,
    public senderEmail: string,
    public senderUid: string,
    public currency: string,
    public gateway: string,
    public transactionId: string,
    public dateOfTransaction: any,
    public imageUrl: string,
    public status: string,
    public type: string,
    public organizationId: string,
    public campaign?: PaypalCampaign,
  ) {}
}

export interface PaypalBeneficiary {
  name: string;
  profileId: string;
}

export interface PaypalCampaign {
  title: string;
  uid: string;
}
