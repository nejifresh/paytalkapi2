import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class WebhookGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const headers = request.headers;
    console.log(headers);

    const certURL = headers['paypal-cert-url'];
    const transmissionId = headers['paypal-transmission-id'];
    const transmissionSignature = headers['paypal-transmission-sig'];
    const transmissionTimestamp = headers['paypal-transmission-time'];

    const requestHeaders = {
      'paypal-auth-algo': 'SHA256withRSA',
      'paypal-cert-url': certURL,
      'paypal-transmission-id': transmissionId,
      'paypal-transmission-sig': transmissionSignature,
      'paypal-transmission-time': transmissionTimestamp,
    };

    const body = request.body;


    return true;
  }
}
