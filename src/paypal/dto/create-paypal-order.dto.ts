import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class CreatePaypalOrderDto {

  @ApiProperty()
  @IsDefined()
  currencyCode: string;

  @ApiProperty()
  @IsDefined()
  value: number;

}
