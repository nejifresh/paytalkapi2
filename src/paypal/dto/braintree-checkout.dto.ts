import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class BraintreeCheckoutDto {
  @ApiProperty()
  @IsDefined()
  sender: Sender;

  @ApiProperty()
  @IsDefined()
  currency: string;

  @ApiProperty()
  @IsDefined()
  receiver: Receiver;

  @ApiProperty()
  @IsDefined()
  paymentMethodNonce: string;

  @ApiProperty()
  @IsDefined()
  deviceData: any;

  @ApiProperty()
  @IsDefined()
  amount: number;

  @ApiProperty()
  @IsDefined()
  campaignId: string;

  @ApiProperty()
  @IsDefined()
  organizationId: string;
}

export interface TransactionIndividual {
  email: string;
  uid: string;
}

type Sender = TransactionIndividual;
type Receiver = TransactionIndividual;
