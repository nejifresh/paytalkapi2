import { PartialType } from '@nestjs/mapped-types';
import { CreatePaypalOrderDto } from './create-paypal-order.dto';

export class UpdatePaypalDto extends PartialType(CreatePaypalOrderDto) {
}
