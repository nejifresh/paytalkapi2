export const enum PaypalSaleEvent {
  completed = 'PAYMENT.SALE.COMPLETED',
  denied = 'PAYMENT.SALE.DENIED',
  pending = 'PAYMENT.SALE.PENDING',
  refunded = 'PAYMENT.SALE.REFUNDED',
  reversed = 'PAYMENT.SALE.REVERSED',
}

export const enum PaypalPaymentOrderEvent {
  cancelled = 'PAYMENT.ORDER.CANCELLED',
  created = 'PAYMENT.ORDER.CREATED',
}

export const enum PaypalBatchPayoutsEvent {
  success = 'PAYMENT.PAYOUTSBATCH.SUCCESS',
  processing = 'PAYMENT.PAYOUTSBATCH.PROCESSING',
  denied = 'PAYMENT.PAYOUTSBATCH.DENIED',
  cancelled = 'PAYMENT.PAYOUTSBATCH.CANCELED',
}
