import { HttpStatus, Injectable, Res } from '@nestjs/common';
import { CreatePaypalOrderDto } from './dto/create-paypal-order.dto';
import { Response } from 'express';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { firebaseAdmin, logger } from '../commons/firebaseConfig';
import {
  PaypalTransaction,
  PaypalBeneficiary,
} from './entities/paypal-transaction.entity';
import { braintreeGateway } from '../commons/braintree.config';
import { PaypalCampaign } from './entities/paypal-transaction.entity';
import { Organization } from '../commons/interfaces/organization.interface';
import {
  paypalClient,
  paypalSDK,
  paypalPayOutSdk,
} from '../commons/paypal.config';
import {
  ValidatedResponse,
  ClientToken,
  TransactionRequest,
  Transaction,
} from 'braintree';

@Injectable()
export class PaypalService {
  async create(
    createPaypalDto: CreatePaypalOrderDto,
    @Res() response: Response,
  ): Promise<Response> {
    const orderRequest = new paypalSDK.orders.OrdersCreateRequest();
    orderRequest.requestBody({
      intent: 'CAPTURE',
      purchase_units: [
        {
          amount: {
            currency_code: createPaypalDto.currencyCode,
            value: createPaypalDto.value,
          },
        },
      ],
    });

    const orderResponse = await paypalClient.execute(orderRequest);

    return response
      .status(HttpStatus.CREATED)
      .json(new SuccessResponseDto(orderResponse.result, HttpStatus.CREATED));
  }

  /**
   *
   * @param customerId: Braintree Customer ID
   * @returns Braintree Client Token
   */
  generateClientToken(
    customerId?: string,
  ): Promise<ValidatedResponse<ClientToken>> {
    if (customerId && customerId.length > 0) {
      return braintreeGateway.clientToken.generate({ customerId });
    } else {
      return braintreeGateway.clientToken.generate({});
    }
  }

  /**
   *
   * @param amount: Transaction Amount
   * @param paymentMethodNonce: Braintree Payment Method Nonce
   * @param deviceData: Braintree Device Data
   * @returns Braintree validated transaction response
   */
  initiateTransaction(
    amount: string,
    paymentMethodNonce: any,
    deviceData: any,
  ): Promise<ValidatedResponse<Transaction>> {
    const transactionRequest: TransactionRequest = {
      amount,
      paymentMethodNonce,
      deviceData,
      options: {
        submitForSettlement: true,
      },
    };
    return braintreeGateway.transaction.sale(transactionRequest);
  }

  /**
   *
   * @param receiverEmail: Transaction receiver email
   * @param amount: Transaction amount
   * @param currency: Transaction currency
   * @returns Any
   */
  payout(
    senderEmail: string,
    receiverEmail: string,
    amount: number,
    currency: string,
  ): Promise<any> {
    const sender_batch_id = Math.random()
      .toString(36)
      .substring(9);

    const sender_item_id = Math.random()
      .toString(36)
      .substring(9);

    const payoutRequestBody = {
      sender_batch_header: {
        recipient_type: 'EMAIL',
        email_message: `You have just been credited with ${currency}${amount} sent from ${senderEmail}`,
        note: 'Enjoy your Payout!!',
        sender_batch_id,
        email_subject:
          'Payment Notification: Your Paypal account has just been credited!',
      },
      items: [
        {
          note: `Your ${amount} Payout!`,
          amount: {
            currency,
            value: amount,
          },
          receiver: receiverEmail,
          sender_item_id,
        },
      ],
    };

    const payoutRequest = new paypalPayOutSdk.payouts.PayoutsPostRequest();
    payoutRequest.requestBody(payoutRequestBody);
    return paypalClient.execute(payoutRequest);
  }

  async saveTransaction(
    amount: string,
    beneficiary: PaypalBeneficiary,
    senderEmail: string,
    senderUid: string,
    currency: string,
    gateway: string,
    transactionId: string,
    dateOfTransaction: any,
    imageUrl: string,
    status: string,
    type: string,
    organizationId: string,
    campaign: PaypalCampaign,
  ): Promise<FirebaseFirestore.WriteResult> {
    const transactionDetails: PaypalTransaction = {
      amount: +amount,
      beneficiary,
      senderEmail,
      senderUid,
      currency,
      gateway,
      transactionId,
      dateOfTransaction,
      imageUrl,
      status,
      type,
      organizationId,
      campaign,
    };

    const org = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(transactionDetails.organizationId)
      .get();

    const orgImage = (org.data() as Organization).imageUrl;

    const orgProfile = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(transactionDetails.organizationId)
      .collection('profiles')
      .doc(transactionDetails.beneficiary.profileId)
      .get();

    const profileName = orgProfile.data().name as string;

    transactionDetails.imageUrl = orgImage;
    transactionDetails.beneficiary.name = profileName;

    return firebaseAdmin
      .firestore()
      .collection('transactions2')
      .doc(transactionDetails.transactionId)
      .set(transactionDetails);
  }

  updateTransactionStatus(
    transactionId: string,
  ): Promise<FirebaseFirestore.WriteResult> {
    const transactionDetails: Partial<PaypalTransaction> = {
      status: 'success',
    };

    return firebaseAdmin
      .firestore()
      .collection('transactions2')
      .doc(`${transactionId}`)
      .update(transactionDetails);
  }
}
