import { PayoutSplitRatio } from '../constants/transanctions.const';

export class PaypalUtils {
  static calculatePayout(amountCredited: number): number {
    const paytalkCharges = amountCredited * PayoutSplitRatio;
    return amountCredited - paytalkCharges;
  }
}
