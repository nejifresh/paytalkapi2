import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import crypto from 'crypto';

@Injectable()
export class PaystackWebhookGuardGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {

    const request = context.switchToHttp().getRequest();
    const headers = request.headers;
    const paysackHeader = headers['x-paystack-signature'];
    const body = request.body;

    const hash = crypto.createHmac('sha512', process.env.payStackTestKey)
      .update(JSON.stringify(body))
      .digest('hex');

    if (hash == paysackHeader) {
      return true;
    } else {
      throw new UnauthorizedException('Request is not from Paystack');
    }
  }
}
