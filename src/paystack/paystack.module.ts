import { HttpModule, Module } from '@nestjs/common';
import { PaystackService } from './paystack.service';
import { PaystackController } from './paystack.controller';

@Module({
  imports: [
    HttpModule.register({
      baseURL: process.env.payStackURL,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${process.env.payStackTestKey}`,
      },
    }),
  ],
  controllers: [PaystackController],
  providers: [PaystackService],
})
export class PaystackModule {
}
