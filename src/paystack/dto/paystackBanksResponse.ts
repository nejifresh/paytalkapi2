import { ApiProperty } from '@nestjs/swagger';
import { PaystackBanksDto } from './banks.dto';

export class PaystackBanksResponse {

  @ApiProperty({ type: [PaystackBanksDto] })
  data: Array<PaystackBanksDto>;
}
