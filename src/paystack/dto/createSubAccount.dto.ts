import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class CreatePaystackSubAccountDto {

  @ApiProperty()
  @IsDefined()
  organizationId: string;

  @ApiProperty()
  @IsDefined()
  profileId: string;

  @ApiProperty()
  @IsDefined()
  bankCode: number;

  @ApiProperty()
  @IsDefined()
  accountNumber: number;

  @ApiProperty()
  @IsDefined()
  name: string;
}
