import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNumber, IsOptional } from 'class-validator';

export class CreatePaystackDto {

  @IsDefined()
  @ApiProperty()
  @IsNumber()
  amount: number;

  @IsDefined()
  @ApiProperty()
  organizationId: string;

  @ApiProperty()
  @IsOptional()
  campaignId: string;

  @IsOptional()
  @ApiProperty()
  profileId: string;

  @IsDefined()
  @ApiProperty()
  transactionType: string;
}
