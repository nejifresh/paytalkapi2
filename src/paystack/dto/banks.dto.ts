import { ApiProperty } from '@nestjs/swagger';

export class PaystackBanksDto {

  @ApiProperty()
  name: string;
  @ApiProperty()
  code: number;
  @ApiProperty()
  id: number;

  constructor(bank: any) {
    this.name = bank.name;
    this.code = bank.code;
    this.id = bank.id;
  }
}
