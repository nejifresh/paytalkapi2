import { IsDefined, IsNumber, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PayAuthCodeDto {

  @IsDefined()
  @ApiProperty()
  @IsNumber()
  amount: number;

  @IsDefined()
  @ApiProperty()
  organizationId: string;

  @IsDefined()
  @IsOptional()
  @ApiProperty()
  campaignId: string;

  @IsDefined()
  @IsOptional()
  @ApiProperty()
  profileId: string;

  @IsDefined()
  @ApiProperty()
  cardId: string;

  @IsDefined()
  @ApiProperty()
  transactionType: string;

}
