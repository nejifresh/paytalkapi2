import { ApiProperty } from '@nestjs/swagger';
import { PayStackData } from './PayStack.data.dto';

export class PaystackResponseDto {

   @ApiProperty()
   status: boolean;
   @ApiProperty()
   message: string;
   @ApiProperty({ type: PayStackData })
   data: PayStackData;
}

