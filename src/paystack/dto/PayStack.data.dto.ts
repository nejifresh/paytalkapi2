import { ApiProperty } from '@nestjs/swagger';

export class PayStackData {
  @ApiProperty()
  authorization_url: string;
  @ApiProperty()
  access_code: string;
  @ApiProperty()
  reference: string;
}
