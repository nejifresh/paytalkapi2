import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class ResolveAccountNumberDto {

  @ApiProperty()
  @IsDefined()
  bankCode: number;
  @IsDefined()
  @ApiProperty()
  accountNumber: number;


}
