import {
  HttpService,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
  Res,
} from '@nestjs/common';
import { CreatePaystackDto } from './dto/create-paystack.dto';
import { Response } from 'express';
import { firebaseAdmin, logger } from '../commons/firebaseConfig';
import { ErrorResponseDto } from '../commons/dto/errorResponse.dto';
import { UserRecord } from 'firebase-functions/lib/providers/auth';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { PaystackBanksDto } from './dto/banks.dto';
import { CreatePaystackSubAccountDto } from './dto/createSubAccount.dto';
import { Account } from '../organizations/entities/account.entity';
import {
  Beneficiary,
  Campaign,
  Transaction,
} from '../transactions/entities/transaction.entity';
import { Sender } from '../commons/interfaces/paytalk-transaction.interface';
import { PayAuthCodeDto } from './dto/payAuthCode.dto';
import { ResolveAccountNumberDto } from './dto/resolveAccountNumber.dto';

@Injectable()
export class PaystackService {
  constructor(private httpService: HttpService) {}

  initalizeCharge(
    createPaystackDto: CreatePaystackDto,
    loggedInUserId: string,
    @Res() response: Response,
  ): Promise<any> {
    let loggedUser: UserRecord;
    let reference: string;
    let beneficiary: Beneficiary;
    let campaign: Campaign;
    let responseData;

    return firebaseAdmin
      .auth()
      .getUser(loggedInUserId)
      .then(user => {
        loggedUser = user;
      })
      .then(() => {
        return firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(createPaystackDto.organizationId)
          .get();
      })
      .then(orgDoc => {
        if (!orgDoc.exists) {
          throw new NotFoundException('Organization Id is not valid');
        }

        let { category } = orgDoc.data();

        category = category.toLowerCase();

        if (category === 'non-profit') {
          return firebaseAdmin
            .firestore()
            .collection('organizations')
            .doc(createPaystackDto.organizationId)
            .collection('campaigns')
            .doc(createPaystackDto.campaignId)
            .get();
        }
      })
      .then(campaignsRef => {
        if (campaignsRef) {
          if (!campaignsRef.exists) {
            throw new NotFoundException('Campaigns Id is not valid');
          }
          const { profileId, title } = campaignsRef.data();

          campaign = { title, id: campaignsRef.id };

          return firebaseAdmin
            .firestore()
            .collection('organizations')
            .doc(createPaystackDto.organizationId)
            .collection('profiles')
            .doc(profileId)
            .get();
        } else {
          campaign = null;
          return firebaseAdmin
            .firestore()
            .collection('organizations')
            .doc(createPaystackDto.organizationId)
            .collection('profiles')
            .doc(createPaystackDto.profileId)
            .get();
        }
      })
      .then(profile => {
        const { paystackSubAccount } = profile.data().account;

        beneficiary = {
          name: profile.data().name,
          profileId: profile.id,
        };

        return this.httpService
          .post('transaction/initialize', {
            email: loggedUser.email,
            amount: createPaystackDto.amount * 100,
            subaccount: paystackSubAccount,
            bearer: 'subaccount',
          })
          .toPromise();
      })
      .then(payResponse => {
        const { data } = payResponse.data;
        responseData = payResponse.data;
        reference = data.reference;

        const transaction = new Transaction(
          createPaystackDto.amount,
          'NGN',
          createPaystackDto.organizationId,
          loggedUser.uid,
          loggedUser.email,
          'Paystack',
          reference,
          beneficiary,
          campaign,
          '',
          createPaystackDto.transactionType
        );

        return firebaseAdmin
          .firestore()
          .collection('transactions2')
          .doc(reference)
          .create({ ...transaction });
      }).then(() => {
        return firebaseAdmin
          .firestore()
          .collection('transactions2')
          .doc(reference)
          .get();
      })
      .then(txnData => {
        return response.status(HttpStatus.CREATED).json({
          transaction: txnData.data(),
          data: responseData
        });
      })
      .catch(error => {
        Logger.error(`An Error Occurred`, error, 'Paystack Service');
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(new ErrorResponseDto('Something went wrong', 500));
      });
  }

  ListAllNGBanks(@Res() response: Response): Promise<Response> {
    return this.httpService
      .get('/bank')
      .toPromise()
      .then(APIresponse => {
        const { data } = APIresponse.data;
        const banks: Array<PaystackBanksDto> = new Array<PaystackBanksDto>();

        data.forEach(bank => {
          banks.push(new PaystackBanksDto(bank));
        });
        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto(banks, HttpStatus.OK));
      });
  }

  resolveAccountNumber(
    resolveAccountNumberDto: ResolveAccountNumberDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.httpService
      .get(
        `bank/resolve?account_number=${resolveAccountNumberDto.accountNumber}&bank_code=${resolveAccountNumberDto.bankCode}`,
      )
      .toPromise()
      .then(APIresponse => {
        const { data } = APIresponse.data;

        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto(data, HttpStatus.OK));
      });
  }

  payWithAuthCode(
    paystackAuthCodeDTO: PayAuthCodeDto,
    loggedInUserId: string,
    @Res() response: Response,
  ): Promise<any> {
    let loggedUser: UserRecord;
    let reference: string;
    let beneficiary: Beneficiary;
    let campaign: Campaign;
    let card: FirebaseFirestore.DocumentData;
    let responseData;

    return firebaseAdmin
      .auth()
      .getUser(loggedInUserId)
      .then(user => {
        loggedUser = user;
        return firebaseAdmin
          .firestore()
          .collection('users')
          .doc(user.uid)
          .collection('cards')
          .doc(paystackAuthCodeDTO.cardId)
          .get();
      })
      .then(cardDocRef => {
        if (!cardDocRef.exists) {
          throw new NotFoundException('Card no found');
        }

        card = cardDocRef.data();

        return firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(paystackAuthCodeDTO.organizationId)
          .get();
      })
      .then(orgDoc => {
        if (!orgDoc.exists) {
          throw new NotFoundException('Organization Id is not valid');
        }

        let { category } = orgDoc.data();

        category = category.toLowerCase();

        if (category === 'non-profit') {
          return firebaseAdmin
            .firestore()
            .collection('organizations')
            .doc(paystackAuthCodeDTO.organizationId)
            .collection('campaigns')
            .doc(paystackAuthCodeDTO.campaignId)
            .get();
        }
      })
      .then(campaignsRef => {
        if (campaignsRef) {
          if (campaignsRef.exists) {
            throw new NotFoundException('Campaigns Id is not valid');

            const { profileId, title } = campaignsRef.data();

            campaign = { title, id: campaignsRef.id };

            return firebaseAdmin
              .firestore()
              .collection('organizations')
              .doc(paystackAuthCodeDTO.organizationId)
              .collection('profiles')
              .doc(profileId)
              .get();
          }
        } else {
          campaign = null;
          return firebaseAdmin
            .firestore()
            .collection('organizations')
            .doc(paystackAuthCodeDTO.organizationId)
            .collection('profiles')
            .doc(paystackAuthCodeDTO.profileId)
            .get();
        }
      })
      .then(profile => {
        const { paystackSubAccount } = profile.data().account;

        beneficiary = {
          name: profile.data().name,
          profileId: profile.id,
        };
        const { authorization_code } = card;

        return this.httpService
          .post('transaction/charge_authorization', {
            email: loggedUser.email,
            amount: paystackAuthCodeDTO.amount * 100,
            subaccount: paystackSubAccount,
            bearer: 'subaccount',
            authorization_code: authorization_code,
          })
          .toPromise();
      })
      .then(payResponse => {
        const { data } = payResponse.data;

        responseData = payResponse.data;

        reference = data.reference;

        const transaction = new Transaction(
          paystackAuthCodeDTO.amount,
          'NGN',
          paystackAuthCodeDTO.organizationId,
          loggedUser.uid,
          loggedUser.email,
          'Paystack',
          reference,
          beneficiary,
          campaign,
          '',
          paystackAuthCodeDTO.transactionType
        );

        return firebaseAdmin
          .firestore()
          .collection('transactions2')
          .doc(reference)
          .create({ ...transaction });
      })
      .then(() => {
        return firebaseAdmin
          .firestore()
          .collection('transactions2')
          .doc(reference)
          .get();
      })
      .then(txnData => {
        return response.status(HttpStatus.CREATED).json({
          transaction: txnData.data(),
          data: responseData
        });
      })
      .catch(error => {
        logger.error(error);
        Logger.error(`An Error Occurred`, error, 'Paystack Service');
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(new ErrorResponseDto('Something went wrong', 500));
      });
  }

  updateSubAccount(
    createSubAccount: CreatePaystackSubAccountDto,
    @Res() response: Response,
  ): Promise<Response> {
    return firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(createSubAccount.organizationId)
      .get()
      .then(orgDoc => {
        if (!orgDoc.exists) {
          throw new NotFoundException('Organization Id is not valid');
        }
      })
      .then(() => {
        return firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(createSubAccount.organizationId)
          .collection('profiles')
          .doc(createSubAccount.profileId);
      })
      .then(profileRef => {
        return profileRef.get();
      })
      .then(docRef => {
        if (!docRef.exists) {
          throw new NotFoundException('Profile Id is not valid');
        }

        const { paystackSubAccount } = docRef.data().account;

        return this.httpService
          .put(`subaccount/${paystackSubAccount}`, {
            business_name: createSubAccount.name,
            settlement_bank: createSubAccount.bankCode,
            account_number: createSubAccount.accountNumber.toString(),
          })
          .toPromise();
      })
      .then(() => {
        return response
          .status(HttpStatus.OK)
          .json(
            new SuccessResponseDto(
              'Paystack Sub account Updated',
              HttpStatus.OK,
            ),
          );
      })
      .catch(error => {
        Logger.error(`An Error Occurred`, error, 'Paystack Service');
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(new ErrorResponseDto('Something went wrong', 500));
      });
  }

  createSubAccount(
    createSubAccount: CreatePaystackSubAccountDto,
    @Res() response: Response,
  ): Promise<Response> {
    let profileDocRef: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>;

    let profileDoc: FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>;

    let orgSplit: number;

    return firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(createSubAccount.organizationId)
      .get()
      .then(orgDoc => {
        if (!orgDoc.exists) {
          throw new NotFoundException('Organization Id is not valid');
        }

        const { splitRatio } = orgDoc.data();

        orgSplit = splitRatio;
      })
      .then(() => {
        return firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(createSubAccount.organizationId)
          .collection('profiles')
          .doc(createSubAccount.profileId);
      })
      .then(profileRef => {
        profileDocRef = profileRef;
        return profileRef.get();
      })
      .then(docRef => {
        if (!docRef.exists) {
          throw new NotFoundException('Profile Id is not valid');
        }

        profileDoc = docRef;

        return this.httpService
          .post('/subaccount', {
            business_name: createSubAccount.name,
            settlement_bank: createSubAccount.bankCode,
            account_number: createSubAccount.accountNumber.toString(),
            percentage_charge: orgSplit,
          })
          .toPromise();
      })
      .then(APIresponse => {
        const { subaccount_code } = APIresponse.data.data;

        const account: Account = profileDoc.data().account;

        account.paystackSubAccount = subaccount_code;

        return profileDocRef.update({ account });
      })
      .then(() => {
        return response
          .status(HttpStatus.OK)
          .json(
            new SuccessResponseDto(
              'Paystack Sub account Created',
              HttpStatus.OK,
            ),
          );
      })
      .catch(error => {
        Logger.error(`An Error Occurred`, error, 'Paystack Service');
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(new ErrorResponseDto('Something went wrong', 500));
      });
  }

  processWebhook(webhook: any): void {
    const { data, event } = webhook;

    const { status, authorization, fees_split, reference } = data;

    let cardsDocRef: FirebaseFirestore.CollectionReference;

    firebaseAdmin
      .firestore()
      .collection('paystackWebhooks')
      .doc(reference)
      .create(data)
      .then(() => {
        if (event === 'charge.success' && status === 'success')
          return firebaseAdmin
            .firestore()
            .collection('transactions2')
            .doc(reference)
            .update({ status: 'success' });
      })
      .then(() => {
        return firebaseAdmin
          .firestore()
          .collection('transactions2')
          .doc(reference)
          .get();
      })
      .then(tref => {
        const { senderUid } = tref.data();

        return firebaseAdmin
          .firestore()
          .collection('users')
          .doc(senderUid)
          .collection('cards');
      })
      .then(cardDocRef => {
        cardsDocRef = cardDocRef;
        return cardDocRef
          .where('bin', '==', `${authorization.bin}`)
          .where('last4', '==', `${authorization.last4}`)
          .get();
      })
      .then(snapshot => {
        if (snapshot.empty) return cardsDocRef.add({ ...authorization });
      })
      .catch(error => {
        //Logger.error(`An Error Occurred`, error, 'Paystack Webhook Service');
        logger.error(error);
      });
  }
}
