import { Body, Controller, Get, HttpStatus, Post, Put, Res, UseGuards, UsePipes } from '@nestjs/common';
import { PaystackService } from './paystack.service';
import { CreatePaystackDto } from './dto/create-paystack.dto';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { Response } from 'express';
import { GetUid } from '../auth/decorators/getAuthenticatedUser.decorator';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { PaystackBanksResponse } from './dto/paystackBanksResponse';
import { PaystackResponseDto } from './dto/paystack-response.dto';
import { CreatePaystackSubAccountDto } from './dto/createSubAccount.dto';
import { PaystackWebhookGuardGuard } from './paystack-webhook-guard.guard';
import { PayAuthCodeDto } from './dto/payAuthCode.dto';
import { ResolveAccountNumberDto } from './dto/resolveAccountNumber.dto';

@Controller('paystack')
@ApiBearerAuth()
@ApiTags('paystack')
export class PaystackController {
  constructor(private readonly paystackService: PaystackService) {
  }

  @UseGuards(AuthGuard)
  @Post('initialize')
  @ApiOperation({ summary: 'Initialize Charge' })
  @ApiCreatedResponse({
    description: 'Pay',
    type: PaystackResponseDto,
  })
  @UsePipes(DTOValidationPipe)
  create(@Body() createPaystackDto: CreatePaystackDto,
         @GetUid() loggedInUserId: string,
         @Res() response: Response,
  ): Promise<Response> {
    return this.paystackService.initalizeCharge(createPaystackDto, loggedInUserId, response);
  }

  @UseGuards(AuthGuard)
  @Post('sub-account')
  @ApiOperation({ summary: 'Create SubAccount' })
  @ApiCreatedResponse({
    description: 'Pay',
    type: PaystackResponseDto,
  })
  @UsePipes(DTOValidationPipe)
  createSubAccount(@Body() createPaystackDto: CreatePaystackSubAccountDto,
                   @Res() response: Response,
  ): Promise<Response> {
    return this.paystackService.createSubAccount(createPaystackDto, response);
  }

  @UseGuards(AuthGuard)
  @Put('sub-account')
  @ApiOperation({ summary: 'Update SubAccount' })
  @ApiCreatedResponse({
    description: 'Pay',
    type: PaystackResponseDto,
  })
  @UsePipes(DTOValidationPipe)
  updateSubAccount(@Body() createPaystackDto: CreatePaystackSubAccountDto,
                   @Res() response: Response,
  ): Promise<Response> {
    return this.paystackService.updateSubAccount(createPaystackDto, response);
  }

  @UseGuards(AuthGuard)
  @Post('card')
  @ApiOperation({ summary: 'Charge card' })
  @ApiCreatedResponse({
    description: 'Pay',
    type: PaystackResponseDto,
  })
  chargeWithCode(@Body() payAuthCodeDto: PayAuthCodeDto,
                 @GetUid() loggedInUserId: string,
                 @Res() response: Response,
  ): Promise<Response> {
    return this.paystackService.payWithAuthCode(payAuthCodeDto, loggedInUserId, response);
  }

  @UseGuards(AuthGuard)
  @Get('banks')
  @ApiOperation({ summary: 'List All Banks' })
  @ApiOkResponse({
    description: 'Banks',
    type: PaystackBanksResponse,
  })
  listAllBanks(@Res() response: Response,
  ): Promise<Response> {
    return this.paystackService.ListAllNGBanks(response);
  }


  @UseGuards(AuthGuard)
  @Post('resolveAccount')
  @ApiOperation({ summary: 'Get Account Name from Account Number' })
  @ApiOkResponse({
    description: 'Account Info',
    type: PaystackBanksResponse,
  })
  resolve(@Body() resolveAccountNumberDto: ResolveAccountNumberDto, @Res() response: Response,
  ): Promise<Response> {
    return this.paystackService.resolveAccountNumber(resolveAccountNumberDto, response);
  }

  @UseGuards(PaystackWebhookGuardGuard)
  @Post('webhook')
  @ApiOperation({ summary: 'Paystack Webhook' })
  @ApiOkResponse()
  makePayment(@Body() requestBody: any, @Res() res: Response) {
    this.paystackService.processWebhook(requestBody);
    return res.status(HttpStatus.OK).json('Event Received');
  }
}

