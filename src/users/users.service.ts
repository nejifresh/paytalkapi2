import { HttpStatus, Injectable, NotFoundException, Res, UnauthorizedException } from '@nestjs/common';
import { UserDto } from './dto/get-user.dto';

import { UsersDto } from './dto/get-users.dto';
import { Response } from 'express';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { IUser } from './interface/user.interface';
import { GetUserByTokenDto } from './dto/getUserByToken.dto';

@Injectable()
export class UsersService {

  async findAll(
    @Res() response: Response,
    filter?: { propName: string; propVal: string },
  ): Promise<Response> {
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('users')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('users')
        .get();
    }

    const users: Array<UsersDto> = [];

    snapshot.forEach(doc => {
      users.push(new UsersDto(doc.data() as IUser));
    });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(users, HttpStatus.OK));
  }

  async findOne(id: string, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(id)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('user Id is not valid');
    }

    const user = new UserDto(snapshot.data() as IUser);

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(user, HttpStatus.OK));
  }

  async getUserCount(@Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('statsCount')
      .doc('app-users')
      .get();

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(snapshot.data(), HttpStatus.OK));
  }

  async getUserByToken(getUserDTO: GetUserByTokenDto, @Res() response: Response): Promise<Response> {
    return firebaseAdmin.auth().verifyIdToken(getUserDTO.token)
      .then((decodedToken) => {
        //req.uuid = decodedToken.sub;
        return this.findOne(decodedToken.sub, response);
      })
      .catch(() => {
        throw new UnauthorizedException('Token is not valid');
      });
  }
}
