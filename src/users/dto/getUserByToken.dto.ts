import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class GetUserByTokenDto {
  @ApiProperty()
  @IsDefined()
  token: string;
}
