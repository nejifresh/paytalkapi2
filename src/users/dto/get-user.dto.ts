import { IUser } from '../interface/user.interface';

export class UserDto {
  firstName: string;
  lastName: string;
  country: string;
  currency: string;
  dateJoined: Date;
  email: string;
  imageUrl: string;
  enrolled: boolean;
  userName: string;
  uuid: string;
  stripeCustomerId: string;


  constructor(user: IUser) {
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.country = user.country;
    this.currency = user.currency;
    this.dateJoined = user.dateJoined.toDate();
    this.email = user.email;
    this.imageUrl = user.imageUrl;
    this.enrolled = user.enrolled;
    this.userName = user.userName;
    this.uuid = user.uuid;
    this.stripeCustomerId = user.stripeCustomerId;
  }
}
