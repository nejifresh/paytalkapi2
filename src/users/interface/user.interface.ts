import { firestore } from 'firebase-admin';
import Timestamp = firestore.Timestamp;

export interface IUser {
  firstName: string;
  lastName: string;
  country: string;
  currency: string;
  dateJoined: Timestamp;
  email: string;
  imageUrl: string;
  enrolled: boolean;
  userName: string;
  uuid: string;
  stripeCustomerId: string;
}
