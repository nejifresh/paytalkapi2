import { Body, Controller, Get, Param, Post, Query, Res } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { UserDto } from './dto/get-user.dto';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import { GetUserByTokenDto } from './dto/getUserByToken.dto';

@Controller('app-users')
@ApiTags('App-Users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {
  }


  @Get()
  @ApiOperation({ summary: 'Get All Users' })
  @ApiOkResponse({
    description: 'User',
    type: UserDto,
  })
  @ApiImplicitQuery({
    name: 'propName',
    description: 'Property Name',
    required: false,
    type: Number,
  })
  @ApiImplicitQuery({
    name: 'propVal',
    description: 'Property Value',
    required: false,
    type: Number,
  })
  findAll(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ): Promise<Response> {
    if (propName && propVal) {
      const filter = { propName, propVal };
      return this.usersService.findAll(response, filter);
    } else {
      return this.usersService.findAll(response);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get User' })
  @ApiOkResponse({
    description: 'User',
    type: UserDto,
  })
  findOne(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.usersService.findOne(id, response);
  }

  @Get('stats/count')
  @ApiOperation({ summary: 'Get User Stats' })
  getStats(
    @Res() response: Response,
  ): Promise<Response> {
    return this.usersService.getUserCount(response);
  }

  @Post('token')
  @ApiOperation({ summary: 'Get User By Auth Token' })
  getUserByAuthToken(@Body() tokenDto: GetUserByTokenDto,
                     @Res() response: Response,
  ): Promise<Response> {
    return this.usersService.getUserByToken(tokenDto, response);
  }
}
