import * as dotEnv from 'dotenv';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from './commons/filters/http-exception.filter';
import { BanksModule } from './banks/banks.module';
import { SubAccountModule } from './sub-account/sub-account.module';
import { OrganizationsModule } from './organizations/organizations.module';
import { TransactionsModule } from './transactions/transactions.module';
import { PaystackModule } from './paystack/paystack.module';
import { OrganizationUsersModule } from './organizationUsers/organizationUsers.module';
import { SendGridModule } from '@anchan828/nest-sendgrid';
import { VoiceRecognitionModule } from './voice-recognition/voice-recognition.module';
import { CommonsModule } from './commons/commons.module';
import { PayModule } from './pay/pay.module';
import { GspeechModule } from './gspeech/gspeech.module';
import { PaypalModule } from './paypal/paypal.module';
import { FlutterwaveModule } from './flutterwave/flutterwave.module';
import { PlatformModule } from './platform/platform.module';
import { StripeModule } from './stripe/stripe.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderModule } from './order/order.module';
import { DealsModule } from './deals/deals.module';
import { ReservationsModule } from './reservations/reservations.module';
import { ReservationChargesModule } from './reservation-charges/reservation-charges.module';
import { ProfileModule } from './profile/profile.module';
import { WalletsModule } from './wallets/wallets.module';
import { WebhooksModule } from './webhooks/webhooks.module';
import { TaxModule } from './tax/tax.module';
import { DriversModule } from './drivers/drivers.module';
import { RidesModule } from './rides/rides.module';
import { UsersModule } from './users/users.module';
import { LoansModule } from './loans/loans.module';
import { ParkingModule } from './parking/parking.module';
import { EventsModule } from './events/events.module';
import { ChatgptModule } from './chatgpt/chatgpt.module';
import { GiftCardsModule } from './gift-cards/gift-cards.module';

dotEnv.config();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mssql',
      host: 'paytalkdb.database.windows.net',
      port: 1433,
      username: 'saviour',
      password: 'Paytalk123$',
      database: 'paytalkdb',
      entities: [__dirname + '/**/entities/*.entity.{ts,js}'],
      keepConnectionAlive: true,
    }),
    AuthModule,
    BanksModule,
    SubAccountModule,
    OrganizationsModule,
    TransactionsModule,
    PaystackModule,
    OrganizationUsersModule,
    SendGridModule.forRoot({
      apikey: process.env.SENDGRID_API_KEY,
    }),
    VoiceRecognitionModule,
    CommonsModule,
    PayModule,
    GspeechModule,
    PaypalModule,
    FlutterwaveModule,
    PlatformModule,
    StripeModule,
    DealsModule,
    OrderModule,
    ReservationsModule,
    ReservationChargesModule,
    ProfileModule,
    WalletsModule,
    WebhooksModule,
    TaxModule,
    DriversModule,
    RidesModule,
    UsersModule,
    LoansModule,
    ParkingModule,
    EventsModule,
    ChatgptModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule {}
