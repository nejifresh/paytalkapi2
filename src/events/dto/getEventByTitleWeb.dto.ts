import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class GetEventByTitleDtoWeb {

  @ApiProperty()
  @IsDefined()
  title: string;

}
