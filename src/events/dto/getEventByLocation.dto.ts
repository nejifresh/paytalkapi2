import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class GetEventByLocationDto {

  @ApiProperty()
  @IsDefined()
  longitude: number;

  @ApiProperty()
  @IsDefined()
  latitude: number;

}
