import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class GetEventByTitleDto {

  @ApiProperty()
  @IsDefined()
  longitude: number;

  @ApiProperty()
  @IsDefined()
  latitude: number;

  @ApiProperty()
  @IsDefined()
  title: string;

}
