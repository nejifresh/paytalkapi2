import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class GetTicketsDto {

  @ApiProperty()
  @IsDefined()
  userId: string;

  @ApiProperty()
  @IsDefined()
  eventId: string;

  @ApiProperty()
  @IsDefined()
  numberOfTickets: number;
}
