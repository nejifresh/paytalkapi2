import { Body, Controller, Post, Res } from '@nestjs/common';
import { EventsService } from './events.service';
import { ApiTags } from '@nestjs/swagger';
import { GetEventByLocationDto } from './dto/getEventByLocation.dto';
import { Response } from 'express';
import { GetEventByTitleDto } from './dto/getEventByTitle.dto';
import { GetTicketsDto } from './dto/getTickets.dto';
import { GetEventByTitleDtoWeb } from './dto/getEventByTitleWeb.dto';

@Controller('events')
@ApiTags('Events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {
  }

  /*@Post()
  create(@Body() createEventDto: CreateEventDto) {
    return this.eventsService.create();
  }*/

  @Post('location')
  findAllByLocation(@Body() payload: GetEventByLocationDto, @Res() response: Response) {
    return this.eventsService.findAllByLocation(payload, response);
  }

  @Post('title')
  findAllByTitle(@Body() payload: GetEventByTitleDto, @Res() response: Response) {
    return this.eventsService.findAllByTitle(payload, response);
  }

  @Post('location-web')
  findAllByLocationWeb(@Body() payload: GetEventByLocationDto, @Res() response: Response) {
    return this.eventsService.findAllByLocationWeb(payload, response);
  }

  @Post('title-web')
  findAllByTitleWeb(@Body() payload: GetEventByTitleDtoWeb, @Res() response: Response) {
    return this.eventsService.findAllByTitleWeb(payload, response);
  }

  @Post('buy-ticket')
  purchaseTicketFromWallet(@Body() payload: GetTicketsDto, @Res() response: Response) {
    return this.eventsService.buyTicketFromWallet(payload, response);
  }
}
