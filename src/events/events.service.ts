import { HttpStatus, Injectable, NotFoundException, Res } from '@nestjs/common';
import { adminTypes, firebaseAdmin } from '../commons/firebaseConfig';
import { IEvent } from '../commons/interfaces/events.interface';
import { Events } from '../commons/entities/events.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventImages } from '../commons/entities/EventImages.entity';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { Response } from 'express';
import { GetEventByLocationDto } from './dto/getEventByLocation.dto';
import { GetEventByTitleDto } from './dto/getEventByTitle.dto';
import { GetTicketsDto } from './dto/getTickets.dto';
import { UserDto } from '../users/dto/get-user.dto';
import { IUser } from '../users/interface/user.interface';
import { WalletsService } from '../wallets/wallets.service';
import { ChargeWalletDto } from '../wallets/dto/charge-wallet.dto';
import { uuid } from '@supercharge/strings';
import { IProfile } from '../commons/interfaces/profile.interface';
import { SelectedPrice } from '../stripe/interfaces/onlineTicketSale.interface';
import { GetEventByTitleDtoWeb } from './dto/getEventByTitleWeb.dto';

@Injectable()
export class EventsService {

  constructor(
    @InjectRepository(Events)
    private eventsRepository: Repository<Events>,
    private walletService: WalletsService,
  ) {
  }


  async create() {

    /*const eventCategoriesSnap =  await  firebaseAdmin.firestore().collection('eventCategories').get()

      const eventCategories = eventCategoriesSnap.docs;

      for (const eventCategory of eventCategories) {
      const  eventCategoryData = eventCategory.data() as IEventCategories

        const eventCat = new EventCategories(eventCategory.id,eventCategoryData.name).save()
      }*/

    const eventsSnap = await firebaseAdmin.firestore().collection('events').get();

    const eventDocs = eventsSnap.docs;

    for (const eventDoc of eventDocs) {

      const eventData = eventDoc.data() as IEvent;

      //console.log(eventDoc.id,eventData.location.latitude,eventData.location.longitude)

      const eventSQL = await new Events(eventDoc.id, eventData.categoryId, eventData.address,
        eventData.currency, eventData.dateCreated.toDate(), eventData.description,
        eventData.endDateTime.toDate(), eventData.eventLink, eventData.location.longitude,
        eventData.location.latitude, eventData.maxTickets, eventData.minTickets,
        eventData.organizationId, eventData.profileId, eventData.promoImage,
        eventData.promoVideo, eventData.startDateTime.toDate(),
        eventData.ticketPrice, eventData.title).save();

      for (const image of eventData.images) {
        await new EventImages(eventSQL.id, image).save();

      }

    }

  }

  async findAllByLocation(payload: GetEventByLocationDto, @Res() response: Response) {
    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.eventsRepository.manager.query(
            `EventsByLocation @0,@1`,
            [payload.latitude, payload.longitude],
          ),
          HttpStatus.OK,
        ),
      );
  }

  async findAllByTitle(payload: GetEventByTitleDto, @Res() response: Response) {
    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.eventsRepository.manager.query(
            `EventsByTitle @0,@1`,
            [payload.latitude, payload.longitude, payload.title],
          ),
          HttpStatus.OK,
        ),
      );
  }

  async findAllByTitleWeb(payload: GetEventByTitleDtoWeb, @Res() response: Response) {
    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.eventsRepository.manager.query(
            `EventsByTitle_Web @0`,
            [payload.title],
          ),
          HttpStatus.OK,
        ),
      );
  }

  async findAllByLocationWeb(payload: GetEventByLocationDto, @Res() response: Response) {
    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.eventsRepository.manager.query(
            `EventsByLocation_Web @0,@1`,
            [payload.latitude, payload.longitude],
          ),
          HttpStatus.OK,
        ),
      );
  }

  async buyTicketFromWallet(payload: GetTicketsDto, @Res() response: Response) {


    const snapshot = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(payload.userId)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('user Id is not valid');
    }

    const eventSnapshot = await firebaseAdmin.firestore().collection('events')
      .doc(payload.eventId).get();

    const event = eventSnapshot.data() as IEvent;

    const user = new UserDto(snapshot.data() as IUser);

    const ticketAmount = await this.calculateTicketTotalAmount(event, payload.numberOfTickets);

    const chargeWallet = new ChargeWalletDto(payload.userId, ticketAmount.totalAmount, event.currency);

    await this.walletService.chargeUserWallet(chargeWallet);

    const newEventTicket = await firebaseAdmin.firestore().collection('eventTickets')
      .add({
        code: uuid(),
        currency: event.currency,
        datePurchased: adminTypes.firestore.Timestamp.now(),
        eventId: payload.eventId,
        purchased: payload.numberOfTickets,
        ticketAmount: ticketAmount.totalAmount,
        ticketFeeAmount: ticketAmount.serviceCharge,
        ticketTaxAmount: ticketAmount.taxAmount,
        userId: payload.userId,
        eventName: event.title,
        admitted: false,
        numberAdmitted: 0,
      });

    const transaction = {
      beneficiary: {
        //name: profile.name,
        profileId: event.profileId,
      },
      amount: +ticketAmount.totalAmount,
      currency: event.currency,
      dateOfTransaction: adminTypes.firestore.Timestamp.now(),
      status: 'success',
      gateway: 'stripe',
      senderEmail: user.email,
      senderUid: payload.userId,
      organizationId: event.organizationId,
      transactionId: '',
      type: 'Events',
      gatewayCommission: 0,
    };

    const docRef = await firebaseAdmin
      .firestore()
      .collection('transactions2')
      .add(transaction);

    const transactionId = docRef.id;

    await docRef.update({ transactionId });
    await newEventTicket.update({ transactionId });

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto('Event Ticket Purchased', HttpStatus.OK));

  }

  async calculateTicketTotalAmount(event: IEvent, numberOfTickets: number, selectedPrice?: SelectedPrice): Promise<TicketBreakdown> {

    const profilesSnapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(event.organizationId)
      .collection('profiles')
      .doc(event.profileId)
      .get();

    let ticketAmount;

    let tax;

    let serviceCharge;

    const profile = profilesSnapshot.data() as IProfile;

    switch (event.ticketPriceType) {
      case 'SINGLE': {

        ticketAmount = (event.ticketPrice * numberOfTickets);

        tax = (profile.taxPercent / 100) * ticketAmount;

        serviceCharge = (profile.serviceCharge / 100) * ticketAmount;
      }

      case 'VARIABLE': {

        ticketAmount = (selectedPrice.price * numberOfTickets);

        tax = (profile.taxPercent / 100) * ticketAmount;

        serviceCharge = (profile.serviceCharge / 100) * ticketAmount;
      }

    }


    const ticket: TicketBreakdown = {
      serviceCharge: serviceCharge, taxAmount: ticketAmount, totalAmount: ticketAmount + tax + serviceCharge,

    };

    return ticket;
  }

}

export interface TicketBreakdown {
  totalAmount: number;
  serviceCharge: number;
  taxAmount: number;
}
