import { Body, Controller, Param, Post, Res, UseGuards, UsePipes } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { GetUid } from '../auth/decorators/getAuthenticatedUser.decorator';
import { Response } from 'express';
import { ApiBearerAuth, ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { OgranizationUser } from './entities/organizationUser.entity';

@Controller('users')
@ApiBearerAuth()
@ApiTags('users')
@UseGuards(AuthGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {
  }

  @Post(':orgId')
  @ApiOperation({ summary: 'Create User' })
  @ApiCreatedResponse({
    description: 'User',
    type: OgranizationUser,
  })
  @UsePipes(DTOValidationPipe)
  create(@Body() createUserDto: CreateUserDto,
         @GetUid() loggedInUserId: string,
         @Param('orgId') id: string,
         @Res() response: Response,
  ): Promise<Response> {
    return this.usersService.create(createUserDto, loggedInUserId, id, response);
  }

}
