import { HttpStatus, Injectable, NotFoundException, Res } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Response } from 'express';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { OgranizationUser } from './entities/organizationUser.entity';
import { SendGridService } from '@anchan828/nest-sendgrid';
import { IOrganizationsUsers } from './dto/organizations.users.dto';

@Injectable()
export class UsersService {
  constructor(private readonly sendGrid: SendGridService) {}

  async create(
    createUserDto: CreateUserDto,
    loggedInUserId: string,
    organizationId: string,
    @Res() response: Response,
  ): Promise<Response> {
    const organization = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(organizationId)
      .get();

    if (!organization.exists) {
      throw new NotFoundException('Organization Id is not valid');
    }

    const orgProfile = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(organizationId)
      .collection('profiles')
      .doc(createUserDto.profileId)
      .get();

    if (!orgProfile.exists) {
      throw new NotFoundException('Profile Id is not valid');
    }

   const orgUserSnap = await firebaseAdmin
      .firestore()
      .collection('organizationUsers')
     .where('email','==', `${createUserDto.email}`).get()

    if(orgUserSnap.empty){

      const user: OgranizationUser = new OgranizationUser(
        createUserDto.email,
        loggedInUserId,
      );

      user.organizations.push({
        organizationId: organization.id,
        profileId: orgProfile.id,
        role: createUserDto.role,
      });

      user.organizationIds.push(organization.id);

      const userRef = await firebaseAdmin
        .firestore()
        .collection('users')
        .where('email','==', `${createUserDto.email}`).get()

      if(userRef.empty){
        await firebaseAdmin
          .firestore()
          .collection('organizationUsers')
          .add({ ...user });
      }else{

        const appUserDoc = userRef.docs[0];

         await firebaseAdmin
          .firestore()
          .collection('organizationUsers')
          .doc(appUserDoc.id)
          .create({ ...user });

      }

    }else {
      const orgUserRef = orgUserSnap.docs[0];

      const {organizations, organizationIds} = orgUserRef.data() as IOrganizationsUsers;

      if(!organizationIds.some(orgId => orgId === organization.id)){
        organizationIds.push(organization.id);
      }

      if(!organizations.some(org => org.organizationId === organization.id) && !organizations.some(org => org.profileId === orgProfile.id)){
        organizations.push({
          organizationId: organization.id,
          profileId: orgProfile.id,
          role: createUserDto.role,
        });
      }

     await orgUserRef.ref.update({organizations,organizationIds})
    }


    return response
      .status(HttpStatus.CREATED)
      .json(
        `User with Email ${createUserDto.email} and role ${createUserDto.role} has been created successfully`,
      );
  }

  findAll() {
    return `This action returns all users`;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
