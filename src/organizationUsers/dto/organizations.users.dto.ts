export interface IOrganizationsUsers {

  organizations: Array<{
    organizationId: string,
    profileId: string, role: string
  }>

  organizationIds: Array<string>

}
