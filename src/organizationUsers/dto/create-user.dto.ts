import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class CreateUserDto {

  @ApiProperty()
  @IsDefined()
  email: string;

  @ApiProperty()
  @IsDefined()
  role: string;

  @ApiProperty()
  @IsDefined()
  profileId: string;
}
