import { ApiProperty } from '@nestjs/swagger';
import { adminTypes } from '../../commons/firebaseConfig';

export class OgranizationUser {
  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  uid: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  dateAdded: any;

  @ApiProperty()
  addedByUserId: string;

  @ApiProperty({ type: [String] })
  organizations: Array<{ organizationId: string; profileId: string; role: string }> = [];

  @ApiProperty({ type: [String] })
  organizationIds: Array<string> = [];

  accountSetupCompleted: boolean;
  passwordChanged: boolean;

  constructor(email: string, loggedInUserId: string) {
    this.email = email;
    this.dateAdded = adminTypes.firestore.Timestamp.now();
    this.addedByUserId = loggedInUserId;
    this.accountSetupCompleted = false;
    this.passwordChanged = false;
  }
}
