import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Stripe } from 'stripe';
import { OnboardRequestDto } from './dto/onboard-request.dto';
import { Response } from 'express';
import { firebaseAdmin, logger } from '../commons/firebaseConfig';
import { CreateCheckoutDto } from './dto/create-checkout.dto';
import { CreatePaymentIntentDto } from './dto/create-payment-intent.dto';
import { CreatePriceDto } from './dto/create-price.dto';
import { GetPaymentMethodsDto } from './dto/get-payment-methods.dto';
import { CreateSetupIntentDto } from './dto/create-setup-intent.dto';
import { DeleteSubscriptionDto } from './dto/delete-subscription.dto';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { CommonUtils } from '../commons/utils/common-utils.class';
import { UserRecord } from 'firebase-functions/lib/providers/auth';
import { CreateTransferDto } from './dto/create-transfer.dto';
import { EventPaymentDto } from './dto/event-payment.dto';
import { IOnlineTicketSale } from './interfaces/onlineTicketSale.interface';
import { IEvent } from '../commons/interfaces/events.interface';
import { UserDto } from '../users/dto/get-user.dto';
import { IUser } from '../users/interface/user.interface';
import { EventsService, TicketBreakdown } from '../events/events.service';

// TODO Add gatewayCommission, platformCommission, splitRatio & amountCredited to subscription methods
@Injectable()
export class StripeService {
  stripe: Stripe;

  constructor(private eventService: EventsService) {
    this.stripe = new Stripe(process.env.stripeSecretKey, {
      apiVersion: '2020-08-27',
      typescript: true,
    });
  }

  async createAccount(): Promise<Stripe.Response<Stripe.Account>> {
    try {
      const account = await this.stripe.accounts.create({
        type: 'express',
      });

      logger.info(account);

      return account;
    } catch (error) {
      logger.error(error);
      throw new HttpException(error.type, error.code);
    }
  }

  async initAccountOnboarding(
    reqObj: OnboardRequestDto,
  ): Promise<Stripe.Response<Stripe.AccountLink>> {
    const createdAcct = await this.createAccount();

    try {
      await firebaseAdmin
        .firestore()
        .collection('organizations')
        .doc(reqObj.organizationId)
        .collection('profiles')
        .doc(reqObj.profileId)
        .set({ stripeConnectedAccount: createdAcct.id }, { merge: true });
    } catch (error) {
      logger.error(error);
      throw new HttpException(error.type, error.code);
    }

    try {
      const accountLinks = await this.stripe.accountLinks.create({
        account: createdAcct.id,
        refresh_url: 'https://paytalk.org/stripe-setup',
        return_url: 'https://paytalk.org/stripe-return',
        type: 'account_onboarding',
      });

      return accountLinks;
    } catch (error) {
      logger.error(error);
      throw new HttpException(error.type, error.code);
    }
  }

  async createCheckoutSession(payload: CreateCheckoutDto): Promise<any> {
    const applicationFeeAmount = Math.round(
      CommonUtils.percentToVal(2.9, +payload.amount) +
        0.3 * 100 +
        CommonUtils.percentToVal(1, +payload.amount),
    );

    const platformCommission =
      +payload.amount -
      Math.round(CommonUtils.percentToVal(1, +payload.amount));

    const gatewayCommission =
      +payload.amount -
      Math.round(CommonUtils.percentToVal(2.9, +payload.amount));

    const amountCredited = +payload.amount - applicationFeeAmount;

    try {
      const session = await this.stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: [
          {
            price_data: {
              currency: payload.currency,
              product_data: {
                name: `INVOICE for ${payload.amount}`,
              },
              unit_amount: Math.round(+payload.amount),
            },
            quantity: 1,
          },
        ],
        mode: 'payment',
        payment_intent_data: {
          application_fee_amount: +applicationFeeAmount,
          transfer_data: {
            destination: payload.accountId,
          },
        },
        metadata: {
          type: 'Payment',
          splitRatio: payload.splitRatio,
          amountCredited,
          amountSent: payload.amount,
          beneficiaryName: payload.beneficiaryName,
          beneficiaryProfileId: payload.beneficiaryProfileId,
          lineItems: payload.lineItems,
          billId: payload.billId,
          organizationId: payload.organizationId,
          senderEmail: payload.senderEmail,
          imageUrl: payload.imageUrl,
          currency: payload.currency,
          campaignTitle: '',
          campaignId: '',
          platformCommission,
          gatewayCommission,
          applicationFeeAmount,
        },
        success_url: 'https://paytalk-bill-checkout.web.app/success',
        cancel_url: 'https://paytalk-bill-checkout.web.app/checkout',
      });

      return session;
    } catch (error) {
      logger.error(error);

      throw new HttpException(error.type, error.code);
    }
  }

  async createEventCheckoutSession(payload: EventPaymentDto): Promise<any> {
    const attemptDoc = await firebaseAdmin
      .firestore()
      .collection('onlineTicketSales')
      .doc(payload.attemptId)
      .get();

    if (!attemptDoc.exists) {
      throw new NotFoundException('Invalid Online Ticket Sales ID');
    }

    const attempt = attemptDoc.data() as IOnlineTicketSale;

    const eventSnapshot = await firebaseAdmin
      .firestore()
      .collection('events')
      .doc(attempt.eventId)
      .get();

    const event = eventSnapshot.data() as IEvent;

    let ticketAmounts: TicketBreakdown;

    switch (event.ticketPriceType) {
      case 'SINGLE': {
        ticketAmounts = await this.eventService.calculateTicketTotalAmount(
          event,
          attempt.qty,
        );
      }

      case 'VARIABLE': {
        ticketAmounts = await this.eventService.calculateTicketTotalAmount(
          event,
          attempt.qty,
          attempt.selectedPrice,
        );
      }
    }

    const gatewayCommission =
      +attempt.amount -
      Math.round(CommonUtils.percentToVal(2.9, +ticketAmounts.totalAmount));

    await attemptDoc.ref.update({
      totalAmount: ticketAmounts.totalAmount,
      serviceCharge: ticketAmounts.serviceCharge,
      taxAmount: ticketAmounts.taxAmount,
    });

    const snapshot = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(attempt.userId)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('user Id is not valid');
    }

    const user = new UserDto(snapshot.data() as IUser);

    try {
      const session = await this.stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: [
          {
            price_data: {
              currency: event.currency,
              product_data: {
                name: `INVOICE for ${Math.round(+ticketAmounts.totalAmount)}`,
              },
              unit_amount: Math.round(+ticketAmounts.totalAmount) * 100,
            },
            quantity: 1,
          },
        ],
        mode: 'payment',
        customer: user.stripeCustomerId,
        metadata: {
          saleId: payload.attemptId,
          type: 'Event_Sales',
          amountSent: ticketAmounts.totalAmount,
          beneficiaryProfileId: event.profileId,
          organizationId: event.organizationId,
          senderEmail: user.email,
          eventName: event.title,
          gatewayCommission,
        },
        success_url: 'https://paytalk-events.web.app/events/success',
        cancel_url: `https://paytalk-events.web.app/events/${attempt.eventId}`,
      });

      return session;
    } catch (error) {
      logger.error(error);

      throw new HttpException(error.type, error.code);
    }
  }

  async createPaymentIntent(
    payload: CreatePaymentIntentDto,
  ): Promise<{
    paymentIntent: Stripe.Response<Stripe.PaymentIntent>;
    ephemeralKey: Stripe.Response<Stripe.EphemeralKey>;
  }> {
    const applicationFeeAmount = Math.round(
      CommonUtils.percentToVal(2.9, +payload.amount) +
        0.3 * 100 +
        CommonUtils.percentToVal(+payload.splitRatio, +payload.amount),
    );

    const platformCommission = Math.round(
      CommonUtils.percentToVal(+payload.splitRatio, +payload.amount),
    );

    const gatewayCommission =
      Math.round(CommonUtils.percentToVal(2.9, +payload.amount)) + 0.3 * 100;

    const ephemeralKey = await this.stripe.ephemeralKeys.create(
      { customer: payload.customerId },
      { apiVersion: '2020-08-27' },
    );

    switch (payload.type) {
      case 'restaurantOrder':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            setup_future_usage: 'on_session',
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              accountId: payload.accountId,
              interval: payload.interval,
              paymentMethodId: payload.paymentMethodId,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited:
                +Math.round(+payload.amount) - applicationFeeAmount,
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }
      case 'deal':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            setup_future_usage: 'on_session',
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'payment':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'luxury':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'groceries':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'events':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: +payload.amount - applicationFeeAmount,
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'events':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: +payload.amount,
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: +payload.amount - applicationFeeAmount,
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      default:
        break;
    }
  }

  async createInitPaymentIntent(
    payload: CreatePaymentIntentDto,
  ): Promise<{
    paymentIntent: Stripe.Response<Stripe.PaymentIntent>;
    ephemeralKey: Stripe.Response<Stripe.EphemeralKey>;
  }> {
    const applicationFeeAmount = Math.round(
      CommonUtils.percentToVal(2.9, +payload.amount) +
        0.3 * 100 +
        CommonUtils.percentToVal(+payload.splitRatio, +payload.amount),
    );

    const platformCommission = Math.round(
      CommonUtils.percentToVal(+payload.splitRatio, +payload.amount),
    );

    const gatewayCommission = Math.round(
      CommonUtils.percentToVal(2.9, +payload.amount),
    );

    const ephemeralKey = await this.stripe.ephemeralKeys.create(
      { customer: payload.customerId },
      { apiVersion: '2020-08-27' },
    );

    switch (payload.type) {
      case 'deal':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            setup_future_usage: 'on_session',
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'payment':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            setup_future_usage: 'on_session',
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'luxury':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: Math.round(
              CommonUtils.percentToVal(2.9, +payload.amount) +
                0.3 * 100 +
                CommonUtils.percentToVal(+payload.splitRatio, +payload.amount),
            ),
            transfer_data: {
              destination: payload.accountId,
            },
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'groceries':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'events':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: Math.round(+payload.amount),
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: Math.round(
                +payload.amount - applicationFeeAmount,
              ),
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'groceries':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: +payload.amount,
            currency: payload.currency,
            payment_method_types: ['card'],
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              gatewayCommission,
              amountCredited: +payload.amount - applicationFeeAmount,
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      case 'events':
        logger.log(payload);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: +payload.amount,
            currency: payload.currency,
            payment_method_types: ['card'],
            application_fee_amount: applicationFeeAmount,
            transfer_data: {
              destination: payload.accountId,
            },
            customer: payload.customerId,
            metadata: {
              beneficiaryName: payload.beneficiary.name,
              beneficiaryProfileId: payload.beneficiary.profileId,
              campaignTitle: payload.campaign.title,
              campaignId: payload.campaign.id,
              senderEmail: payload.senderEmail,
              senderUid: payload.senderUid,
              imageUrl: payload.imageUrl,
              organizationId: payload.organizationId,
              amountSent: Math.round(+payload.amount),
              currency: payload.currency,
              type: payload.type,
              splitRatio: payload.splitRatio,
              platformCommission,
              gatewayCommission,
              amountCredited: +payload.amount - applicationFeeAmount,
            },
          });

          return { paymentIntent, ephemeralKey };
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }

      default:
        break;
    }
  }

  async createPrice(
    payload: CreatePriceDto,
  ): Promise<Stripe.Response<Stripe.Price>> {
    const priceObj = await this.stripe.prices.create({
      product: payload.productId,
      unit_amount: +payload.amount,
      currency: 'usd',
      recurring: {
        interval: payload.interval as Stripe.PriceCreateParams.Recurring.Interval,
      },
    });

    return priceObj;
  }

  async updateCustomerObj(
    customerId: string,
    data: Stripe.CustomerUpdateParams,
  ) {
    const customer = await this.stripe.customers.update(customerId, data);
    return customer;
  }

  async createSetupIntent(payload: CreateSetupIntentDto) {
    const setupIntent = await this.stripe.setupIntents.create({
      payment_method_types: ['card'],
      usage: 'off_session',
      customer: payload.customerId,
    });

    return setupIntent;
  }

  async deleteSubscription(payload: DeleteSubscriptionDto, res: Response) {
    try {
      await firebaseAdmin
        .firestore()
        .collection('subscriptions')
        .doc(payload.subscriptionId)
        .update({ active: false });

      return res.status(HttpStatus.OK);
    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).json(error);
    }
  }

  async createSubscription(
    payload: CreateSubscriptionDto,
    res: Response,
  ): Promise<
    Stripe.Response<Stripe.Subscription> | Response<any, Record<string, any>>
  > {
    try {
      const subscriptionObj = await this.stripe.subscriptions.create({
        customer: payload.customerId,
        metadata: {
          beneficiaryName: payload.beneficiary.name,
          beneficiaryProfileId: payload.beneficiary.profileId,
          campaignTitle: payload.campaign.title,
          campaignId: payload.campaign.id,
          senderEmail: payload.senderEmail,
          senderUid: payload.senderUid,
          imageUrl: payload.imageUrl,
          organizationId: payload.organizationId,
          amountSent: payload.amount,
          currency: payload.currency,
          type: payload.type,
          productId: payload.productId,
          interval: payload.interval,
        },
        items: [
          {
            price_data: {
              unit_amount: +payload.amount,
              currency: payload.currency,
              product: payload.productId,
              recurring: {
                interval: payload.interval as Stripe.SubscriptionCreateParams.Item.PriceData.Recurring.Interval,
              },
            },
          },
        ],
        transfer_data: {
          destination: payload.accountId,
        },
        application_fee_percent: Math.round(
          CommonUtils.percentToVal(2.9, +payload.amount) +
            0.3 * 100 +
            CommonUtils.percentToVal(+payload.splitRatio, +payload.amount),
        ),
        default_payment_method: payload.paymentMethodId,
      });

      return res.status(HttpStatus.OK).json(subscriptionObj);
    } catch (error) {
      logger.error(error);
      return res.status(400).json(error);
    }
  }

  async getListOfPaymentMethods(payload: GetPaymentMethodsDto) {
    try {
      const paymentMethods = await this.stripe.paymentMethods.list({
        customer: payload.customerId,
        type: 'card',
      });

      return paymentMethods;
    } catch (error) {
      logger.error(error);
      throw new HttpException(error.type, error.code);
    }
  }

  handleAccountEvents(payload: any, type: string) {
    logger.info(type);
    logger.info(payload);
  }

  async chargeSavedCard(payload: any, loggedInUserId: string) {
    logger.info(payload);

    let loggedUser: UserRecord;

    return firebaseAdmin
      .auth()
      .getUser(loggedInUserId)
      .then(user => {
        loggedUser = user;
        return firebaseAdmin
          .firestore()
          .collection('users')
          .doc(user.uid)
          .get();
      })
      .then(async userData => {
        const { stripeCustomerId } = userData.data();
        const methodDTO = new GetPaymentMethodsDto();
        methodDTO.customerId = stripeCustomerId;
        methodDTO.type = 'card';

        const methods = await this.getListOfPaymentMethods(methodDTO);

        try {
          const paymentIntent = await this.stripe.paymentIntents.create({
            amount: +payload.amount,
            currency: payload.currency,
            payment_method: methods.data[0].id,
            application_fee_amount: Math.round(
              CommonUtils.percentToVal(2.9, +payload.amount) +
                0.3 * 100 +
                CommonUtils.percentToVal(+3, +payload.amount),
            ),
            /*transfer_data: {
              destination: payload.accountId,
            },*/
            customer: stripeCustomerId,
            off_session: true,
            confirm: true,
          });

          return paymentIntent;
        } catch (error) {
          logger.error(error);

          throw new HttpException(error.type, error.code);
        }
      });
  }

  async createTransfer(
    payload: CreateTransferDto,
  ): Promise<Stripe.Response<Stripe.Transfer>> {
    try {
      const transferObj = await this.stripe.transfers.create({
        amount: +payload.amount,
        currency: payload.currency,
        destination: payload.accountId,
      });

      return transferObj;
    } catch (error) {
      logger.error(error);

      throw new HttpException(error.type, error.code);
    }
  }
}
