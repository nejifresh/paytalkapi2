import {
  Body,
  Controller,
  Post,
  Res,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { StripeService } from './stripe.service';
import { OnboardRequestDto } from './dto/onboard-request.dto';
import { Response } from 'express';
import { CreateCheckoutDto } from './dto/create-checkout.dto';
import { CreatePaymentIntentDto } from './dto/create-payment-intent.dto';
import { CreatePriceDto } from './dto/create-price.dto';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { GetPaymentMethodsDto } from './dto/get-payment-methods.dto';
import { CreateSetupIntentDto } from './dto/create-setup-intent.dto';
import { DeleteSubscriptionDto } from './dto/delete-subscription.dto';
import { GetUid } from '../auth/decorators/getAuthenticatedUser.decorator';
import { CreateTransferDto } from './dto/create-transfer.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { EventPaymentDto } from './dto/event-payment.dto';

@Controller('stripe')
@ApiBearerAuth()
@ApiTags('stripe')
export class StripeController {
  constructor(private readonly stripeService: StripeService) {}

  @Post('onboard')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  initAcctOnboarding(@Body() req: OnboardRequestDto) {
    return this.stripeService.initAccountOnboarding(req);
  }

  @Post('checkout')
  @UsePipes(DTOValidationPipe)
  createCheckoutSession(@Body() payload: CreateCheckoutDto) {
    return this.stripeService.createCheckoutSession(payload);
  }

  @Post('checkout/event-tickets')
  @UsePipes(DTOValidationPipe)
  createEventCheckoutSession(@Body() payload: EventPaymentDto) {
    return this.stripeService.createEventCheckoutSession(payload);
  }

  @Post('create_init_payment_intent')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  createInitPaymentIntent(@Body() payload: CreatePaymentIntentDto) {
    return this.stripeService.createInitPaymentIntent(payload);
  }

  @Post('create_payment_intent')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  createPaymentIntent(@Body() payload: CreatePaymentIntentDto) {
    return this.stripeService.createPaymentIntent(payload);
  }

  @Post('create_price')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  createPrice(@Body() payload: CreatePriceDto) {
    return this.stripeService.createPrice(payload);
  }

  @Post('create_subscription')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  createSubscription(
    @Body() payload: CreateSubscriptionDto,
    @Res() res: Response,
  ) {
    return this.stripeService.createSubscription(payload, res);
  }

  @Post('create_setup_intent')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  createSetupIntent(@Body() payload: CreateSetupIntentDto) {
    return this.stripeService.createSetupIntent(payload);
  }

  @Post('payment_methods/list')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  getListOfPaymentMethods(@Body() payload: GetPaymentMethodsDto) {
    return this.stripeService.getListOfPaymentMethods(payload);
  }

  @Post('accounts_updated_events')
  accountsUpdatedEvents(@Body() payload: any) {
    return this.stripeService.handleAccountEvents(payload, 'accounts_updated');
  }

  @Post('delete_subscription')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  deleteSubscription(
    @Body() payload: DeleteSubscriptionDto,
    @Res() res: Response,
  ) {
    return this.stripeService.deleteSubscription(payload, res);
  }

  @Post('charge_card')
  @UseGuards(AuthGuard)
  chargeSavedCard(@Body() payload: any, @GetUid() loggedInUserId: string) {
    return this.stripeService.chargeSavedCard(payload, loggedInUserId);
  }

  @Post('transfer')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  createPayout(@Body() payload: CreateTransferDto) {
    return this.stripeService.createTransfer(payload);
  }
}
