import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export interface IOnlineTicketSale {
  amount: number;
  currency: string;
  dateCreated: Timestamp;
  qty: number;
  userId: string;
  eventId: string;
  totalAmount: number;
  serviceCharge: number;
  taxAmount: number;
  selectedPrice: SelectedPrice;
  ticketPriceType: string;
}

export interface SelectedPrice {
  price: number;
  title: string;
}
