export interface Customer {
  id: string;
  object: string;
  address: string;
  balance: number;
  created: number;
  currency: string;
  default_source: any;
  delinquent: boolean;
  description: string;
  discount: string;
  email: string;
  invoice_prefix: string;
  invoice_settings: {
    custom_fields: any;
    default_payment_method: any;
    footer: any;
  };
  livemode: boolean;
  metadata: {
    order_id: number;
  };
  name: string;
  next_invoice_sequence: number;
  phone: null;
  preferred_locales: any[];
  shipping: string;
  tax_exempt: any;
}
