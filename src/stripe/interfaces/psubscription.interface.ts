export interface PSubscription {
  active: boolean;
  amount: number;
  campaignId: string;
  currency: string;
  dateSubscribed: any;
  gateway: string;
  organizationId: string;
  paymentInterval: string;
  profileId: string;
  subscriptionId: string;
  userId: string;
}
