import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class EventPaymentDto {


  @ApiProperty()
  @IsDefined()
  attemptId: string;
}
