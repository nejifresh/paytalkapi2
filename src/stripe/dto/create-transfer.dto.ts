import { IsDefined } from 'class-validator';
import {
  Beneficiary,
  Campaign,
} from '../../commons/interfaces/paytalk-transaction.interface';

export class CreateTransferDto {
  @IsDefined()
  amount: string;

  @IsDefined()
  accountId: string;

  @IsDefined()
  currency: string;

  @IsDefined()
  customerId: string;

  @IsDefined()
  organizationId: string;

  @IsDefined()
  senderEmail: string;

  @IsDefined()
  senderUid: string;

  @IsDefined()
  imageUrl: string;

  @IsDefined()
  type: string;
}
