import { IsDefined, IsOptional } from 'class-validator';

export class GetPaymentMethodsDto {
  @IsDefined()
  customerId: string;

  @IsOptional()
  type: string;
}
