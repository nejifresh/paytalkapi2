import { IsDefined, IsOptional } from 'class-validator';
import {
  Beneficiary,
  Campaign,
} from '../../commons/interfaces/paytalk-transaction.interface';

export class CreatePaymentIntentDto {
  @IsDefined()
  amount: string;

  @IsDefined()
  accountId: string;

  @IsDefined()
  currency: string;

  @IsDefined()
  customerId: string;

  @IsDefined()
  beneficiary: Beneficiary;

  @IsDefined()
  campaign: { title: string; id: string };

  @IsDefined()
  organizationId: string;

  @IsDefined()
  senderEmail: string;

  @IsDefined()
  senderUid: string;

  @IsDefined()
  imageUrl: string;

  @IsDefined()
  type: string;

  @IsDefined()
  splitRatio: number;

  @IsOptional()
  productId?: string;
  
  @IsOptional()
  interval?: string;
  
  @IsOptional()
  paymentMethodId?: string;
}
