import { IsDefined } from 'class-validator';

export class CreatePriceDto {
  @IsDefined()
  amount: string;

  @IsDefined()
  productId: string;

  @IsDefined()
  interval: string;
}
