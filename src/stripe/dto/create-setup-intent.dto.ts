import { IsDefined } from 'class-validator';

export class CreateSetupIntentDto {
  @IsDefined()
  customerId: string;
}
