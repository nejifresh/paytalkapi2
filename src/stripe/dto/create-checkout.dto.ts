import { Optional } from '@nestjs/common';
import { IsDefined, IsOptional } from 'class-validator';

export class CreateCheckoutDto {
  @IsDefined()
  accountId: string;

  @IsDefined()
  amount: string;

  @IsDefined()
  currency: string;

  @IsOptional()
  organizationId?: string;

  @IsOptional()
  senderEmail?: string;

  @IsOptional()
  imageUrl?: string;

  @IsOptional()
  type?: string;

  @IsOptional()
  splitRatio?: number;

  @IsOptional()
  beneficiaryName?: string;

  @IsOptional()
  beneficiaryProfileId?: string;

  @IsOptional()
  lineItems?: string;

  @Optional()
  billId?: string;
}

// export interface Invoice {
//   assignedName: string;
//   email: string;
//   price: string;
//   status: string;
// }
