import { IsDefined } from 'class-validator';

export class DeleteSubscriptionDto {
  @IsDefined()
  active: boolean;

  @IsDefined()
  subscriptionId: string;
}
