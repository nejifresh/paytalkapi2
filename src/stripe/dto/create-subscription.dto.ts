import { IsDefined } from 'class-validator';
import { Beneficiary } from '../../commons/interfaces/paytalk-transaction.interface';

export class CreateSubscriptionDto {
  @IsDefined()
  amount: string;

  @IsDefined()
  currency: string;

  @IsDefined()
  interval: string;

  @IsDefined()
  productId: string;

  @IsDefined()
  customerId: string;

  @IsDefined()
  accountId: string;

  @IsDefined()
  splitRatio: number;

  @IsDefined()
  organizationId: string;

  @IsDefined()
  type: string;

  @IsDefined()
  beneficiary: Beneficiary;

  @IsDefined()
  campaign: { title: string; id: string };

  @IsDefined()
  senderEmail: string;

  @IsDefined()
  senderUid: string;

  @IsDefined()
  imageUrl: string;

  @IsDefined()
  paymentMethodId: string;
}
