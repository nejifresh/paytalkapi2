import { IsDefined } from 'class-validator';

export class OnboardRequestDto {
  @IsDefined()
  name: string;

  @IsDefined()
  address: string;

  @IsDefined()
  phoneNo: string;

  @IsDefined()
  email: string;

  @IsDefined()
  website: string;

  @IsDefined()
  profileId: string;

  @IsDefined()
  organizationId: string;
}
