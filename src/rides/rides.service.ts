import { HttpStatus, Injectable, Logger, Res } from '@nestjs/common';
import { CreateRideDto } from './dto/create-ride.dto';
import { Response } from 'express';
import { firebaseAdmin, logger } from '../commons/firebaseConfig';
import { Request } from './entities/request.entity';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { ErrorResponseDto } from '../commons/dto/errorResponse.dto';
import GeoPoint from 'geo-point';

@Injectable()
export class RidesService {

  requestRide(createRideDto: CreateRideDto,
              @Res() response: Response,
  ): Promise<Response> {

    const startPoint = new GeoPoint(createRideDto.pickUpLatitude, createRideDto.pickUpLongitude);
    const endPoint = new GeoPoint(createRideDto.destinationLatitude, createRideDto.destinationLongitude);

    const distance = startPoint.calculateDistance(endPoint) / 1000;

    const amount = distance * 1.4;

    const rideRequest = new Request(createRideDto, distance, amount);

    return firebaseAdmin
      .firestore()
      .collection('rideRequests')
      .add({ ...rideRequest })
      .then(docRef => {

        return response
          .status(HttpStatus.CREATED)
          .json(new SuccessResponseDto(docRef.id, HttpStatus.CREATED));

      }).catch(error => {
        logger.error(error)
        Logger.error(`An Error Occurred`, error, 'Rides Service');
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(
            new SuccessResponseDto(
              `Error Creating Ride Request ${error.message}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            ),
          );
      });
  }

  getRequestStatus(requestId: string,
              @Res() response: Response,
  ): Promise<Response> {


    return firebaseAdmin
      .firestore()
      .collection('rideRequests')
      .doc(requestId)
      .get()
      .then(docRef => {

        const {status} = docRef.data()

        return response
          .status(HttpStatus.CREATED)
          .json(new SuccessResponseDto(status, HttpStatus.CREATED));

      }).catch(error => {
        logger.error(error)
        Logger.error(`An Error Occurred`, error, 'Rides Service');
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(
            new ErrorResponseDto(
              `Error getting Ride Request Status ${error.message}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            ),
          );
      });
  }

}
