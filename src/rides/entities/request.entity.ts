import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';
import firebase from 'firebase';
import { adminTypes } from '../../commons/firebaseConfig';
import { CreateRideDto } from '../dto/create-ride.dto';
import GeoPoint = firebase.firestore.GeoPoint;
import Timestamp = firebase.firestore.Timestamp;

export class Request {

  @ApiProperty()
  @IsDefined()
  currency: string;

  @ApiProperty()
  @IsDefined()
  customerId: string;

  @ApiProperty()
  @IsDefined()
  destination: GeoPoint;

  @ApiProperty()
  @IsDefined()
  destinationAddress: string;


  @ApiProperty()
  @IsDefined()
  pickupLocation: GeoPoint;

  @ApiProperty()
  @IsDefined()
  pickupAddress: string;

  @ApiProperty()
  @IsDefined()
  status: string;

  @ApiProperty()
  @IsDefined()
  distance: number;

  @ApiProperty()
  @IsDefined()
  amount: number;

  @ApiProperty()
  @IsDefined()
  requestDateTime: Timestamp;


  constructor(createRideDto: CreateRideDto, distance: number, amount: number) {
    this.currency = createRideDto.currency;
    this.customerId = createRideDto.customerId;
    this.destination = new adminTypes.firestore.GeoPoint(createRideDto.destinationLatitude, createRideDto.destinationLongitude);
    this.destinationAddress = createRideDto.destinationAddress;
    this.pickupLocation = new adminTypes.firestore.GeoPoint(createRideDto.pickUpLatitude, createRideDto.pickUpLongitude);
    this.pickupAddress = createRideDto.pickUpAddress;
    this.status = 'PENDING';
    this.requestDateTime = adminTypes.firestore.Timestamp.now();
    this.amount = amount;
    this.distance = distance;
  }
}
