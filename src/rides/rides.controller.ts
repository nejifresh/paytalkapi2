import { Body, Controller, Get, Param, Post, Res } from '@nestjs/common';
import { RidesService } from './rides.service';
import { CreateRideDto } from './dto/create-ride.dto';
import { Response } from 'express';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@Controller('rides')
@ApiTags('rides')
export class RidesController {
  constructor(private readonly ridesService: RidesService) {}

  @Post("request")
  @ApiOperation({ summary: 'Create Ride Request' })
  create(@Body() createRideDto: CreateRideDto,@Res() response: Response) {
    return this.ridesService.requestRide(createRideDto,response);
  }

  @Get("request/:id")
  @ApiOperation({ summary: 'Get Ride Request Status' })
  getRideStatus(@Param('id') id: string,@Res() response: Response) {
    return this.ridesService.getRequestStatus(id,response);
  }

}
