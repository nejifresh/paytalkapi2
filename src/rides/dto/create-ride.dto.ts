import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class CreateRideDto {

  @ApiProperty()
  @IsDefined()
  currency: string;

  @ApiProperty()
  @IsDefined()
  customerId: string;

  @ApiProperty()
  @IsDefined()
  destinationLongitude: number;

  @ApiProperty()
  @IsDefined()
  destinationLatitude: number;

  @ApiProperty()
  @IsDefined()
  destinationAddress: string;

  @ApiProperty()
  @IsDefined()
  pickUpLongitude: number;


  @ApiProperty()
  @IsDefined()
  pickUpLatitude: number;

  @ApiProperty()
  @IsDefined()
  pickUpAddress: string;

}
