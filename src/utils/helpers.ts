
export class Helpers {
  // eslint-disable-next-line @typescript-eslint/ban-types
  static recursiveTrim(value: object): any {
    return JSON.parse(JSON.stringify(value).replace(/"\s+|\s+"/g, '"'));
  }

}
