import { Controller, Post, Body } from '@nestjs/common';
import { WebhooksService } from './webhooks.service';

@Controller('webhooks')
export class WebhooksController {
  constructor(private readonly webhooksService: WebhooksService) {}

  @Post('stripe/payment-intent-events')
  handlePaymentIntentEvents(@Body() payload: any) {
    switch (payload.type) {
      case 'payment_intent.succeeded':
        return this.webhooksService.handlePaymentIntentSuccess(payload);
      default:
        return null;
    }
  }

  @Post('stripe/subscription-events')
  handleSubscriptionEvents(@Body() payload: any) {
    return this.webhooksService.handleSubscriptionEvents(payload);
  }

  @Post('stripe/transfer-events')
  handleTransferEvents(@Body() payload: any) {
    switch (payload.type) {
      case 'transfer.created':
        return this.webhooksService.handleTranserEvents(payload);
      default:
        break;
    }
  }

  @Post('stripe/checkout-events')
  handleCheckoutEvents(@Body() payload: any) {
    return this.webhooksService.handleCheckoutEvents(payload);

    // switch (payload.type) {
    //   case 'checkout.session.async_payment_succeeded':
    //     return this.webhooksService.handleCheckoutEvents(payload);
    //   default:
    //     break;
    // }
  }
  
  @Post('revenuecat/payment-events')
  handleRevenuecatPaymentEvents(@Body() payload: any) {
    return this.webhooksService.handleRevenuecatPaymentEvents(payload);
  }
}
