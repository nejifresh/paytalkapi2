export interface Bill {
  creationDate: any;
  currency: string;
  id: string;
  orgId: string;
  paymentConfirmerId: string;
  profileId: string;
  status: string;
  tableNumber: number;
  totalAmount: number;
  type: string;
  invoices: Invoice[];
}

export interface Invoice {
  assignedName: string;
  email: string;
  price: string;
  status: string;
}
