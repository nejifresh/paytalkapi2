import { HttpException, Injectable } from '@nestjs/common';
import { adminTypes, firebaseAdmin, logger } from '../commons/firebaseConfig';
import { Transaction } from '../transactions/entities/transaction.entity';
import { AuditTrail } from '../wallets/entities/audit-trail.entity';
import { WalletHistory } from '../wallets/entities/wallet-history.entity';
import { CreditWalletResponse } from '../wallets/dto/credit-wallet-response.dto';
import { DebitWalletResponse } from '../wallets/dto/debit-wallet-response.dto';
import { Bill, Invoice } from './entities/bill.interface';
import { Wallet } from '../wallets/entities/wallet.entity';
import { CommonUtils } from '../commons/utils/common-utils.class';
import { IWallet } from '../wallets/wallet.interface';
import { IOnlineTicketSale } from '../stripe/interfaces/onlineTicketSale.interface';
import { uuid } from '@supercharge/strings';

@Injectable()
export class WebhooksService {
  async handlePaymentIntentSuccess(payload: any) {
    logger.info(payload.data.object.id);
    let transaction: Transaction;

    switch (payload.data.object.metadata.type) {
      case 'restaurantOrder':
        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          accountId: payload.data.object.metadata.accountId,
          customerId: payload.data.object.metadata.customerId,
          interval: payload.data.object.metadata.interval,
          productId: payload.data.object.metadata.productId,
          paymentMethodId: payload.data.object.metadata.paymentMethodId,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
          splitRatio: +payload.data.object.metadata.splitRatio,
          platformCommission:
            +payload.data.object.metadata.platformCommission / 100,
          gatewayCommission:
            +payload.data.object.metadata.gatewayCommission / 100,
          amountCredited: +payload.data.object.metadata.amountCredited / 100,
        };

        logger.info(transaction);

        this._createTransactionRecord(transaction, payload.data.object.id);
        break;
      case 'payment':
        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
          splitRatio: +payload.data.object.metadata.splitRatio,
          platformCommission:
            +payload.data.object.metadata.platformCommission / 100,
          gatewayCommission:
            +payload.data.object.metadata.gatewayCommission / 100,
          amountCredited: +payload.data.object.metadata.amountCredited / 100,
        };

        this._createTransactionRecord(transaction, payload.data.object.id);

        break;
      case 'luxury':
        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
          splitRatio: +payload.data.object.metadata.splitRatio,
          platformCommission:
            +payload.data.object.metadata.platformCommission / 100,
          gatewayCommission:
            +payload.data.object.metadata.gatewayCommission / 100,
          amountCredited: +payload.data.object.metadata.amountCredited / 100,
        };

        this._createTransactionRecord(transaction, payload.data.object.id);

        break;
      case 'deal':
        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
          splitRatio: +payload.data.object.metadata.splitRatio,
          platformCommission:
            +payload.data.object.metadata.platformCommission / 100,
          gatewayCommission:
            +payload.data.object.metadata.gatewayCommission / 100,
          amountCredited: +payload.data.object.metadata.amountCredited / 100,
        };

        this._createTransactionRecord(transaction, payload.data.object.id);

        break;
      case 'credit':
        const cAmount = +payload.data.object.metadata.amount / 100;
        const cCurrency = payload.data.object.metadata.currency;
        const cType = payload.data.object.metadata.type;
        const cUserId = payload.data.object.metadata.userId;
        const cWalletId = payload.data.object.metadata.walletId;
        const cTransactionId = 'change-me';
        const cSource = 'demo';

        const creditRes = await this._creditWallet(cAmount, cUserId, cWalletId);
        this._updateWalletHistory(
          cUserId,
          cAmount,
          cType,
          creditRes.prevBal,
          creditRes.currBal,
          cCurrency,
        );
        this._updateAuditTrail(
          cAmount,
          creditRes.prevBal,
          creditRes.currBal,
          cCurrency,
          cUserId,
          cTransactionId,
          cSource,
          cType,
        );

        break;
      case 'groceries':
        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
          gatewayCommission:
            +payload.data.object.metadata.gatewayCommission / 100,
          amountCredited: +payload.data.object.metadata.amountCredited / 100,
        };

        this._createTransactionRecord(transaction, payload.data.object.id);
      // case 'events':
      //   transaction = {
      //     beneficiary: {
      //       name: payload.data.object.metadata.beneficiaryName,
      //       profileId: payload.data.object.metadata.beneficiaryProfileId,
      //     },
      //     campaign: {
      //       title: payload.data.object.metadata.campaignTitle,
      //       id: payload.data.object.metadata.campaignId,
      //     },
      //     amount: +payload.data.object.metadata.amountSent / 100,
      //     currency: payload.data.object.metadata.currency,
      //     dateOfTransaction: adminTypes.firestore.Timestamp.now(),
      //     status: 'success',
      //     gateway: 'stripe',
      //     senderEmail: payload.data.object.metadata.senderEmail,
      //     senderUid: payload.data.object.metadata.senderUid,
      //     imageUrl: payload.data.object.metadata.imageUrl,
      //     organizationId: payload.data.object.metadata.organizationId,
      //     transactionId: '',
      //     type: payload.data.object.metadata.type,
      //     splitRatio: +payload.data.object.metadata.splitRatio,
      //     platformCommission:
      //       +payload.data.object.metadata.platformCommission / 100,
      //     gatewayCommission:
      //       +payload.data.object.metadata.gatewayCommission / 100,
      //     amountCredited: +payload.data.object.metadata.amountCredited / 100,
      //   };

      case 'events':
        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
          splitRatio: +payload.data.object.metadata.splitRatio,
          platformCommission:
            +payload.data.object.metadata.platformCommission / 100,
          gatewayCommission:
            +payload.data.object.metadata.gatewayCommission / 100,
          amountCredited: +payload.data.object.metadata.amountCredited / 100,
        };

      case 'events':
        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
          splitRatio: +payload.data.object.metadata.splitRatio,
          platformCommission:
            +payload.data.object.metadata.platformCommission / 100,
          gatewayCommission:
            +payload.data.object.metadata.gatewayCommission / 100,
          amountCredited: +payload.data.object.metadata.amountCredited / 100,
        };

        this._createTransactionRecord(transaction, payload.data.object.id);

      default:
        break;
    }
  }

  async handleTranserEvents(payload: any) {
    let transaction: Transaction;

    switch (payload.data.object.metadata.type) {
      case 'debit':
        const dAmount = +payload.data.object.metadata.amount;
        const dCurrency = payload.data.object.metadata.currency;
        const dItemType = payload.data.object.metadata.itemType;
        const type = payload.data.object.metadata.type;
        const dSenderId = payload.data.object.metadata.senderId;
        const dWalletId = payload.data.object.metadata.senderWalletId;
        const dTransactionId = payload.data.object.metadata.transactionId;
        const dSource = 'web';
        const senderEmail = payload.data.object.metadata.senderEmail;
        const beneficiaryId = payload.data.object.metadata.recepientId;
        const imageUrl = payload.data.object.metadata.imageUrl;
        const organizationId = payload.data.object.metadata.organizationId;
        const amountInCAD = payload.data.object.metadata.amountInCAD;
        const gatewayCommission =
          payload.data.object.metadata.gatewayCommission;
        const platformCommission =
          payload.data.object.metadata.platformCommission;
        const totalAmount = payload.data.object.metadata.totalAmount;

        const beneficiaryName = await (
          await firebaseAdmin
            .firestore()
            .collection('organizations')
            .doc(organizationId)
            .collection('profiles')
            .doc(beneficiaryId)
            .get()
        ).get('name');

        transaction = {
          beneficiary: {
            name: beneficiaryName,
            profileId: beneficiaryId,
          },
          amount: dAmount,
          currency: dCurrency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: senderEmail,
          senderUid: dSenderId,
          imageUrl: imageUrl,
          organizationId: organizationId,
          transactionId: '',
          type,
          platformCommission,
          gatewayCommission,
          amountCredited: totalAmount,
        };

        // const debitRes = await this._debitWallet(
        //   dAmount,
        //   dSenderId,
        //   dWalletId,
        //   dTransactionId,
        // );

        this._createTransactionRecord(transaction, dTransactionId);

        // this._updateWalletHistory(
        //   dSenderId,
        //   dAmount,
        //   dItemType,
        //   debitRes.prevBal,
        //   debitRes.currBal,
        //   dCurrency,
        // );

        // this._updateAuditTrail(
        //   dAmount,
        //   debitRes.prevBal,
        //   debitRes.currBal,
        //   dCurrency,
        //   dSenderId,
        //   dTransactionId,
        //   dSource,
        //   dItemType,
        // );

        break;
      default:
        break;
    }
  }

  async handleSubscriptionEvents(payload: any) {
    logger.info(payload);

    let subscription: any;
    let transaction: Transaction;

    switch (payload.data.object.object) {
      case 'subscription':
        subscription = {
          subscriptionId: payload.data.object.id,
          campaignId: payload.data.object.metadata.campaignId,
          profileId: payload.data.object.metadata.beneficiaryProfileId,
          paymentInterval: payload.data.object.metadata.interval,
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateSubscribed: adminTypes.firestore.Timestamp.now(),
          active: true,
          gateway: 'stripe',
          userId: payload.data.object.metadata.senderUid,
          organizationId: payload.data.object.metadata.organizationId,
          planId: payload.data.object.id,
          planName: payload.data.object.metadata.campaignTitle,
        };

        transaction = {
          beneficiary: {
            name: payload.data.object.metadata.beneficiaryName,
            profileId: payload.data.object.metadata.beneficiaryProfileId,
          },
          campaign: {
            title: payload.data.object.metadata.campaignTitle,
            id: payload.data.object.metadata.campaignId,
          },
          amount: +payload.data.object.metadata.amountSent / 100,
          currency: payload.data.object.metadata.currency,
          dateOfTransaction: adminTypes.firestore.Timestamp.now(),
          status: 'success',
          gateway: 'stripe',
          senderEmail: payload.data.object.metadata.senderEmail,
          senderUid: payload.data.object.metadata.senderUid,
          imageUrl: payload.data.object.metadata.imageUrl,
          organizationId: payload.data.object.metadata.organizationId,
          transactionId: '',
          type: payload.data.object.metadata.type,
        };
        this._createTransactionRecord(transaction);
        this._createSubscriptionRecord(subscription);

      default:
        break;
    }
  }

  async handleCheckoutEvents(payload: any) {
    if (payload.data.object.metadata.type === 'Event_Sales') {
      const attemptDoc = await firebaseAdmin
        .firestore()
        .collection('onlineTicketSales')
        .doc(payload.data.object.metadata.saleId)
        .get();

      const attempt = attemptDoc.data() as IOnlineTicketSale;

      const newEventTicket = await firebaseAdmin
        .firestore()
        .collection('eventTickets')
        .add({
          code: uuid(),
          currency: attempt.currency,
          datePurchased: adminTypes.firestore.Timestamp.now(),
          eventId: attempt.eventId,
          purchased: attempt.qty,
          ticketAmount: attempt.totalAmount,
          ticketFeeAmount: attempt.serviceCharge,
          ticketTaxAmount: attempt.taxAmount,
          userId: attempt.userId,
          eventName: payload.data.object.metadata.eventName,
          admitted: false,
          numberAdmitted: 0,
          selectedPrice: attempt.selectedPrice,
        });

      const transaction = {
        beneficiary: {
          // name: payload.data.object.metadata.beneficiaryName,
          profileId: payload.data.object.metadata.beneficiaryProfileId,
        },
        amount: +payload.data.object.metadata.amountSent,
        currency: payload.data.object.metadata.currency,
        dateOfTransaction: adminTypes.firestore.Timestamp.now(),
        status: 'success',
        gateway: 'stripe',
        senderEmail: payload.data.object.metadata.senderEmail,
        senderUid: attempt.userId,
        organizationId: payload.data.object.metadata.organizationId,
        transactionId: '',
        type: payload.data.object.metadata.type,
        gatewayCommission: +payload.data.object.metadata.gatewayCommission,
      };

      try {
        const docRef = await firebaseAdmin
          .firestore()
          .collection('transactions2')
          .add(transaction);

        const transactionId = docRef.id;

        await docRef.update({ transactionId });
        await newEventTicket.update({ transactionId });
      } catch (error) {
        logger.error(error);

        throw new HttpException(error.type, error.code);
      }
    } else {
      const billId = payload.data.object.metadata.billId;
      const invoiceIds = (payload.data.object.metadata
        .lineItems as string).split(',');

      const transaction: Transaction = {
        beneficiary: {
          name: payload.data.object.metadata.beneficiaryName,
          profileId: payload.data.object.metadata.beneficiaryProfileId,
        },
        campaign: {
          title: payload.data.object.metadata.campaignTitle,
          id: payload.data.object.metadata.campaignId,
        },
        amount: +payload.data.object.metadata.amountSent / 100,
        currency: payload.data.object.metadata.currency,
        dateOfTransaction: adminTypes.firestore.Timestamp.now(),
        status: 'success',
        gateway: 'stripe',
        senderEmail: payload.data.object.metadata.senderEmail,
        senderUid: '',
        imageUrl: payload.data.object.metadata.imageUrl,
        organizationId: payload.data.object.metadata.organizationId,
        transactionId: '',
        type: payload.data.object.metadata.type,
        splitRatio: +payload.data.object.metadata.splitRatio,
        platformCommission:
          +payload.data.object.metadata.platformCommission / 100,
        gatewayCommission:
          +payload.data.object.metadata.gatewayCommission / 100,
        amountCredited: +payload.data.object.metadata.amountCredited / 100,
      };

      try {
        const docRef = await firebaseAdmin
          .firestore()
          .collection('transactions2')
          .add(transaction);

        const transactionId = docRef.id;

        await docRef.update({ transactionId });
      } catch (error) {
        logger.error(error);

        throw new HttpException(error.type, error.code);
      }

      try {
        const docRef = await firebaseAdmin
          .firestore()
          .collection('bills')
          .doc(billId);

        const queryRef = await docRef.get();

        const doc = queryRef.data() as Bill;

        const fetchedInvoices = [...doc.invoices];

        logger.info('fetched invoices', fetchedInvoices);

        const updatedInvoices: Invoice[] = [];

        fetchedInvoices.forEach(invoice => {
          if (invoiceIds.includes(invoice.assignedName)) {
            invoice.status = 'PAID';
            updatedInvoices.push(invoice);
          } else {
            updatedInvoices.push(invoice);
          }
        });

        logger.info('updated invoices', updatedInvoices);

        await docRef.update({ invoices: updatedInvoices });
      } catch (error) {
        logger.error(error);

        throw new HttpException(error.type, error.code);
      }
    }
  }

  handleRevenuecatPaymentEvents(payload: any) {
    logger.debug(payload);
  }

  private async _createTransactionRecord(
    transaction: Transaction,
    id?: string,
  ) {
    if (id) {
      transaction.transactionId = id;

      await firebaseAdmin
        .firestore()
        .collection('transactions2')
        .doc(id)
        .set(transaction);
    } else {
      const docRef = await firebaseAdmin
        .firestore()
        .collection('transactions2')
        .add(transaction);

      const transactionId = docRef.id;

      await docRef.update({ transactionId });

      // const orgSnapshot = await firebaseAdmin
      //   .firestore()
      //   .collection('bills')
      //   .where('orgId', '==', `${transaction.organizationId}`)
      //   .where('profileId', '==', `${transaction.beneficiary.profileId}`)
      //   .get();

      // const docs = orgSnapshot.docs;
      // const data = docs.
    }
  }

  async _creditWallet(
    amount: number,
    userId: string,
    walletId: string,
  ): Promise<CreditWalletResponse> {
    const walletRef = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(userId)
      .collection('wallet')
      .doc(walletId);

    const balance = await (await walletRef.get()).get('balance');
    const updatedBalance = +balance + amount;
    const writeResult = await walletRef.update({ balance: updatedBalance });

    const res = {
      prevBal: balance,
      currBal: updatedBalance,
      time: writeResult.writeTime,
    };
    return res;
  }

  async _debitWallet(
    amount: number,
    userId: string,
    walletId: string,
    transactionId: string,
  ): Promise<DebitWalletResponse> {
    const walletRef = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(userId)
      .collection('wallet')
      .doc(walletId);

    const userWallet = (await walletRef.get()).data() as IWallet;

    const balance = userWallet.balance; //await (await walletRef.get()).get('balance');
    console.log(balance);
    logger.log(balance);

    const updatedBalance = CommonUtils.roundToTwoDecimalPlaces(
      +balance - amount,
    );
    console.log(updatedBalance);
    logger.log(updatedBalance);

    const writeResult = await walletRef.update({ balance: updatedBalance });

    const res = {
      prevBal: balance,
      currBal: updatedBalance,
      dateTime: writeResult.writeTime,
      transactionId,
    };

    return res;
  }

  async _updateWalletHistory(
    userId: string,
    amount: number,
    type: string,
    prevBal: number,
    currBal: number,
    currency: string,
  ) {
    const walletHistory: WalletHistory = {
      userId,
      amount,
      type,
      prevBal,
      currBal,
      currency,
      dateTime: adminTypes.firestore.Timestamp.now(),
    };

    try {
      const walletRef = await firebaseAdmin
        .firestore()
        .collection('walletHistory')
        .add(walletHistory);

      return walletRef;
    } catch (error) {
      throw new HttpException(error.type, error.code);
    }
  }

  async _updateAuditTrail(
    amount: number,
    oldBalance: number,
    newBalance: number,
    currency: string,
    user: string,
    transactionId: string,
    source: string,
    type: string,
  ) {
    const auditTrail: AuditTrail = {
      oldBalance,
      newBalance,
      user,
      transactionId,
      source,
      type,
      amount,
      currency,
      dateTime: adminTypes.firestore.Timestamp.now(),
    };

    try {
      const auditRef = await firebaseAdmin
        .firestore()
        .collection('audit')
        .add(auditTrail);

      return auditRef;
    } catch (error) {
      throw new HttpException(error.type, error.code);
    }
  }

  private async _createSubscriptionRecord(subscription: any) {
    const res = await firebaseAdmin
      .firestore()
      .collection('subscriptions')
      .doc(subscription.subscriptionId)
      .set(subscription);

    return res;
  }
}
