import { ApiProperty } from '@nestjs/swagger';

export class CreditWalletResponse {
  @ApiProperty()
  prevBal: number;

  @ApiProperty()
  currBal: number;

  @ApiProperty()
  time: any;
}
