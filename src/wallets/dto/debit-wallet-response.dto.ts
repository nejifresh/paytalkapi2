import { ApiProperty } from '@nestjs/swagger';

export class DebitWalletResponse {
  @ApiProperty()
  prevBal: number;

  @ApiProperty()
  currBal: number;

  @ApiProperty()
  dateTime: any;

  @ApiProperty()
  transactionId: string;
}
