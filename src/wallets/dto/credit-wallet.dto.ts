import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNumber, IsOptional } from 'class-validator';

export class CreditWalletDto {
  @IsDefined()
  @IsNumber()
  @ApiProperty()
  amount: number;

  @IsDefined()
  @ApiProperty()
  currency: string;

  @IsDefined()
  @ApiProperty()
  userId: string;

  @IsOptional()
  @ApiProperty()
  customerId?: string;
}
