import { IsDefined, IsOptional } from 'class-validator';

export class DebitWalletDto {
  @IsDefined()
  userId: string;

  @IsDefined()
  amount: number;

  @IsDefined()
  recepientId: string;

  @IsDefined()
  currency: string;

  @IsOptional()
  items: DebitItem[];

  @IsDefined()
  itemType: string;

  @IsDefined()
  userEmail: string;

  @IsDefined()
  imageUrl: string;

  @IsDefined()
  organizationId: string;

  @IsDefined()
  recepientStripeAccount: string;

  @IsDefined()
  walletCurrency: string;
}

class DebitItem {
  description: string;
  name?: string;
  price: number;
  currency: string;
}
