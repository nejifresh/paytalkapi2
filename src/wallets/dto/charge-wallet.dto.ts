import { IsDefined, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ChargeWalletDto {
  @IsDefined()
  @ApiProperty()
  userId: string;

  @IsDefined()
  @ApiProperty()
  amount: number;

  @IsOptional()
  @ApiProperty()
  currency: string;


  constructor(userId: string, amount: number, currency: string) {
    this.userId = userId;
    this.amount = amount;
    this.currency = currency;
  }
}

