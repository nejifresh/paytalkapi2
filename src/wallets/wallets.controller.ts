import {
  Body,
  Controller,
  Param,
  Post,
  Res,
  UsePipes,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { WalletsService } from './wallets.service';
import { CreditWalletDto } from './dto/credit-wallet.dto';
import { Response } from 'express';
import { ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreditWalletResponse } from './dto/credit-wallet-response.dto';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { DebitWalletDto } from './dto/debit-wallet.dto';
import { DebitWalletResponse } from './dto/debit-wallet-response.dto';
import { ChargeWalletDto } from './dto/charge-wallet.dto';

@Controller('wallets')
@ApiTags('wallets')
export class WalletsController {
  constructor(private readonly walletsService: WalletsService) {}

  @Post(':walletId')
  @UsePipes(DTOValidationPipe)
  @ApiOperation({ summary: 'Credit Wallet' })
  @ApiCreatedResponse({
    description: 'Credit Wallet Response',
    type: CreditWalletResponse,
  })
  addFunds(
    @Param('walletId') walletId: string,
    @Body() addFundsDto: CreditWalletDto,
  ) {
    if (walletId) {
      return this.walletsService.addFunds(walletId, addFundsDto);
    } else {
      throw new HttpException(
        'Bad Request: Missing walletId parameter!',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  @Post(':walletId/debit')
  @UsePipes(DTOValidationPipe)
  @ApiOperation({ summary: 'Debit Wallet' })
  @ApiCreatedResponse({
    description: 'Debit Wallet Response',
    type: DebitWalletResponse,
  })
  debitWallet(
    @Param('walletId') walletId: string,
    @Body() debitWalletDto: DebitWalletDto,
    @Res() response: Response,
  ) {
    if (walletId) {
      return this.walletsService.debitWallet(
        walletId,
        debitWalletDto,
        response,
      );
    } else {
      throw new HttpException(
        'Bad Request: Missing walletId parameter!',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  @Post('user/charge')
  @UsePipes(DTOValidationPipe)
  @ApiOperation({ summary: 'Charge Wallet' })
  @ApiCreatedResponse({
    description: 'Debit Wallet Response',
    type: DebitWalletResponse,
  })
  chargeWallet(
    @Body() chargeWalletDto: ChargeWalletDto,
    @Res() response: Response,
  ) {
    return this.walletsService.chargeWallet(chargeWalletDto, response);
  }
}
