import { Module } from '@nestjs/common';
import { WalletsService } from './wallets.service';
import { WalletsController } from './wallets.controller';
import { WebhooksService } from '../webhooks/webhooks.service';

@Module({
  controllers: [WalletsController],
  providers: [WalletsService, WebhooksService],
  exports: [WalletsService],
})
export class WalletsModule {}
