export interface IWallet {
  balance: number;
  currency: string;
}
