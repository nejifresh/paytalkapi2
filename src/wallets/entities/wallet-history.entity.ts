export class WalletHistory {
  amount: number;
  type: string;
  prevBal: number;
  currBal: number;
  userId: string;
  currency: string;
  dateTime: any;
}
