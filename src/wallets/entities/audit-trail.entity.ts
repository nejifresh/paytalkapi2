export class AuditTrail {
  amount: number;
  currency: string;
  dateTime: any;
  newBalance: number;
  oldBalance: number;
  source: string;
  transactionId: string;
  type: string;
  user: string;
}
