import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreditWalletDto } from './dto/credit-wallet.dto';
import { Stripe } from 'stripe';
import { adminTypes, firebaseAdmin, logger } from '../commons/firebaseConfig';
import { Response } from 'express';
import { DebitWalletDto } from './dto/debit-wallet.dto';
import axios, { AxiosResponse } from 'axios';
import { ChargeWalletDto } from './dto/charge-wallet.dto';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { IWallet } from './wallet.interface';
import { CommonUtils } from '../commons/utils/common-utils.class';
import { APLCurrConversionResponse } from 'src/commons/interfaces/apl-curr-conversion-response.interface';
import { WebhooksService } from '../webhooks/webhooks.service';

@Injectable()
export class WalletsService {
  stripe: Stripe;

  constructor(private webhookService: WebhooksService) {
    this.stripe = new Stripe(process.env.stripeSecretKey, {
      apiVersion: '2020-08-27',
      typescript: true,
    });
  }

  async addFunds(
    walletId: string,
    payload: CreditWalletDto,
  ): Promise<{
    paymentIntent: Stripe.Response<Stripe.PaymentIntent>;
    ephemeralKey: Stripe.Response<Stripe.EphemeralKey>;
  }> {
    const type = 'credit';

    const ephemeralKey = await this.stripe.ephemeralKeys.create(
      { customer: payload.customerId },
      { apiVersion: '2020-08-27' },
    );

    try {
      const paymentIntent = await this.stripe.paymentIntents.create({
        amount: Math.round(+payload.amount),
        currency: payload.currency,
        payment_method_types: ['card'],
        customer: payload.customerId,
        metadata: {
          userId: payload.userId,
          amount: Math.round(payload.amount),
          currency: payload.currency,
          type: type,
          walletId: walletId,
        },
      });
      logger.info(paymentIntent, ephemeralKey);

      return { paymentIntent, ephemeralKey };
    } catch (error) {
      logger.error(error);

      throw new HttpException(error.type, error.code);
    }
  }

  async debitWallet(
    walletId: string,
    payload: DebitWalletDto,
    response: Response,
  ) {
    let amountInCAD: number;
    let amountInCADCents: number;
    let transactionId: string;
    // let amountInWalletCurrency: number;

    logger.info(`Payload: ${payload}`);

    const amount = payload.amount;

    // Create Wallet Reference
    const walletRef = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(payload.userId)
      .collection('wallet')
      .doc(walletId);

    // Get Wallet Balance
    const balance = await (await walletRef.get()).get('balance');
    logger.info(`Balance: ${balance}`);

    const amountInWalletCurrency = await (
      await this._convertCurrency(
        payload.currency.toLowerCase(),
        payload.walletCurrency.toLowerCase(),
        amount,
      )
    ).data.result;
    logger.info(`Amount in Wallet Currency: ${amountInWalletCurrency}`);

    // Check if balance is sufficient
    if (+balance > amount) {
      // Get amount in CAD
      amountInCAD = await (
        await this._convertCurrency(payload.currency, 'cad', amount)
      ).data.result;
      logger.info(`Amount in CAD: ${amountInCAD}`);

      // amountInWalletCurrency = await (
      //   await this._convertCurrency('cad', payload.walletCurrency, amountInCAD)
      // ).data.result;
      // logger.info(`Amount in Wallet Currency: ${amountInWalletCurrency}`);

      // Convert CAD to cents
      amountInCADCents = amountInCAD * 100;
      logger.info(`Amount in CAD Cents: ${amountInCADCents}`);

      const stripeBalanceResp = await this.stripe.balance.retrieve();
      logger.info('Stripe Balance', stripeBalanceResp);

      // const stripeBalance = stripeBalanceResp.available[0].amount;

      const userWallet = (await walletRef.get()).data() as IWallet;

      const balance = userWallet.balance; //await (await walletRef.get()).get('balance');
      logger.info('Wallet Balance', balance);

      const updatedBalance = CommonUtils.roundToTwoDecimalPlaces(
        +balance - amountInWalletCurrency,
      );

      logger.info('Updated Wallet Balance', updatedBalance);

      await walletRef.update({
        balance: updatedBalance,
      });

      const beneficiaryName = await (
        await firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(payload.organizationId)
          .collection('profiles')
          .doc(payload.recepientId)
          .get()
      ).get('name');

      const transaction = {
        beneficiary: {
          name: beneficiaryName,
          profileId: payload.recepientId,
        },
        amount: payload.amount,
        currency: payload.currency,
        dateOfTransaction: adminTypes.firestore.Timestamp.now(),
        status: 'success',
        gateway: 'stripe',
        senderEmail: payload.userEmail,
        senderUid: payload.userId,
        imageUrl: payload.imageUrl,
        organizationId: payload.organizationId,
        transactionId: '',
        type: 'debit',
        platformCommission: CommonUtils.roundToTwoDecimalPlaces(
          this._getPaytalkFees(amount),
        ),
        gatewayCommission: 0,
        amountCredited: payload.amount,
      };

      // Update Transaction Records
      const docRef = await firebaseAdmin
        .firestore()
        .collection('transactions2')
        .add(transaction);

      const transactionId = docRef.id;

      await docRef.update({ transactionId });

      // this.webhookService._createTransactionRecord(transaction);

      // if (stripeBalance > Math.floor(amountInCADCents)) {
      //   const transactionRef = firebaseAdmin
      //     .firestore()
      //     .collection('transactions2')
      //     .doc();
      //   transactionId = transactionRef.id;

      //   try {
      //     const transfer = await this.stripe.transfers.create({
      //       amount: Math.floor(amountInCADCents),
      //       currency: 'cad',
      //       destination: payload.recepientStripeAccount,
      //       metadata: {
      //         recepientId: payload.recepientId,
      //         organizationId: payload.organizationId,
      //         senderId: payload.userId,
      //         senderEmail: payload.userEmail,
      //         senderWalletId: walletId,
      //         imageUrl: payload.imageUrl,
      //         currency: payload.currency,
      //         recepientStripeAcct: payload.recepientStripeAccount,
      //         amount: payload.amount,
      //         type: 'debit',
      //         itemType: payload.itemType,
      //         amountInCAD,
      //         transactionId,
      //         gatewayCommission: CommonUtils.roundToTwoDecimalPlaces(
      //           this._getStripeFees(amountInCAD),
      //         ),
      //         platformCommission: CommonUtils.roundToTwoDecimalPlaces(
      //           this._getPaytalkFees(amountInCAD),
      //         ),
      //         totalAmount: CommonUtils.roundToTwoDecimalPlaces(
      //           amountInCAD + this._getCalculatedFees(amountInCAD),
      //         ),
      //       },
      //     });
      //     logger.info(transfer);

      //   } catch (error) {
      //     logger.error(error);
      //     throw new HttpException(error.type, error.code);
      //   }
      // } else {
      //   throw new HttpException('Insufficient Balance', 400);
      // }

      return response.status(HttpStatus.OK).json({ transactionId });
    } else {
      throw new HttpException('Insufficient Balance', 400);
    }
  }

  async chargeWallet(payload: ChargeWalletDto, response: Response) {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(payload.userId)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('user Id is not valid');
    }

    let walletSnapshot = await snapshot.ref.collection('wallet').get();

    let walletDoc = walletSnapshot.docs[0];

    let wallet = walletDoc.data() as IWallet;

    if (payload.currency === wallet.currency) {
      if (wallet.balance >= payload.amount) {
        wallet.balance = wallet.balance - payload.amount;

        await walletDoc.ref.update(wallet);

        walletSnapshot = await snapshot.ref.collection('wallet').get();

        walletDoc = walletSnapshot.docs[0];

        wallet = walletDoc.data() as IWallet;

        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto(wallet, HttpStatus.OK));
      } else {
        throw new BadRequestException('Insufficient Funds');
      }
    } else {
      throw new BadRequestException('Currency Mismatch');
    }
  }

  async chargeUserWallet(payload: ChargeWalletDto) {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(payload.userId)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('user Id is not valid');
    }

    let walletSnapshot = await snapshot.ref.collection('wallet').get();

    let walletDoc = walletSnapshot.docs[0];

    let wallet = walletDoc.data() as IWallet;

    if (payload.currency === wallet.currency) {
      if (wallet.balance >= payload.amount) {
        wallet.balance = wallet.balance - payload.amount;

        await walletDoc.ref.update(wallet);

        walletSnapshot = await snapshot.ref.collection('wallet').get();

        walletDoc = walletSnapshot.docs[0];

        wallet = walletDoc.data() as IWallet;
      } else {
        throw new BadRequestException('Insufficient Funds');
      }
    } else {
      throw new BadRequestException('Currency Mismatch');
    }
  }

  private _convertCurrency(
    from: string,
    to: string,
    amount: number,
  ): Promise<AxiosResponse<APLCurrConversionResponse>> {
    const baseUrl = `https://api.apilayer.com/exchangerates_data/convert`;
    const config = {
      headers: {
        apikey: '3aYCXBCcUwa0Oyb3Y8HcfWRmRU832JYy',
      },
    };

    return axios.get(
      `${baseUrl}?from=${from}&to=${to}&amount=${amount}`,
      config,
    );
  }

  private _getCalculatedFees(paymentAmount) {
    const stripeFees = (paymentAmount * 2.9) / 100 + 0.3;
    const paytalkFees = (paymentAmount * 1) / 100;
    const totalFees = stripeFees + paytalkFees;

    return totalFees;
  }

  private _getStripeFees(paymentAmount) {
    const stripeFees = (paymentAmount * 2.9) / 100 + 0.3;
    return stripeFees;
  }

  private _getPaytalkFees(paymentAmount) {
    const paytalkFees = (paymentAmount * 1) / 100;
    return paytalkFees;
  }

  // private async _updateWalletHistory(
  //   userId: string,
  //   amount: number,
  //   type: string,
  //   prevBal: number,
  //   currBal: number,
  //   currency: string,
  // ) {
  //   const walletHistory: WalletHistory = {
  //     userId,
  //     amount,
  //     type,
  //     prevBal,
  //     currBal,
  //     currency,
  //     dateTime: adminTypes.firestore.Timestamp.now(),
  //   };

  //   try {
  //     const walletRef = await firebaseAdmin
  //       .firestore()
  //       .collection('walletHistory')
  //       .add(walletHistory);

  //     return walletRef;
  //   } catch (error) {
  //     throw new HttpException(error.type, error.code);
  //   }
  // }

  // private async _createTransactionRecord(transaction: Transaction) {
  //   const docRef = await firebaseAdmin
  //     .firestore()
  //     .collection('transactions2')
  //     .add(transaction);

  //   const transactionId = docRef.id;

  //   await docRef.update({ transactionId });

  //   return transactionId;
  // }

  // private _convertCurrency(from: string, to: string, amount: number) {
  //   const baseUrl = 'https://data.fixer.io/api/convert';

  //   return axios.get(
  //     `${baseUrl}?access_key=9433ed16fac53b120c01b6950f1248e5&from=${from}&to=${to}&amount=${amount}`,
  //   );
  // }

  // private async _updateAuditTrail(
  //   amount: number,
  //   oldBalance: number,
  //   newBalance: number,
  //   currency: string,
  //   user: string,
  //   transactionId: string,
  //   source: string,
  //   type: string,
  // ) {
  //   const auditTrail: AuditTrail = {
  //     oldBalance,
  //     newBalance,
  //     user,
  //     transactionId,
  //     source,
  //     type,
  //     amount,
  //     currency,
  //     dateTime: adminTypes.firestore.Timestamp.now(),
  //   };

  //   const auditId = Date.now();

  //   try {
  //     const auditRef = await firebaseAdmin
  //       .firestore()
  //       .collection('audit')
  //       .doc(`${auditId}`)
  //       .set(auditTrail);

  //     return auditRef;
  //   } catch (error) {
  //     throw new HttpException(error.type, error.code);
  //   }
  // }
}
