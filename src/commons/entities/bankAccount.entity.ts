import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Profile } from './profiles.entity';

@Entity('BankAccounts')
export class BankAccount extends BaseEntity {

    @PrimaryColumn()
    BankAccountID: string;

    @Column()
    BankName: string;

    @Column()
    AccountNumber: string;

    @Column()
    AccountName: string;

    @ManyToOne(()=> Profile)
    @JoinColumn({ name: "ProfileID" })
    Profile:Profile

    @Column()
    DateAdded: Date;

    @Column()
    BankCode: string;

}