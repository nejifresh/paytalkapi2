import { BaseEntity, Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { Profile } from './profiles.entity';

@Entity('Organizations')
export class Organization extends BaseEntity {
  @PrimaryColumn()
  OrganizationID: string;

  @Column()
  Name: string;

  @Column()
  About: string;

  @Column()
  Website: string;

  @Column()
  PaymentGateWayID: string;

  @Column()
  OrganizationTypeID: string;

  @Column()
  SplitRatio: number;

  @Column()
  Status: boolean;

  @Column()
  DateAdded: Date;

  @Column()
  IsDeleted: boolean;

  @OneToMany(() => Profile, profile => profile.Organization,{eager: true})
  profiles: Profile[]
}