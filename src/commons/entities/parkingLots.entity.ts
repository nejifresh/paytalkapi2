import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('ParkingLots')
export class ParkingLots extends BaseEntity {

    @PrimaryColumn()
    ParkingLotID: string;

    @Column()
    ZoneNumber: string;

    @Column()
    MonFriDayRate: number;

    @Column()
    SatSunDayRate: number;

    @Column()
    StateProvince: string;

    @Column()
    MinWeekendPurchase: number;

    @Column()
    MaxWeekendPurchase: number;

    @Column()
    MinTimeMins: number;

    @Column()
    MinEveningPurchase: number;

    @Column()
    MaxEveningPurchase: number;

    @Column({ type: 'float' })
    Longitude: number;

    @Column({ type: 'float' })
    Latitude: number;

    @Column()
    EveningRate: number;

    @Column()
    DateCreated: Date;

    @Column()
    Currency: string;

    @Column()
    Country: string;

    @Column()
    Clearance: number;

    @Column()
    City: string;

    @Column()
    Address: string;

    @Column()
    MinWeekdayPurchase: number;

    @Column()
    MaxWeekdayPurchase: number;


    constructor(ParkingLotID: string, ZoneNumber: string, MonFriDayRate: number, SatSunDayRate: number, StateProvince: string, MinWeekendPurchase: number, MaxWeekendPurchase: number, MinTimeMins: number, MinEveningPurchase: number, MaxEveningPurchase: number, Longitude: number, Latitude: number, EveningRate: number, Currency: string, Country: string, Clearance: number, City: string, Address: string, MinWeekdayPurchase: number, MaxWeekdayPurchase: number) {
        super();
        this.ParkingLotID = ParkingLotID;
        this.ZoneNumber = ZoneNumber;
        this.MonFriDayRate = MonFriDayRate;
        this.SatSunDayRate = SatSunDayRate;
        this.StateProvince = StateProvince;
        this.MinWeekendPurchase = MinWeekendPurchase;
        this.MaxWeekendPurchase = MaxWeekendPurchase;
        this.MinTimeMins = MinTimeMins;
        this.MinEveningPurchase = MinEveningPurchase;
        this.MaxEveningPurchase = MaxEveningPurchase;
        this.Longitude = Longitude;
        this.Latitude = Latitude;
        this.EveningRate = EveningRate;
        this.Currency = Currency;
        this.Country = Country;
        this.Clearance = Clearance;
        this.City = City;
        this.Address = Address;
        this.MinWeekdayPurchase = MinWeekdayPurchase;
        this.MaxWeekdayPurchase = MaxWeekdayPurchase;
    }
}
