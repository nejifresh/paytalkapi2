import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('Events')
export class Events extends BaseEntity {

  @PrimaryColumn()
  id: string;

  @Column()
  eventCategoryId: string;

  @Column()
  address: string;

  @Column()
  currency: string;

  @Column()
  dateCreated: Date;

  @Column()
  description: string;

  @Column()
  endDateTime: Date;

  @Column()
  eventLink: string;

  @Column({ type: 'float' })
  longitude: number;

  @Column({ type: 'float' })
  latitude: number;

  @Column()
  maxTickets: number;

  @Column()
  minTickets: number;

  @Column()
  organizationId: string;

  @Column()
  profileId: string;

  @Column()
  promoImage: string;

  @Column()
  promoVideo: string;

  @Column()
  startDateTime: Date;

  @Column()
  ticketPrice: number;

  @Column()
  title: string;


  constructor(id: string, eventCategoryId: string, address: string, currency: string, dateCreated: Date, description: string, endDateTime: Date, eventLink: string, longitude: number, latitude: number, maxTickets: number, minTickets: number, organizationId: string, profileId: string, promoImage: string, promoVideo: string, startDateTime: Date, ticketPrice: number, title: string) {
    super();
    this.id = id;
    this.eventCategoryId = eventCategoryId;
    this.address = address;
    this.currency = currency;
    this.dateCreated = dateCreated;
    this.description = description;
    this.endDateTime = endDateTime;
    this.eventLink = eventLink;
    this.longitude = longitude;
    this.latitude = latitude;
    this.maxTickets = maxTickets;
    this.minTickets = minTickets;
    this.organizationId = organizationId;
    this.profileId = profileId;
    this.promoImage = promoImage;
    this.promoVideo = promoVideo;
    this.startDateTime = startDateTime;
    this.ticketPrice = ticketPrice;
    this.title = title;
  }
}
