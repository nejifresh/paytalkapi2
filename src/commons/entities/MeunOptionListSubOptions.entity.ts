import { BaseEntity, Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { MenuOptionLists } from './menuOptionLists.entity';

@Entity()
export class MenuOptionListSubOptions extends BaseEntity {
  @PrimaryColumn()
  MenuOptionListSubOptionID: string;

  @ManyToOne(() => MenuOptionLists,
      MenuOptionLists => MenuOptionLists.MenuOptionListSubOptions)
  MenuOptionList: MenuOptionLists;

  @Column()
  Name: string;

  @Column()
  Price: string;

  @Column()
  Amount: string;
}