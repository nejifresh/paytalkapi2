import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('EventImages')
export class EventImages extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  eventId: string;

  @Column()
  imageURL: string;


  constructor(eventId: string, imageURL: string) {
    super();
    this.eventId = eventId;
    this.imageURL = imageURL;
  }
}
