import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryColumn } from 'typeorm';
import { Organization } from './organization.entity';
import { BankAccount } from './bankAccount.entity';
import { Campaign } from './campaign.entity';

@Entity('Profiles')
export class Profile extends BaseEntity {
  @PrimaryColumn()
  ProfileID: string;

  @ManyToOne(() => Organization)
  @JoinColumn({ name: "OrganizationID" })
  Organization: Organization;

  @Column()
  Name: string;

  @Column()
  EmailAddress: string;

  @Column({
    nullable: true,
  })
  PaypalEmailAddress: string;

  @Column()
  PhoneNumber: string;

  @Column()
  Address: string;

  @Column()
  DateAdded: Date;

  @Column()
  IsDefault: boolean;

  /*@Column()
  Country: string;*/

  @Column({type:'decimal'})
  Longitude: number;

  @Column({type:'decimal'})
  Latitude: number;

  @OneToOne(() => BankAccount,{eager:true})
  @JoinColumn({ name: "ProfileID" })
  bankAccount: BankAccount;

  @OneToMany(() => Campaign, campaign => campaign.profile,{eager: true})
  campaigns: Campaign[]

  /*
  *
  stripeConnectedAccount: string;
  * */

}