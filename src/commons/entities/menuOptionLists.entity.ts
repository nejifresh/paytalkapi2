import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { MenuOptions } from './menuOptions.entity';
import { MenuOptionListSubOptions } from './MeunOptionListSubOptions.entity';

@Entity()
export class MenuOptionLists extends BaseEntity {

  @PrimaryColumn()
  MenuOptionListID: string;

  @ManyToOne(() => MenuOptions)
  MenuOption: MenuOptions;

  @Column()
  Title: string;

  @Column()
  MaxSelectable: number;

  @Column()
  IsRequired: boolean;

  @OneToMany(()=> MenuOptionListSubOptions,
      MenuOptionListSubOptions => MenuOptionListSubOptions.MenuOptionList)
  MenuOptionListSubOptions:MenuOptionListSubOptions



}