import { BaseEntity, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class MenuOptions extends BaseEntity {
  @PrimaryColumn()
  MenuItemID: string;

}