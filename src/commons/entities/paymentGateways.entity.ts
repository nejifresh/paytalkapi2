import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm';
import { uuid } from '@supercharge/strings';

@Entity('PaymentGateways')
export class PaymentGatewaysEntity extends BaseEntity {

  @PrimaryColumn('uuid')
  PaymentGatewayID: string;

  @Column()
  Name: string;

  @Column()
  DateAdded: Date;


  constructor(Name: string) {
    super();
    this.PaymentGatewayID = uuid().toUpperCase();
    this.DateAdded = new Date();
    this.Name = Name;
  }
}
