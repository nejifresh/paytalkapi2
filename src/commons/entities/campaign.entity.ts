import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Profile } from './profiles.entity';


@Entity('Campaigns')
export class Campaign {

  @PrimaryColumn()
  CampaignID: string

  @Column()
  Title:string

  @Column()
  Target:string

  @Column()
  Description:string

  @Column()
  DateAdded:Date

  @Column()
  ImageURL:string

  @ManyToOne(()=> Profile)
  @JoinColumn({ name: "ProfileID" })
  profile:Profile

}