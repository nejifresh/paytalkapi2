import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm';
import { uuid } from '@supercharge/strings';

@Entity('OrganizationTypes')
export class OrganisationTypes extends BaseEntity {

  @PrimaryColumn()
  OrganizationTypeID: string;

  @Column()
  Name: string;

  @Column()
  DateAdded: Date;

  constructor(Name: string) {
    super();
    this.OrganizationTypeID = uuid().toUpperCase();
    this.DateAdded = new Date();
    this.Name = Name;
  }
}
