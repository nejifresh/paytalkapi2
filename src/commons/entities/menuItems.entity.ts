import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { MenuItemCategories } from './menuItemCategories.entity';

@Entity('MenuItems')
export class MenuItems extends BaseEntity {
  @PrimaryColumn()
  MenuItemID: string;

  @ManyToOne(() => MenuItemCategories,{eager: true})
  @JoinColumn({ name: "MenuItemCategoryID" })
  MenuItemCategory: MenuItemCategories;

  @Column()
  Name: string;

  @Column()
  Description: string;

  @Column()
  ImageURL: string;

  @Column()
  Price: number;

  @Column()
  DateAdded: Date;

}