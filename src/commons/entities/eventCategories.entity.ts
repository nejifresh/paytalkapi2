import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('EventCategories')
export class EventCategories extends BaseEntity {

  @PrimaryColumn()
  id: string;

  @Column()
  name: string;


  constructor(id: string, name: string) {
    super();
    this.id = id;
    this.name = name;
  }
}
