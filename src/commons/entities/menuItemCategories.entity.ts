import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Organization } from './organization.entity';
import { Profile } from './profiles.entity';

@Entity('MenuItemCategories')
export class MenuItemCategories extends BaseEntity {
  @PrimaryColumn()
  MenuItemCategoryID: string;

  @Column()
  Name: string;

  @ManyToOne(type => Organization,{eager:true})
  @JoinColumn({ name: "OrganizationID" })
  Organization: Organization;

  @ManyToOne(type => Profile,{eager:true})
  @JoinColumn({ name: "ParentID" })
  Parent: string;

  @ManyToOne(type => Profile,{eager:true})
  @JoinColumn({ name: "ProfileID" })
  Profile: Profile;

}