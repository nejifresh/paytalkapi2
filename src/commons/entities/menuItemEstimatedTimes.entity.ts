import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm';
import { MenuItems } from './menuItems.entity';

@Entity()
export class MenuItemEstimatedTimes extends BaseEntity {

  @JoinColumn({ name: "MenuItemID" })
  @OneToOne(() => MenuItems,{primary: true})
  MenuItem: MenuItems;

  @Column()
  Max: number;

  @Column()
  Min: number;
}