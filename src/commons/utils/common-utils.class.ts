export class CommonUtils {
  static numToString(val: number): string {
    return String(val);
  }

  static stringToNum(val: string): number | undefined {
    if (isNaN(Number(val))) {
      return undefined;
    } else {
      return Number(val);
    }
  }

  static percentToVal(percent: number, amount: number) {
    return (percent / 100) * amount;
  }

  static roundToTwoDecimalPlaces(num) {
    return Math.round((num + Number.EPSILON) * 100) / 100;
  }
}
