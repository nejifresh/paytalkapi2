import { SpeechClient, protos } from '@google-cloud/speech';
import fs from 'fs';

const client = new SpeechClient();

export const gSpeech2Text = async (languageCode, filePath) => {
  // The audio file's encoding, sample rate in hertz, and BCP-47 language code
  const audio = {
    content: fs.readFileSync(filePath).toString('base64'),
  };
  const config = {
    encoding:
      protos.google.cloud.speech.v1.RecognitionConfig.AudioEncoding.LINEAR16, //'LINEAR16',
    sampleRateHertz: 16000,
    languageCode,
  };
  const request = {
    audio: audio,
    config: config,
  };

  // Detects speech in the audio file
  const [response] = await client.recognize(request);
  const transcription = response.results
    .map(result => result.alternatives[0].transcript)
    .join('\n');
  console.log(`Transcription: ${transcription}`);

  return transcription;
};
