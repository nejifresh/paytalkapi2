import * as dotEnv from 'dotenv';
import Paystack from 'paystack';

dotEnv.config();


export const paystack = Paystack(process.env.payStackTestKey);
