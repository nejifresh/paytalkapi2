export interface APLCurrConversionResponse {
  success: boolean;
  query: Query;
  info: Info;
  date: string;
  result: number;
}

interface Info {
  timestamp: number;
  rate: number;
}

interface Query {
  from: string;
  to: string;
  amount: number;
}
