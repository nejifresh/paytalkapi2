import firebase from 'firebase';

export interface OrganizationUser {
  accountSetupCompleted: boolean;
  dateCreated: firebase.firestore.Timestamp;
  firstName: string;
  lastName: string;
  imageUrl: string;
  organizations: UserOrganization[];
  email: string;
  passwordChanged: boolean;
}

export interface UserOrganization {
  organizationId: string;
  profileId: string;
}
