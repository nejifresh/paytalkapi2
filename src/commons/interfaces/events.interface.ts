import firebase from 'firebase';
import GeoPoint = firebase.firestore.GeoPoint;
import Timestamp = firebase.firestore.Timestamp;

export interface IEvent {

  categoryId: string;

  address: string;

  currency: string;

  dateCreated: Timestamp;

  description: string;

  endDateTime: Timestamp;

  eventLink: string;

  location: GeoPoint;

  maxTickets: number;

  minTickets: number;

  organizationId: string;

  profileId: string;

  promoImage: string;

  promoVideo: string;

  startDateTime: Timestamp;

  ticketPrice: number;

  title: string;

  images: Array<string>;

  ticketPriceType: string;
}
