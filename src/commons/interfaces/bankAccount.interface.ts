export interface IBankAccount {
    accountName: string
    accountNumber:string
    bankCode: string
    bankName: string
}