import firebase from 'firebase';

export interface Organization {
  name: string;
  website: string;
  userId: string;
  profileCount: number;
  category: string;
  dateRegistered: firebase.firestore.Timestamp;
  defaultProfileId: string;
  addedByUserId: string;
  about: string;
  paymentGateway: string;
  profiles: Profile;
  campaigns: Campaign;
  defaultAdmin: string;
  defaultProfile: string;
  totalCampaigns: number;
  type: string;
  active: string;
  splitRatio: number;
  imageUrl: string;
}

export interface Campaign {
  title: string;
  target: number;
  profileId: string;
  description: string;
  images: string[];
  dateCreated: firebase.firestore.Timestamp;
  creatorId: string;
}

export interface Profile {
  name: string;
  account: ProfileAccount;
  address: string;
  country: string;
  dateCreated: firebase.firestore.Timestamp;
  defaultProfile: boolean;
  email: string;
  phoneNumber: string;
}

export interface ProfileAccount {
  accountName: string;
  accountNumber: number;
  bankName: string;
  paystackSubAccount: string;
}
