import { firestore } from 'firebase-admin';
import { IBankAccount } from './bankAccount.interface';

export interface IProfile {

	name: string;
	email: string;
	paypalEmail: string;
	phoneNumber: string;
	address: string;
	dateCreated: firestore.Timestamp;
	defaultProfile: boolean;
	country: string;
	account: IBankAccount;
	location: firestore.GeoPoint;
	profileId: string;
	organizationId: string;
	longitude: number;
	latitude: number;
	stripeCustomerId: string;
	taxPercent: number;
	serviceCharge: number;
}
