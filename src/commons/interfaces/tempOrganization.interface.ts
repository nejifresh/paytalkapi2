import firebase from 'firebase';

export interface ITempOrganization {
  name: string;
  website: string;
  category: string;
  currency: string;
  dateRegistered: firebase.firestore.Timestamp;
  about: string;
  paymentGateway: string;
  defaultAdmin: string;
  type: string;
  splitRatio: number;
  imageUrl: string;
  status: string;
  rejectionMessage: string;
  organizationId: string;
}

