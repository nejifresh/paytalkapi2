import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export interface PaytalkUsers {

  country: string;
  currency: string;
  dateJoined: Timestamp
  dateVoiceEnrolled: Timestamp
  email: string;
  enableVoiceRecognition: boolean
  enrolled: boolean
  firstName: string
  lastName: string
  imageUrl: string
  symbol: string
  username: string
  uuid: string
  voiceEnrolled: boolean
  voice_id: string


}
