import firebase from 'firebase';

export interface PaytalkTransaction {
  amount: number;
  currency: string;
  dateOfTransaction: firebase.firestore.Timestamp;
  paymentGateway: string;
  imageUrl: string;
  campaign: Campaign;
  organizationId: string;
  beneficiary: Beneficiary;
  sender: Sender;
  transactionId: string;
  type: string;
  status: string;
}

export interface Sender {
  uid: string;
  email: string;
}

export interface Beneficiary {
  name: string;
  profileId: string;
}

export interface Campaign {
  title: string;
  uid: string;
}
