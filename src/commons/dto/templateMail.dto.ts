export class TemplateMailDTO {
  to: string;
  subject: string;
  template: string;
  views: string[];
  data: Record<string, unknown>;
  options?: Record<string, unknown>

  constructor(mailObject: Record<string, unknown>) {
    Object.assign(this, mailObject);
  }
}
