import { ApiProperty } from '@nestjs/swagger';

export class SuccessResponseDto<T> {
  @ApiProperty()
  private data: T;
  @ApiProperty()
  private statusCode: number;


  constructor(data: T, statusCode: number) {
    this.data = data;
    this.statusCode = statusCode;
  }
}
