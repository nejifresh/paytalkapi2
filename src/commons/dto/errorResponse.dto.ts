import { ApiProperty } from '@nestjs/swagger';

export class ErrorResponseDto {

  @ApiProperty()
  private data: any;
  @ApiProperty()
  private statusCode: number;


  constructor(data: any, statusCode: number) {
    this.data = data;
    this.statusCode = statusCode;
  }
}
