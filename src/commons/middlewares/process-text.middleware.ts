import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import stringJs from 'string';
import wordsToNumbers from 'words-to-numbers';
import { firebaseAdmin } from '../firebaseConfig';

@Injectable()
export class ProcessTextMiddleware implements NestMiddleware {
  async use(req: Request, res: Response, next: () => void) {
    const { privText } = req['text'];
    const text = stringJs(privText.toLowerCase()).stripPunctuation().s;
    const textInFigures = wordsToNumbers(text) as any;

    const textPart = textInFigures.split(' ');
    let sendAmount;
    const data = {};

    for (const tp of textPart) {
      if (!isNaN(tp)) {
        sendAmount = parseInt(tp);
      }
    }

    const searchResult = text.search(/send/i);

    const searchResult2 = text.search(/donate/i);

    if (searchResult !== -1 || searchResult2 !== -1) {
      data['amount'] = sendAmount;

      const searchResult = text.search(/TO/i);

      const searchResultToo = text.search(/TO/i);
      let merchantName;

      if (searchResult !== -1) {
        merchantName = text.substring(searchResult + 2).trim();
      }

      if (searchResultToo !== -1) {
        merchantName = text.substring(searchResult + 2).trim();
      }

      if (merchantName.search(/the/i) !== -1) {
        merchantName = stringJs(merchantName).chompLeft('the ').s;
        merchantName = stringJs(merchantName).collapseWhitespace().s;
      }

      const merchantNameArray = merchantName.split(' ');
      const firstChar = merchantName.charAt(0);
      const lastChar = merchantName.charAt(merchantName.length - 1);

      //console.log(merchantNameArray);

      if (searchResult !== -1) {
        const snapshot = firebaseAdmin
          .firestore()
          .collection('users')
          .get();

        const merchantsSnap = (await snapshot).docs.map(doc => doc.data());
        const merchants = [];

        for (const merchant of merchantsSnap) {
          for (const name of merchantNameArray) {
            if (merchant.username.includes(`${name}`)) {
              merchants.push(merchant);
            }
          }

          if (
            merchant.username.startsWith(firstChar) ||
            merchant.username.endsWith(lastChar)
          ) {
            merchants.push(merchant);
          }

          data['merchant'] = Array.from(new Set(merchants));
          data['command'] = text;
          data['score'] = req['score'];
        }
      }

      if (searchResult2 !== -1) {
        const snapshot = firebaseAdmin
          .firestore()
          .collection('charities')
          .get();

        const merchantsSnap = (await snapshot).docs.map(doc => doc.data());
        const charities = [];

        for (const merchant of merchantsSnap) {
          for (const name of merchantNameArray) {
            if (merchant.name.includes(`${name}`)) {
              charities.push(merchant);
            }
          }

          if (
            merchant.name.startsWith(firstChar) ||
            merchant.name.endsWith(lastChar)
          ) {
            charities.push(merchant);
          }

          data['charity'] = Array.from(new Set(charities));
          data['command'] = text;
          data['score'] = req['score'];
        }
      }
      req['data'] = data;
      return next();
    } else {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ error: 'Invalid command' });
    }
  }
}
