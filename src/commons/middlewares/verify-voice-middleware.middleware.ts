import {
  Injectable,
  NestMiddleware,
  HttpService,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { firebaseAdmin } from '../firebaseConfig';

@Injectable()
export class VerifyVoiceMiddleware implements NestMiddleware {
  constructor(private httpService: HttpService) {}

  async use(req: Request, res: Response, next: () => void) {
    const userId = req['uuid'];
    const docRef = firebaseAdmin
      .firestore()
      .collection('users')
      .doc(userId);

    const doc = await docRef.get();
    const { voice_id } = doc.data();

    this.httpService
      .post(`/text-independent/profiles/${voice_id}/verify`, req.body)
      .toPromise()
      .then(result => {
        const { data } = result;

        if (data.score > 0.4) {
          req['score'] = data.score;
          next();
        } else {
          return res
            .status(HttpStatus.BAD_REQUEST)
            .json({ error: 'Voice does not match', score: data.score });
        }
      })
      .catch(error => {
        const { data } = error.response;
        console.log('Error From Voice Verification', data);
        return res
          .status(HttpStatus.BAD_REQUEST)
          .json({ error: 'Voice not verified' });
      });
  }
}
