import { ProcessTextMiddleware } from './process-text.middleware';

describe('ProcessTextMiddleware', () => {
  it('should be defined', () => {
    expect(new ProcessTextMiddleware()).toBeDefined();
  });
});
