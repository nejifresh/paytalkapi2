import { Injectable, NestMiddleware } from '@nestjs/common';
import Busboy from 'busboy';
import os from 'os';
import fs from 'fs';
import path from 'path';

@Injectable()
export class FileUploadMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    const busboy = new Busboy({
      headers: req.headers,
      limits: {
        // Cloud functions impose this restriction anyway
        fileSize: 100 * 1024 * 1024,
      },
    });

    const fields = {};
    const files = [];
    const fileWrites = [];
    // Note: os.tmpdir() points to an in-memory file system on GCF
    // Thus, any files in it must fit in the instance's memory.
    const tmpdir = os.tmpdir();

    busboy.on('field', (key, value) => {
      // You could do additional deserialization logic here, values will just be
      // strings
      fields[key] = value;
    });

    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      const filepath = path.join(tmpdir, filename);
      // console.log(
      //   `Handling file upload field ${fieldname}: ${filename} (${filepath})`,
      // );
      const writeStream = fs.createWriteStream(filepath);
      file.pipe(writeStream);

      fileWrites.push(
        new Promise((resolve, reject) => {
          file.on('end', () => writeStream.end());
          writeStream.on('finish', () => {
            fs.readFile(filepath, (err, buffer) => {
              const size = Buffer.byteLength(buffer);
              if (err) {
                return reject(err);
              }

              files.push({
                fieldname,
                originalname: filename,
                encoding,
                mimetype,
                buffer,
                size,
                filepath,
              });

              /*try {
               fs.unlinkSync(filepath);
             } catch (error) {
               return reject(error);
             }*/

              resolve(files);
            });
          });
          writeStream.on('error', reject);
        }),
      );
    });

    busboy.on('finish', () => {
      Promise.all(fileWrites)
        .then(() => {
          req.body = fields;
          req.files = files;
          return next();
        })
        .catch(next);
    });

    busboy.end(req.rawBody);
  }
}
