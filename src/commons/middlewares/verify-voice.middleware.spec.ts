import { VerifyVoiceMiddleware } from './verify-voice-middleware.middleware';

describe('VerifyVoiceMiddlewareMiddleware', () => {
  it('should be defined', () => {
    expect(new VerifyVoiceMiddleware()).toBeDefined();
  });
});
