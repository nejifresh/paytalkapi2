import { ExtractTextMiddleware } from './extract-text.middleware';

describe('ExtractTextMiddleware', () => {
  it('should be defined', () => {
    expect(new ExtractTextMiddleware()).toBeDefined();
  });
});
