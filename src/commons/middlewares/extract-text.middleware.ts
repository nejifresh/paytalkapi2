import { Injectable, NestMiddleware, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import * as sdk from 'microsoft-cognitiveservices-speech-sdk';

@Injectable()
export class ExtractTextMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    const audio = req.body;
    const subscriptionKey = process.env.azureSubscriptionKey;
    const serviceRegion = process.env.azureServiceRegion;
    const pushStream = sdk.AudioInputStream.createPushStream();

    pushStream.write(audio);

    const speechConfig = sdk.SpeechConfig.fromSubscription(
      subscriptionKey,
      serviceRegion,
    );

    speechConfig.speechRecognitionLanguage = 'en-NG';
    // speechConfig.setProperty(
    //TODO Fix this
    // TODO replace setting with REST api approach
    // sdk.PropertyId.SpeechServiceConnection_InitialSilenceTimeoutMs,
    // '50000',
    // );

    const audioConfig = sdk.AudioConfig.fromStreamInput(pushStream);

    let recognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);

    recognizer.recognizeOnceAsync(
      result => {
        req['text'] = result;

        recognizer.close();
        recognizer = undefined;

        return next();
      },
      error => {
        res.status(HttpStatus.BAD_REQUEST).json(error);

        recognizer.close();
        recognizer = undefined;
      },
    );
  }
}
