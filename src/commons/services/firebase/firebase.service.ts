import { Injectable } from '@nestjs/common';
import { firebaseAdmin } from '../../firebaseConfig';

@Injectable()
export class FirebaseService {
  async getUser(
    userId: string,
  ): Promise<firebase.default.firestore.DocumentData> {
    const docRef = firebaseAdmin
      .firestore()
      .collection('users')
      .doc(userId);

    const doc = await docRef.get();
    const user = doc.data();

    return user;
  }
}
