import { Test, TestingModule } from '@nestjs/testing';
import { GoogleTtsService } from './google-tts.service';

describe('GoogleTtsService', () => {
  let service: GoogleTtsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GoogleTtsService],
    }).compile();

    service = module.get<GoogleTtsService>(GoogleTtsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
