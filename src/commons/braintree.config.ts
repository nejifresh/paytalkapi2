import { BraintreeGateway, Environment, KeyGatewayConfig } from 'braintree';

const gatewayConfig: KeyGatewayConfig = {
  environment: Environment.Sandbox,
  merchantId: process.env.brainTreeMerchantId,
  publicKey: process.env.brainTreePublicKey,
  privateKey: process.env.brainTreePrivateKey,
};

export const braintreeGateway = new BraintreeGateway(gatewayConfig);
