export class Log {
  severity: 'INFO' | 'ERROR' | 'WARNING';
  payload: unknown;
  message?: string;

  constructor(
    severity: 'INFO' | 'ERROR' | 'WARNING',
    payload: any,
    message?: string,
  ) {
    this.severity = severity;
    this.payload = payload;
    if (message && message.length > 0) {
      this.message = message;
    }
  }
}
