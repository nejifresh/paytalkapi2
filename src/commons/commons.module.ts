import { Module, HttpModule } from '@nestjs/common';
import { AzureService } from './services/azure/azure.service';
import { FirebaseService } from './services/firebase/firebase.service';
import { GoogleTtsService } from './services/google-tts/google-tts.service';

@Module({
  imports: [
    HttpModule.register({
      baseURL: process.env.azureBaseURL,
      headers: {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': process.env.azureSubscriptionKey,
      },
    }),
  ],
  exports: [FirebaseService, HttpModule],
  providers: [FirebaseService, AzureService, GoogleTtsService],
})
export class CommonsModule {}
