import * as dotEnv from 'dotenv';

import Ravepay from 'flutterwave-node';

dotEnv.config();
import  flutterwave = require('flutterwave-node-v3');

export const Flutterwave = new flutterwave(process.env.flutterWavePublicKey, process.env.flutterWaveSecretKey);

export const rave = new Ravepay(process.env.flutterWavePublicKey, process.env.flutterWaveSecretKey, true);

