import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { ErrorResponseDto } from '../dto/errorResponse.dto';
import { Request, Response } from 'express';
import { logger } from '../firebaseConfig';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter{
  catch(exception: Error, host: ArgumentsHost): any {
    const context = host.switchToHttp();
    const response = context.getResponse<Response>();
    const request = context.getRequest<Request>();
    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const message =
      exception instanceof HttpException
        ? exception.message
        : "Something went wrong";


    logger.error(`${request.method} ${request.url}`, exception.message, exception.stack ,'Exception Filter')

    if(!(exception instanceof HttpException)){
      logger.error(`${request.method} ${request.url}`, exception.message, exception.stack ,'Exception Filter')
    }

    return response.status(status).json(
      new ErrorResponseDto(
        message,
        status,
      )
    );
  }
}
