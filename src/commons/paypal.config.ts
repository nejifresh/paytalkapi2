import paypalCheckOut = require('@paypal/checkout-server-sdk');
import paypalPayOut = require('@paypal/payouts-sdk');
import paypalRest from 'paypal-rest-sdk';

import * as dotEnv from 'dotenv';

dotEnv.config();

export const environment = new paypalCheckOut.core.SandboxEnvironment(
  process.env.paypalSandboxClientId,
  process.env.paypalSanboxClientSecret,
);
export const paypalClient = new paypalCheckOut.core.PayPalHttpClient(
  environment,
);

export const paypalSDK: paypalCheckOut = paypalCheckOut;

paypalRest.configure({
  client_id: process.env.paypalSandboxClientId,
  client_secret: process.env.paypalSanboxClientSecret,
  mode: 'sandbox',
});

export const payPal = paypalRest;
export const paypalPayOutSdk = paypalPayOut;
