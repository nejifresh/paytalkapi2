import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class CreateCustomerDTO {

  @ApiProperty()
  @IsDefined()
  organizationId: string

  @ApiProperty()
  @IsDefined()
  profileId: string
}