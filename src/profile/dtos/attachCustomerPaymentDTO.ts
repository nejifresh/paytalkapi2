import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class AttachCustomerPaymentDTO {

  @ApiProperty()
  @IsDefined()
  organizationId: string

  @ApiProperty()
  @IsDefined()
  profileId: string

  @ApiProperty()
  @IsDefined()
  paymentMethodId: string
}