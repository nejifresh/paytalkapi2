import { HttpStatus, Injectable, Res } from '@nestjs/common';
import { Stripe } from 'stripe';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { CreateCustomerDTO } from './dtos/createCustomerDTO';
import { IProfile } from '../commons/interfaces/profile.interface';
import { Response } from 'express';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { AttachCustomerPaymentDTO } from './dtos/attachCustomerPaymentDTO';

@Injectable()
export class ProfileService {
  stripe: Stripe;

  constructor() {
    this.stripe = new Stripe(process.env.stripeSecretKey, {
      apiVersion: '2020-08-27',
      typescript: true,
    });
  }

  async createCustomer(createCustomerDTO: CreateCustomerDTO,@Res() response: Response):  Promise<Response> {

    return  firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(createCustomerDTO.organizationId)
      .collection('profiles')
      .doc(createCustomerDTO.profileId)
      .get()
      .then(profilesSnapshot =>{

        const profile = profilesSnapshot.data() as IProfile

        return  this.stripe.customers.create({
          email: profile.email,
        })
      })
      .then(customer =>{

        return  firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(createCustomerDTO.organizationId)
          .collection('profiles')
          .doc(createCustomerDTO.profileId)
          .update({stripeCustomerId : customer.id})
      }).then(()=>{
        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto('Customer Created', HttpStatus.OK));
      })
  }

  async listCustomerPaymentMethods(createCustomerDTO: CreateCustomerDTO,@Res() response: Response):  Promise<Response> {

    return  firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(createCustomerDTO.organizationId)
      .collection('profiles')
      .doc(createCustomerDTO.profileId)
      .get()
      .then(profilesSnapshot =>{

        const profile = profilesSnapshot.data() as IProfile

        return this.stripe.paymentMethods.list({
          customer: profile.stripeCustomerId,
          type: 'card',
        })
      })
      .then(paymentMethods =>{
        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto(paymentMethods.data, HttpStatus.OK));
      })
  }

  async attachCustomerPaymentMethods(attachPaymentMethod: AttachCustomerPaymentDTO,@Res() response: Response):  Promise<Response> {

    return  firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(attachPaymentMethod.organizationId)
      .collection('profiles')
      .doc(attachPaymentMethod.profileId)
      .get()
      .then(profilesSnapshot =>{

        const profile = profilesSnapshot.data() as IProfile

        return this.stripe.paymentMethods.attach(
          attachPaymentMethod.paymentMethodId,
          { customer: profile.stripeCustomerId })
      })
      .then(paymentMethods =>{
        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto(paymentMethods, HttpStatus.OK));
      })
  }

}
