import { Body, Controller, Post, Res, UseGuards, UsePipes } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { CreateReservationDto } from '../reservations/dto/create-reservation.dto';
import { Response } from 'express';
import { CreateCustomerDTO } from './dtos/createCustomerDTO';
import { AttachCustomerPaymentDTO } from './dtos/attachCustomerPaymentDTO';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';

@Controller('profile')
@ApiBearerAuth()
@ApiTags('profile')
@UseGuards(AuthGuard)
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Post('/create-customer')
  @UsePipes(DTOValidationPipe)
  create(
    @Body() createCustomerDTO: CreateCustomerDTO,
    @Res() response: Response,
  ) {
    return this.profileService.createCustomer(createCustomerDTO,response)
  }

  @Post('list-payment-methods')
  @UsePipes(DTOValidationPipe)
  listPaymentMethods(
    @Body() createCustomerDTO: CreateCustomerDTO,
    @Res() response: Response,
  ) {
    return this.profileService.listCustomerPaymentMethods(createCustomerDTO,response)
  }

  @Post('attach-payment-methods')
  @UsePipes(DTOValidationPipe)
  attachPaymentMethod(
    @Body() attachPaymentMethod: AttachCustomerPaymentDTO,
    @Res() response: Response,
  ) {
    return this.profileService.attachCustomerPaymentMethods(attachPaymentMethod,response)
  }

}
