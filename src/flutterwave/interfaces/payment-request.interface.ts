export interface FlutterwavePaymentRequest {
  tx_ref: string;
  amount: number;
  currency: string;
  redirect_url: string;
  payment_options: string;
  meta: Meta;
  customer: Customer;
  customizations: Customizations;
}

interface Meta {
  consumer_id: string;
  consumer_mac: string;
}

interface Customer {
  email: string;
  phonenumber: string;
  name: string;
}

interface Customizations {
  title: string;
  description: string;
  logo: string;
}
