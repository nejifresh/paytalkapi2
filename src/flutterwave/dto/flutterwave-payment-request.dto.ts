import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class FlutterwavePaymentRequestDTO {
  @ApiProperty()
  @IsDefined()
  tx_ref: string;

  @ApiProperty()
  @IsDefined()
  amount: number;

  organizationId: string;
  profileId: string;
  campaignId: string;

  @ApiProperty()
  @IsDefined()
  currency: string;

  @ApiProperty()
  @IsDefined()
  redirect_url: string;

  @ApiProperty()
  meta: Meta;


  @ApiProperty()
  @IsDefined({
    each: true,
  })
  customer: Customer;

  @ApiProperty()
  @IsDefined({
    each: true,
  })
  customizations: Customizations;
}

interface Meta {
  consumer_id: string;
  consumer_mac: string;
}

interface Customer {
  email: string;
  phonenumber: string;
  name: string;
}

interface Customizations {
  title: string;
  description: string;
  logo: string;
}
