import { Module, HttpModule } from '@nestjs/common';
import { FlutterwaveController } from './flutterwave.controller';
import { FlutterwaveService } from './flutterwave.service';

@Module({
  controllers: [FlutterwaveController],
  providers: [FlutterwaveService],
  imports: [
    HttpModule.register({
      baseURL: process.env.flutterWaveBaseURL,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${process.env.flutterWaveSecretKey}`,
      },
    }),
  ],
})
export class FlutterwaveModule {}
