import { Body, Controller, HttpStatus, Post, Res, UseGuards, UsePipes } from '@nestjs/common';
import { FlutterwaveService } from './flutterwave.service';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { Response } from 'express';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetUid } from '../auth/decorators/getAuthenticatedUser.decorator';
import { AuthGuard } from '../auth/auth-guard.service';


@ApiTags('flutterwave')
@ApiBearerAuth()
@Controller('flutterwave')
export class FlutterwaveController {
  constructor(private readonly flutterwaveService: FlutterwaveService) {
  }

  @Post('/pay')
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  payment(
    @Body() flutterwavePaymentRequestDto: any,
    @GetUid() loggedInUserId: string,
    @Res() response: Response,
  ): Promise<any> {
    return this.flutterwaveService.payment(flutterwavePaymentRequestDto, loggedInUserId, response);
  }

  @Post('webhook')
  @ApiOperation({ summary: 'Flutterwave Webhook' })
  @ApiOkResponse()
  makePayment(@Body() requestBody: any, @Res() res: Response) {
    this.flutterwaveService.processWebhook(requestBody);
    return res.status(HttpStatus.OK).json('Event Received');
  }

}
