import {
  HttpService,
  HttpStatus,
  Injectable,
  NotFoundException,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { firebaseAdmin, logger, adminTypes } from '../commons/firebaseConfig';
import {
  Beneficiary,
  Campaign,
  Transaction,
} from '../transactions/entities/transaction.entity';

@Injectable()
export class FlutterwaveService {
  constructor(private readonly httpService: HttpService) {}

  payment(
    requestObj: any,
    loggedInUserId: string,
    @Res() response: Response,
  ): Promise<any> {
    let beneficiary: Beneficiary;
    let campaign: Campaign;

    return firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(requestObj.organizationId)
      .collection('profiles')
      .doc(requestObj.profileId)
      .get()
      .then(profile => {
        if (!profile.exists) {
          throw new NotFoundException('Profile Id is not valid');
        }
        beneficiary = {
          name: profile.data().name,
          profileId: profile.id,
        };

        return firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(requestObj.organizationId)
          .collection('campaigns')
          .doc(requestObj.campaignId)
          .get();
      })
      .then(campaignsRef => {
        if (!campaignsRef.exists) {
          throw new NotFoundException('Campaigns Id is not valid');
        }
        const { title } = campaignsRef.data();

        campaign = { title, id: campaignsRef.id };
      })
      .then(() => {
        const transaction = new Transaction(
          requestObj.amount,
          requestObj.currency,
          requestObj.organizationId,
          loggedInUserId,
          requestObj.customer.email,
          'flutterwave',
          requestObj.tx_ref,
          beneficiary,
          campaign,
          requestObj.customizations.logo,
          requestObj.meta.payment_type,
        );

        firebaseAdmin
          .firestore()
          .collection('transactions2')
          .doc(requestObj.tx_ref)
          .create({ ...transaction });

        return this.httpService
          .post('/payments', requestObj)
          .toPromise()
          .then(responseObj => {
            logger.info(responseObj);

            if (
              responseObj.data.status === 'success' &&
              requestObj.type === 'subscription'
            ) {
              const respData = JSON.parse(responseObj.config.data);
              firebaseAdmin
                .firestore()
                .collection('subscriptions')
                .doc(respData.document_id)
                .update({
                  active: true,
                  transactionId: respData.tx_ref,
                });
            }
            return response.status(HttpStatus.CREATED).json(responseObj.data);
          });
      })
      .catch(error => logger.error(error));
  }

  async processWebhook(payload: any): Promise<void> {
    const { tx_ref, created_at, status } = payload.data;

    if (tx_ref && tx_ref.length > 0 && status && status === 'successful') {
      firebaseAdmin
        .firestore()
        .collection('transactions2')
        .doc(tx_ref)
        .update({ status: 'success' });
    }
  }
}
