import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsDefined, IsOptional } from 'class-validator';

export class GetServiceProvidersDTO {

  @ApiProperty()
  @IsDefined()
  name: string

  @ApiProperty()
  @IsDefined()
  longitude: number

  @ApiProperty()
  @IsDefined()
  latitude: number

  @ApiProperty()
  @IsDefined()
  radius: number

}
