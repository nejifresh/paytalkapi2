import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsDefined, IsOptional } from 'class-validator';

export class GetResturantsDTO {

  @ApiProperty({type : [String]})
  @IsDefined()
  @IsArray()
  keywords: Array<string>

  @ApiProperty()
  @IsDefined()
  longitude: number

  @ApiProperty()
  @IsDefined()
  latitude: number

  @ApiProperty()
  @IsDefined()
  radius: number

}
