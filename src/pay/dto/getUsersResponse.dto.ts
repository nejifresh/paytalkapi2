import { ApiProperty } from '@nestjs/swagger';
import { ProfileDTO } from '../../organizations/dto/profile.dto';
import { Profile } from '../../organizations/entities/profile.entity';
import { CampaignDTO } from '../../organizations/dto/campaign.dto';
import { Campaign } from '../../organizations/entities/campaign.entity';

export class GetUsersResponseDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  category: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  dateRegistered: Date;
  @ApiProperty()
  userId: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  paymentGateway: string;
  @ApiProperty()
  about: string;
  @ApiProperty()
  certificateOfIncorporationURL: string;
  @ApiProperty()
  logoURL: string;
  @ApiProperty()
  addedByUserId: string;
  @ApiProperty()
  splitRatio: string;

  @ApiProperty({ type: [ProfileDTO] })
  profiles: Array<Profile>;
  @ApiProperty({ type: [CampaignDTO] })
  campaigns: Array<Campaign>;

}
