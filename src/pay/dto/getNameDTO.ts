import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class GetNameDTO {

  @ApiProperty()
  @IsDefined()
  name: string;

  @ApiProperty()
  @IsDefined()
  longitude: number

  @ApiProperty()
  @IsDefined()
  latitude: number

}
