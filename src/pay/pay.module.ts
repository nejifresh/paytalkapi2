import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { PayController } from './pay.controller';
import { VerifyVoiceMiddleware } from '../commons/middlewares/verify-voice-middleware.middleware';
import { ExtractTextMiddleware } from '../commons/middlewares/extract-text.middleware';
import { CommonsModule } from '../commons/commons.module';
import { PayService } from './pay.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Organization } from '../commons/entities/organization.entity';
import { Profile } from '../commons/entities/profiles.entity';
import { MenuItemCategories } from '../commons/entities/menuItemCategories.entity';
import { MenuItems } from '../commons/entities/menuItems.entity';
import { MenuItemEstimatedTimes } from '../commons/entities/menuItemEstimatedTimes.entity';
import { MenuOptions } from '../commons/entities/menuOptions.entity';
import { MenuOptionLists } from '../commons/entities/menuOptionLists.entity';
import { MenuOptionListSubOptions } from '../commons/entities/MeunOptionListSubOptions.entity';


@Module({
  controllers: [PayController],
  imports: [CommonsModule, TypeOrmModule.forFeature([
    Organization,
    Profile,
    MenuItemCategories,
    MenuItems,
    MenuItemEstimatedTimes,
    MenuOptions,
    MenuOptionLists,
    MenuOptionListSubOptions

  ])],
  providers: [PayService],
})
export class PayModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        // FileUploadMiddleware,
        //VerifyVoiceMiddleware,
       // ExtractTextMiddleware,
        // ProcessTextMiddleware,
      )
      .exclude({ path: 'pay/get-users', method: RequestMethod.POST },{path: 'pay/users',method: RequestMethod.POST})
      .forRoutes('pay');
  }
}
