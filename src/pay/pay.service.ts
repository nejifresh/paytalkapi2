import { HttpStatus, Injectable, Res } from '@nestjs/common';
import { GetNameDTO } from './dto/getNameDTO';
import stringJs from 'string';
import { ILike, Repository } from 'typeorm';
import { Organization } from '../commons/entities/organization.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Profile } from '../commons/entities/profiles.entity';
import { MenuItemCategories } from '../commons/entities/menuItemCategories.entity';
import { MenuItems } from '../commons/entities/menuItems.entity';
import { MenuOptions } from '../commons/entities/menuOptions.entity';
import { MenuOptionLists } from '../commons/entities/menuOptionLists.entity';
import { MenuOptionListSubOptions } from '../commons/entities/MeunOptionListSubOptions.entity';
import { MenuItemEstimatedTimes } from '../commons/entities/menuItemEstimatedTimes.entity';
import { GetResturantsDTO } from './dto/getResturantsDTO';
import { Response } from 'express';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { GetOrganizationsResponseDto } from '../organizations/dto/SQLDto/GetOrganizationsResponse.dto';
import { ProfileSQLDTO } from '../organizations/dto/SQLDto/profile.sql.dto';
import { GetServiceProvidersDTO } from './dto/getServiceProvidersDTO';

@Injectable()
export class PayService {
  constructor(
    @InjectRepository(Organization)
    private organizationRepository: Repository<Organization>,
    @InjectRepository(Profile) private profileRepository: Repository<Profile>,
    @InjectRepository(MenuItemCategories)
    private menuItemCategoryRepository: Repository<MenuItemCategories>,
    @InjectRepository(MenuItems)
    private menuItemRepository: Repository<MenuItems>,
    @InjectRepository(MenuOptions)
    private menuOptionsRepository: Repository<MenuOptions>,
    @InjectRepository(MenuOptionLists)
    private menuOptionListsRepository: Repository<MenuOptionLists>,
    @InjectRepository(MenuOptionListSubOptions)
    private menuOptionListSubOptionRepository: Repository<
      MenuOptionListSubOptions
    >,
    @InjectRepository(MenuItemEstimatedTimes)
    private menuItemEstimatedTimeRepository: Repository<MenuItemEstimatedTimes>,
  ) {}


  async getUsersAndOrganizations(dto: GetNameDTO, @Res() response: Response) {
    let merchantName: string = dto.name;

    if (merchantName.search(/the/i) !== -1) {
      merchantName = stringJs(merchantName).chompLeft('the ').s;
      merchantName = stringJs(merchantName).collapseWhitespace().s;
    }

    const merchantNameArray: string[] = merchantName.split(' ');
    const firstChar: string = merchantName.charAt(0);
    const lastChar: string = merchantName.charAt(merchantName.length - 1);

    const organizations = [];

    const organizationsSQLDTO = [];

    for (const merchant of merchantNameArray) {
      const orgs = await this.organizationRepository.find({
        Name: ILike(`%${merchant}%`),
      });

      orgs.forEach(gottenOrg => {
        if (
          !organizations.some(
            savedOrg => savedOrg.OrganizationID === gottenOrg.OrganizationID,
          )
        ) {
          organizations.push(gottenOrg);

          const orgDTO = new GetOrganizationsResponseDto(
            gottenOrg.OrganizationID,
            gottenOrg,
          );
          const profilesDTO = [];
          for (const profiles of gottenOrg.profiles) {
            const profileDTO = new ProfileSQLDTO(profiles.ProfileID, profiles);

            profilesDTO.push(profileDTO);
          }

          orgDTO.profiles = profilesDTO;

          organizationsSQLDTO.push(orgDTO);
        }
      });
    }

    const data = {
      organizations: organizationsSQLDTO,
    };

    return response.status(HttpStatus.OK).json(data);
  }

  async getRestaurantsByMenuItem(
    dto: GetResturantsDTO,
    @Res() response: Response,
  ): Promise<Response> {

    /* const dbResponse: Array<any> = await this.organizationRepository.manager.query(
       `RestaurantSearch @0,@1,@2,@3`,
       [dto.keywords.join(), dto.latitude, dto.longitude, dto.radius])

     if(!Array.isArray(dbResponse) || !dbResponse.length){

       let menus;

       for (const keyword of dto.keywords) {
        menus = await this.menuItemRepository.find({
           Name: ILike(`%${keyword}%`),
         })
       }

       return response
         .status(HttpStatus.OK)
         .json(
           new SuccessResponseDto(
             menus,
             HttpStatus.OK,
           ),
         );


     }else {
       return response
         .status(HttpStatus.OK)
         .json(
           new SuccessResponseDto(
             await this.organizationRepository.manager.query(
               `RestaurantSearch @0,@1,@2,@3`,
               [dto.keywords.join(), dto.latitude, dto.longitude, dto.radius]
             ),
             HttpStatus.OK,
           ),
         );
     }*/

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.organizationRepository.manager.query(
            `RestaurantSearch @0,@1,@2,@3`,
            [dto.keywords.join(), dto.latitude, dto.longitude, dto.radius]
          ),
          HttpStatus.OK,
        ),
      );

  }

  async getServiceProviders(
    dto: GetServiceProvidersDTO,
    @Res() response: Response,
  ): Promise<Response> {
    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.organizationRepository.manager.query(
            `ServiceProviderSearch @0,@1,@2,@3`,
            [dto.name, dto.latitude, dto.longitude, dto.radius],
          ),
          HttpStatus.OK,
        ),
      );
  }

  async getOrganisationLocation(
    dto: GetNameDTO,
    @Res() response: Response,
  ): Promise<Response> {
    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.organizationRepository.manager.query(
            `ProfileSearchForSiri @0,@1,@2`,
            [dto.name, dto.latitude, dto.longitude],
          ),
          HttpStatus.OK,
        ),
      );
  }

  async productSearch(
    dto: GetResturantsDTO,
    @Res() response: Response,
  ): Promise<Response> {

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.organizationRepository.manager.query(
            `ProductsByLocationSearchASC @0,@1,@2`,
            [dto.keywords.join(), dto.latitude, dto.longitude],
          ),
          HttpStatus.OK,
        ),
      );

  }
}
