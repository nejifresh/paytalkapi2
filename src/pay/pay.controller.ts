import { Body, Controller, HttpStatus, Post, Req, Res, UseGuards, UsePipes } from '@nestjs/common';
import { Request, Response } from 'express';
import stringJs from 'string';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { Organization } from '../commons/interfaces/organization.interface';
import { PaytalkUsers } from '../commons/interfaces/paytalk-users.interface';
import { Profile } from '../organizations/entities/profile.entity';
import { Campaign } from '../organizations/entities/campaign.entity';
import { ProfileDTO } from '../organizations/dto/profile.dto';
import { CampaignDTO } from '../organizations/dto/campaign.dto';
import { GetCharityResponseDto } from '../organizations/dto/GetCharityResponse.dto';
import { PayService } from './pay.service';
import { GetNameDTO } from './dto/getNameDTO';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { AuthGuard } from '../auth/auth-guard.service';
import { GetResturantsDTO } from './dto/getResturantsDTO';
import { GetServiceProvidersDTO } from './dto/getServiceProvidersDTO';
import { ApiTags } from '@nestjs/swagger';

@Controller('pay')
@ApiTags('pay')
export class PayController {

  constructor(private readonly payService: PayService) {
  }


  @UseGuards(AuthGuard)
  @Post()
  makePayment(@Req() req: Request, @Res() res: Response) {
    return res.status(HttpStatus.OK).json(req['data']);
  }

  @UseGuards(AuthGuard)
  @Post('get-users')
  async getUsers(@Req() req: Request, @Res() res: Response) {
    const { name } = req.body;
    const data = {};

    let merchantName = name;

    if (merchantName.search(/the/i) !== -1) {
      merchantName = stringJs(merchantName).chompLeft('the ').s;
      merchantName = stringJs(merchantName).collapseWhitespace().s;
    }

    const merchantNameArray: string[] = merchantName.split(' ');
    const firstChar = merchantName.charAt(0);
    const lastChar = merchantName.charAt(merchantName.length - 1);

    //Find and Add Users
    const snapshot = firebaseAdmin
      .firestore()
      .collection('users')
      .get();

    const merchantsSnap = (await snapshot).docs.map(
      doc => doc.data() as PaytalkUsers,
    );
    const merchants: PaytalkUsers[] = [];

    for (const merchant of merchantsSnap) {
      for (const name of merchantNameArray) {
        if (merchant.username.toLowerCase().includes(`${name.toLowerCase()}`)) {
          merchants.push(merchant);
        }
      }
    }

    if (merchants.length === 0) {
      for (const merchant of merchantsSnap) {
        if (
          merchant.username.startsWith(firstChar) &&
          merchant.username.endsWith(lastChar)
        ) {
          merchants.push(merchant);
        }
      }
    }

    //Find and Add Orgs
    const orgSnapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .get();


    const orgs: Array<GetCharityResponseDto> = [];

    orgSnapshot.forEach(orgRef => {
      const org: Organization = orgRef.data() as Organization;
      let charity: GetCharityResponseDto = <GetCharityResponseDto>{};
      if (org.name.toLowerCase().includes(`${merchantName.toLowerCase()}`)) {
        let profilesSnapshot: any;
        let campaignsSnapshot: any;

        firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(orgRef.id)
          .collection('profiles')
          .get()
          .then((snap) => {
            profilesSnapshot = snap;

            return firebaseAdmin
              .firestore()
              .collection('organizations')
              .doc(orgRef.id)
              .collection('campaigns')
              .get();
          }).then(snap => {
          campaignsSnapshot = snap;

          const profiles: Array<Profile> = [];
          const campaigns: Array<Campaign> = [];

          profilesSnapshot.forEach(doc => profiles.push(new ProfileDTO(doc.id, doc.data())));

          campaignsSnapshot.forEach(async doc => campaigns.push(new CampaignDTO(doc.id, doc.data())));

          charity = new GetCharityResponseDto(orgRef.id, orgRef.data());
          charity.profiles = profiles;
          charity.campaigns = campaigns;

          if (!orgs.some(gottenOrg => gottenOrg.id === charity.id)) {
            orgs.push(charity);
          }

          data['organizations'] = Array.from(new Set(orgs));
          data['users'] = Array.from(new Set(merchants));
          return res.status(200).json(data);

        }).catch(error => {
          console.log(error);
        });
      }
    });

  }

  @UseGuards(AuthGuard)
  @Post('get-restaurants')
  @UsePipes(DTOValidationPipe)
  async getRestaurants(@Body() dto: GetResturantsDTO, @Res() res: Response) {
    return this.payService.getRestaurantsByMenuItem(dto, res);
  }

  @Post('get-products')
  @UsePipes(DTOValidationPipe)
  async getProducts(@Body() dto: GetResturantsDTO, @Res() res: Response) {
    return this.payService.productSearch(dto, res);
  }

  @UseGuards(AuthGuard)
  @Post('get-providers')
  @UsePipes(DTOValidationPipe)
  async getServiceProviders(@Body() dto: GetServiceProvidersDTO, @Res() res: Response) {
    return this.payService.getServiceProviders(dto, res);
  }

  @UseGuards(AuthGuard)
  @Post('users')
  @UsePipes(DTOValidationPipe)
  getUsersSQL(@Body() getNameDTO: GetNameDTO,@Res() res: Response): Promise<Response>{
    return this.payService.getUsersAndOrganizations(getNameDTO,res);
  }

  @Post('users-no-auth')
  @UsePipes(DTOValidationPipe)
  getUsersSQLNoAuth(@Body() getNameDTO: GetNameDTO,@Res() res: Response): Promise<Response>{
    return this.payService.getUsersAndOrganizations(getNameDTO,res);
  }


  @Post('get-users-no-auth')
  async getUsersNoAuth(@Req() req: Request, @Res() res: Response) {
    const { name } = req.body;
    const data = {};

    let merchantName = name;

    if (merchantName.search(/the/i) !== -1) {
      merchantName = stringJs(merchantName).chompLeft('the ').s;
      merchantName = stringJs(merchantName).collapseWhitespace().s;
    }

    const merchantNameArray: string[] = merchantName.split(' ');
    const firstChar = merchantName.charAt(0);
    const lastChar = merchantName.charAt(merchantName.length - 1);

    //Find and Add Users
    const snapshot = firebaseAdmin
      .firestore()
      .collection('users')
      .get();

    const merchantsSnap = (await snapshot).docs.map(
      doc => doc.data() as PaytalkUsers,
    );
    const merchants: PaytalkUsers[] = [];

    for (const merchant of merchantsSnap) {
      for (const name of merchantNameArray) {
        if (merchant.username.toLowerCase().includes(`${name.toLowerCase()}`)) {
          merchants.push(merchant);
        }
      }
    }

    if (merchants.length === 0) {
      for (const merchant of merchantsSnap) {
        if (
          merchant.username.startsWith(firstChar) &&
          merchant.username.endsWith(lastChar)
        ) {
          merchants.push(merchant);
        }
      }
    }

    //Find and Add Orgs
    const orgSnapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .get();


    const orgs: Array<GetCharityResponseDto> = [];

    orgSnapshot.forEach(orgRef => {
      const org: Organization = orgRef.data() as Organization;
      let charity: GetCharityResponseDto = <GetCharityResponseDto>{};
      if (org.name.toLowerCase().includes(`${merchantName.toLowerCase()}`)) {
        let profilesSnapshot: any;
        let campaignsSnapshot: any;

        firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(orgRef.id)
          .collection('profiles')
          .get()
          .then((snap) => {
            profilesSnapshot = snap;

            return firebaseAdmin
              .firestore()
              .collection('organizations')
              .doc(orgRef.id)
              .collection('campaigns')
              .get();
          }).then(snap => {
          campaignsSnapshot = snap;

          const profiles: Array<Profile> = [];
          const campaigns: Array<Campaign> = [];

          profilesSnapshot.forEach(doc => profiles.push(new ProfileDTO(doc.id, doc.data())));

          campaignsSnapshot.forEach(async doc => campaigns.push(new CampaignDTO(doc.id, doc.data())));

          charity = new GetCharityResponseDto(orgRef.id, orgRef.data());
          charity.profiles = profiles;
          charity.campaigns = campaigns;

          if (!orgs.some(gottenOrg => gottenOrg.id === charity.id)) {
            orgs.push(charity);
          }

          data['organizations'] = Array.from(new Set(orgs));
          data['users'] = Array.from(new Set(merchants));
          return res.status(200).json(data);

        }).catch(error => {
          console.log(error);
        });
      }
    });

  }

  @Post('users-location')
  @UsePipes(DTOValidationPipe)
  getUsersSQLLocation(@Body() getNameDTO: GetNameDTO,@Res() res: Response): Promise<Response>{
    return this.payService.getOrganisationLocation(getNameDTO,res);
  }

}

