import { Controller, Get, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { Request, Response } from 'express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @Get()
  getHello(): string {
    return 'this.appService.getHello()';
  }

  @Post()
  paystackWebhook(@Req() req: Request, @Res() res: Response) {
    return res.status(HttpStatus.OK).json(req['data']);
  }
}
