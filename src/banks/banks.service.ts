import { HttpStatus, Injectable, Res } from '@nestjs/common';
import { Response } from 'express';
import { Flutterwave } from '../commons/flutterwave.config';
import { BankDto } from './dto/bank.dto';


@Injectable()
export class BanksService {
  async getBanks(@Res() response: Response): Promise<Response> {
    const payload = {
      'country': 'NG',
    };

    const banks: Array<BankDto> = await Flutterwave.Bank.country(payload);


    return response
      .status(HttpStatus.OK)
      .json(
        banks,
      );

  }

  async getBanksPaystack(@Res() response: Response): Promise<Response> {
    const payload = {
      'country': 'NG',
    };

    const banks: Array<BankDto> = await Flutterwave.Bank.country(payload);


    return response
      .status(HttpStatus.OK)
      .json(
        banks,
      );

  }
}
