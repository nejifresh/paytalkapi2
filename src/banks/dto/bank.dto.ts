import { ApiProperty } from '@nestjs/swagger';
export class BankDto {

  private id: number;

  private code: string;

  private name: string;


  constructor(id: number, code: string, name: string) {
    this.id = id;
    this.code = code;
    this.name = name;
  }
}
