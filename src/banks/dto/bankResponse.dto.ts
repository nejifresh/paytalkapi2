import { ApiProperty } from '@nestjs/swagger';
import { BankDto } from './bank.dto';
export class BankResponseDto {

  @ApiProperty()
  private status: string;

  @ApiProperty()
  private message: string;

  @ApiProperty({type:[BankDto]})
  private data: Array<BankDto>;



}
