import { Controller, Get, Res, UseGuards } from '@nestjs/common';
import { BanksService } from './banks.service';
import { Response } from 'express';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { BankResponseDto } from './dto/bankResponse.dto';

@Controller('banks')
@ApiBearerAuth()
@ApiTags('banks')
@UseGuards(AuthGuard)
export class BanksController {
  constructor(private readonly banksService: BanksService) {}

  @Get()
  @ApiOperation({ summary: 'Get Banks' })
  @ApiOkResponse({
    description: 'Banks',
    type: BankResponseDto,
  })
  getBanks(
    @Res() response: Response,
  ): Promise<Response> {
    return this.banksService.getBanks(response);
  }
}
