import { Test, TestingModule } from '@nestjs/testing';
import { GspeechController } from './gspeech.controller';

describe('GspeechController', () => {
  let controller: GspeechController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GspeechController],
    }).compile();

    controller = module.get<GspeechController>(GspeechController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
