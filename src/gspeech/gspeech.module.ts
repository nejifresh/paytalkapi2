import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { GspeechController } from './gspeech.controller';
import { FileUploadMiddleware } from '../commons/middlewares/file-upload.middleware';

@Module({
  controllers: [GspeechController],
})
export class GspeechModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(FileUploadMiddleware).forRoutes('gspeech');
  }
}
