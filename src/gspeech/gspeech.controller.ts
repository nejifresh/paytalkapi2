import { Controller, Post, Req, Res, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import { gSpeech2Text } from '../commons/googleTextToSpeechConfig';

@Controller('gspeech')
export class GspeechController {
  @Post('/')
  async speech(@Req() req: Request, @Res({ passthrough: true }) res: Response) {
    const { languageCode } = req.body;
    const audio = req.files[0];
    console.log(audio);
    console.log(req.files);

    return res
      .status(HttpStatus.OK)
      .json({ text: await gSpeech2Text(languageCode, audio.filepath) });
  }
}
