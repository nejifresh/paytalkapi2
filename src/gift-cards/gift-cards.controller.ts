import { Body, Controller, Get, Param, Post, Query, Res } from '@nestjs/common';
import { GiftCardsService } from './gift-cards.service';
import { Response } from 'express';
import { OrderGiftCardDto } from './dto/create-gift-card.dto';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';


@Controller('gift-cards')
@ApiBearerAuth()
@ApiTags('Gift-Cards')
export class GiftCardsController {
  constructor(private readonly giftCardsService: GiftCardsService) {
  }

  @Get()
  findAll(
    @Query('page') page: number,
    @Query('pageSize') pageSize: number, @Res() response: Response,
  ) {
    return this.giftCardsService.findAll(page, pageSize, response);
  }

  @Get('/country/:countryCode')
  findAllByCountry(
    @Param('countryCode') countryCode: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number, @Res() response: Response,
  ) {
    return this.giftCardsService.findAllByCountryCode(countryCode, page, pageSize, response);
  }

  @Get(':id')
  findOne(@Param('id') id: number,
          @Res() response: Response) {
    return this.giftCardsService.findOne(+id, response);
  }

  @Post('order')
  update(@Body() orderGiftCardDto: OrderGiftCardDto, @Res() response: Response) {
    return this.giftCardsService.orderGiftCard(orderGiftCardDto, response);
  }

  @Get('/purchased/all')
  @ApiOperation({ summary: 'Get Gift Cards' })
  @ApiImplicitQuery({
    name: 'propName',
    description: 'Property Name',
    required: false,
    type: String,
  })
  @ApiImplicitQuery({
    name: 'propVal',
    description: 'Property Value',
    required: false,
    type: String,
  })
  findAllPurchased(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ): Promise<Response> {
    if (propName && propVal) {
      const filter = { propName, propVal };
      return this.giftCardsService.findAllPurchased(response, filter);
    } else {
      return this.giftCardsService.findAllPurchased(response);
    }
  }

  @Get('purchased/:id')
  @ApiOperation({ summary: 'Get Gift Card' })
  findOnePurchased(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.giftCardsService.findOnePurchased(id, response);
  }
}
