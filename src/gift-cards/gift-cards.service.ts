import { BadRequestException, HttpService, HttpStatus, Injectable, NotFoundException, Res } from '@nestjs/common';

import * as dotEnv from 'dotenv';
import { Response } from 'express';
import { OrderGiftCardDto } from './dto/create-gift-card.dto';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';

dotEnv.config();

@Injectable()
export class GiftCardsService {

  constructor(private httpService: HttpService) {
  }

  async getAuthToken() {
    const authResponse = await this.httpService.post(process.env.reloadlyAuthURL, {
      'client_id': process.env.reloadlyClientId,
      'client_secret': process.env.reloadlyClientSecret,
      'grant_type': 'client_credentials',
      'audience': process.env.reloadlyGiftCardBaseURL,
    }).toPromise();

    const { access_token } = authResponse.data;

    return access_token;
  }


  async findAll(page: number, pageSize: number, response: Response) {

    return this.httpService.get(`${process.env.reloadlyGiftCardBaseURL}/products?page=${page}&size=${pageSize}`,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${await this.getAuthToken()}`,
        },
      }).toPromise().then(products => response.status(HttpStatus.CREATED).json(products.data))
      .catch(error => {
        throw new BadRequestException(error.response.data);
      });


  }

  async findAllByCountryCode(countryCode: string, page: number, pageSize: number, response: Response) {

    return this.httpService.get(`${process.env.reloadlyGiftCardBaseURL}/countries/${countryCode}/products`,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${await this.getAuthToken()}`,
        },
      }).toPromise().then(products => response.status(HttpStatus.CREATED).json(products.data))
      .catch(error => {
        throw new BadRequestException(error.response.data);
      });


  }

  async findOne(id: number, response: Response) {
    return this.httpService.get(`${process.env.reloadlyGiftCardBaseURL}/products/${id}`,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${await this.getAuthToken()}`,
        },
      }).toPromise().then(products => response.status(HttpStatus.CREATED).json(products.data))
      .catch(error => {
        throw new BadRequestException(error.response.data);
      });
  }

  async orderGiftCard(orderGiftCardDto: OrderGiftCardDto, response: Response) {
    return this.httpService.post(`${process.env.reloadlyGiftCardBaseURL}/orders`, orderGiftCardDto,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${await this.getAuthToken()}`,
        },
      }).toPromise().then(async products => {

      const giftCard = await firebaseAdmin.firestore().collection('giftCards').add(products.data);

      await giftCard.update({ id: giftCard.id });

      return response.status(HttpStatus.CREATED).json(products.data);
    })
      .catch(error => {
        throw new BadRequestException(error.response.data);
      });
  }

  async findAllPurchased(
    @Res() response: Response,
    filter?: { propName: string; propVal: string },
  ): Promise<Response> {
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('giftCards')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('giftCards')
        .get();
    }

    const giftCards: Array<any> = [];

    snapshot.forEach(doc => {
      console.log(doc.data());
      giftCards.push(doc.data());
    });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(giftCards, HttpStatus.OK));
  }

  async findOnePurchased(id: string, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('giftCards')
      .doc(id)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('Gift Card Id is not valid');
    }

    const giftCard = snapshot.data();

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(giftCard, HttpStatus.OK));
  }
}
