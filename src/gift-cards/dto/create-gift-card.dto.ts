import { ApiProperty } from '@nestjs/swagger';

export class RecipientPhoneDetails {
  @ApiProperty()
  countryCode: string;

  @ApiProperty()
  phoneNumber: number;
}


export class OrderGiftCardDto {

  @ApiProperty()
  productId: number;

  @ApiProperty()
  countryCode: string;

  @ApiProperty()
  quantity: number;

  @ApiProperty()
  unitPrice: number;

  @ApiProperty()
  customIdentifier: string;

  @ApiProperty()
  senderName: string;

  @ApiProperty()
  recipientEmail?: string;

  @ApiProperty()
  recipientPhoneDetails?: RecipientPhoneDetails;

}

