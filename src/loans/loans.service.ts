import { HttpStatus, Injectable, NotFoundException, Res } from '@nestjs/common';
import { CreateLoanDto } from './dto/create-loan.dto';
import { Response } from 'express';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { Loan } from './entities/loan.entity';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { LoanRequestDto } from './dto/LoanRequest.dto';
import { ILoan } from './entities/loans.interface';
import { ApproveLoanDto } from './dto/approveLoan.dto';

@Injectable()
export class LoansService {
  async create(createLoanDto: CreateLoanDto, response: Response) {

    const loan = new Loan(createLoanDto);

    await firebaseAdmin.firestore()
      .collection('loanRequests')
      .add({ ...loan });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto('Loan Request Created', HttpStatus.OK));
  }

  async findAllRequests(
    @Res() response: Response,
    filter?: { propName: string; propVal: string },
  ): Promise<Response> {
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('loanRequests')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('loanRequests')
        .get();
    }

    const loanRequests: Array<LoanRequestDto> = [];

    snapshot.forEach(doc => {
      loanRequests.push(new LoanRequestDto(doc.id, doc.data() as ILoan));
    });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(loanRequests, HttpStatus.OK));
  }

  async findOneRequest(id: string, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('loanRequests')
      .doc(id)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('Loan Request Id is not valid');
    }

    const loanRequest = new LoanRequestDto(snapshot.id, snapshot.data() as ILoan);

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(loanRequest, HttpStatus.OK));
  }

  async approveRequest(approveLoad: ApproveLoanDto, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('loanRequests')
      .doc(approveLoad.id)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('Loan Request Id is not valid');
    }

    await snapshot.ref.update({ status: 'APPROVED' });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto('Loan Request Approved', HttpStatus.OK));
  }
}
