import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class CreateLoanDto {

  @ApiProperty()
  @IsDefined()
  amount: number;

  @ApiProperty()
  @IsDefined()
  city: string;

  @ApiProperty()
  @IsDefined()
  country: string;

  @ApiProperty()
  @IsDefined()
  currency: string;

  @ApiProperty()
  @IsDefined()
  email: string;

  @ApiProperty()
  @IsDefined()
  longitude: number;

  @ApiProperty()
  @IsDefined()
  latitude: number;

  @ApiProperty()
  @IsDefined()
  userId: string;

}
