import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';
import { ILoan } from '../entities/loans.interface';

export class LoanRequestDto {

  @ApiProperty()
  @IsDefined()
  id: string;

  @ApiProperty()
  @IsDefined()
  amount: number;

  @ApiProperty()
  @IsDefined()
  city: string;

  @ApiProperty()
  @IsDefined()
  country: string;

  @ApiProperty()
  @IsDefined()
  currency: string;

  @ApiProperty()
  @IsDefined()
  email: string;

  @ApiProperty()
  @IsDefined()
  longitude: number;

  @ApiProperty()
  @IsDefined()
  latitude: number;

  @ApiProperty()
  @IsDefined()
  userId: string;

  @ApiProperty()
  @IsDefined()
  status: string;

  @ApiProperty()
  @IsDefined()
  dateOfRequest: Date;


  constructor(id: string, loanRequest: ILoan) {
    this.id = id;
    this.amount = loanRequest.amount;
    this.city = loanRequest.city;
    this.country = loanRequest.country;
    this.currency = loanRequest.currency;
    this.email = loanRequest.email;
    this.longitude = loanRequest.location.longitude;
    this.latitude = loanRequest.location.latitude;
    this.userId = loanRequest.userId;
    this.status = loanRequest.status;
    this.dateOfRequest = loanRequest.dateOfRequest.toDate();
  }
}

