import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class ApproveLoanDto {

  @ApiProperty()
  @IsDefined()
  id: string;
}
