import { Body, Controller, Get, Param, Post, Query, Res, UseGuards } from '@nestjs/common';
import { LoansService } from './loans.service';
import { CreateLoanDto } from './dto/create-loan.dto';
import { Response } from 'express';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import { LoanRequestDto } from './dto/LoanRequest.dto';
import { ApproveLoanDto } from './dto/approveLoan.dto';
import { AuthGuard } from '../auth/auth-guard.service';

@Controller('loans')
@ApiBearerAuth()
@ApiTags('Loans')
export class LoansController {
  constructor(private readonly loansService: LoansService) {
  }

  @Post('create-request')
  create(@Body() createLoanDto: CreateLoanDto,
         @Res() response: Response) {
    return this.loansService.create(createLoanDto, response);
  }

  @ApiOperation({ summary: 'Get Loan Requests' })
  @ApiOkResponse({
    description: 'Loan Requests',
    type: LoanRequestDto,
  })
  @ApiImplicitQuery({
    name: 'propName',
    description: 'Property Name',
    required: false,
    type: String,
  })
  @ApiImplicitQuery({
    name: 'propVal',
    description: 'Property Value',
    required: false,
    type: String,
  })
  //@UseGuards(AuthGuard)
  @Get()
  findAllRequests(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ): Promise<Response> {
    if (propName && propName.length > 0) {
      const filter = {
        propName,
        propVal,
      };

      return this.loansService.findAllRequests(response, filter);
    } else {
      return this.loansService.findAllRequests(response);
    }
  }

  @ApiOperation({ summary: 'Get Loan Request' })
  @ApiOkResponse({
    description: 'Loan Request',
    type: LoanRequestDto,
  })
  //@UseGuards(AuthGuard)
  @Get(':id')
  findOnePending(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.loansService.findOneRequest(id, response);
  }

  @UseGuards(AuthGuard)
  @Post('approve')
  approveLoadRequest(
    @Body() approveLoad: ApproveLoanDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.loansService.approveRequest(approveLoad, response);
  }
}
