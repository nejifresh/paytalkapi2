import firebase from 'firebase';
import GeoPoint = firebase.firestore.GeoPoint;
import Timestamp = firebase.firestore.Timestamp;

export interface ILoan {
  amount: number;

  city: string;

  country: string;

  currency: string;

  email: string;

  location: GeoPoint;

  userId: string;

  status: string;

  dateOfRequest: Timestamp;
}
