import firebase from 'firebase';
import { CreateLoanDto } from '../dto/create-loan.dto';
import { adminTypes } from '../../commons/firebaseConfig';
import Timestamp = firebase.firestore.Timestamp;
import GeoPoint = firebase.firestore.GeoPoint;

export class Loan {

  amount: number;

  city: string;

  country: string;

  currency: string;

  email: string;

  location: GeoPoint;

  userId: string;

  status: string;

  dateOfRequest: Timestamp;


  constructor(createLoan: CreateLoanDto) {
    this.amount = createLoan.amount;
    this.city = createLoan.city;
    this.country = createLoan.country;
    this.currency = createLoan.currency;
    this.email = createLoan.email;
    this.location = new adminTypes.firestore.GeoPoint(createLoan.latitude, createLoan.longitude);
    this.userId = createLoan.userId;
    this.status = 'PENDING';
    this.dateOfRequest = adminTypes.firestore.Timestamp.now();
  }
}
