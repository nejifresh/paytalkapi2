import { BadRequestException, CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { firebaseAdmin } from '../commons/firebaseConfig';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const headers = request.headers;
    const {authorization} = headers;
    if (!authorization) {
      throw new BadRequestException('authorization header is missing');
    }

    if (!authorization.startsWith("Bearer")) {
      throw new BadRequestException('authorization header is not valid')
    }

    const header = authorization.split(" ");

    if (header.length !== 2) {
      throw new BadRequestException('authorization header is not valid')
    }
    const token = header[1];

    return this.validateToken(token);
  }

 async validateToken (token: string): Promise<boolean> {
 return  await firebaseAdmin.auth().verifyIdToken(token)
      .then((decodedToken) => {
        //req.uuid = decodedToken.sub;
        return true
      })
      .catch(() => {
        throw new UnauthorizedException('Token is not valid');
      });
  }
}
