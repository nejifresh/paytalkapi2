import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsDefined, IsEmail, IsNotEmpty } from 'class-validator';

export class AddUserDTO {

  @IsDefined()
  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsDefined()
  @ApiProperty()
  @IsNotEmpty()
  password: string;

  @IsDefined()
  @ApiProperty()
  @IsNotEmpty()
  firstName: string;

  @IsDefined()
  @ApiProperty()
  @IsNotEmpty()
  lastName: string;

  @IsDefined()
  @ApiProperty()
  @IsArray()
  @IsNotEmpty()
  roles: Array<string>;

}
