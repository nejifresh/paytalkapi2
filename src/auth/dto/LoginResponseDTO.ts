import { ApiProperty } from '@nestjs/swagger';

export class LoginResponseDto {
  @ApiProperty()
  private token: string;

  private user: any;

  constructor(token: string, user: any) {
    this.token = token;
    this.user = user;
  }
}
