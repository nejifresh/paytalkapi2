import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsNotEmpty } from 'class-validator';

export class LoginDto {
  @IsDefined()
  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;
  @IsDefined()
  @ApiProperty()
  @IsNotEmpty()
  password: string;

}
