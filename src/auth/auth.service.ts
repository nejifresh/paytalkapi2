import { HttpStatus, Injectable, Res, UnauthorizedException } from '@nestjs/common';
import { LoginDto } from './dto/LoginDTO';
import { LoginResponseDto } from './dto/LoginResponseDTO';

import { Response } from 'express';
import { firebaseAdmin, firebaseClient } from '../commons/firebaseConfig';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { AddUserDTO } from './dto/AddUserDTO';


@Injectable()
export class AuthService {

  async login(
    loginDto: LoginDto,
    @Res() response: Response,
  ): Promise<Response> {
    const { email, password } = loginDto;

    const token = await firebaseClient.auth().signInWithEmailAndPassword(email, password)
      .then(() => {
        return firebaseClient.auth().currentUser.getIdToken();
      }).then((token) => {
        return token;
      })
      .catch(() => {
        throw new UnauthorizedException('Invalid credentials');
      });

    const user = firebaseClient.auth().currentUser;


    const docRef = await firebaseAdmin.firestore()
      .collection('admin')
      .doc(user.uid).get();

    /*if (!docRef.exists) {
      throw new UnauthorizedException('User is not an Admin');
    }*/

    const doc = await docRef.data();


    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(new LoginResponseDto(token, doc), HttpStatus.OK),
      );
  }


  async addNewUser(
    addUser: AddUserDTO,
    @Res() response: Response,
  ): Promise<Response> {

    const userDocRef = await firebaseAdmin.firestore()
      .collection('admin')
      .where('email', '==', addUser.email)
      .get();

    if (userDocRef.empty) {
      return response
        .status(HttpStatus.BAD_REQUEST)
        .json(
          new SuccessResponseDto('User is Already an Admin', HttpStatus.BAD_REQUEST),
        );
    }

    firebaseAdmin.auth().getUserByEmail(addUser.email)
      .then(async (user) => {
        await firebaseAdmin.firestore()
          .collection('admin')
          .doc(user.uid)
          .create({
            email: addUser.email,
            firstName: addUser.firstName,
            lastName: addUser.lastName,
            imageURL: null,
            active: true,
            roles: addUser.roles,
          });
      })
      .catch(async () => {
        const newUser = await firebaseAdmin.auth().createUser({
          email: addUser.email,
          password: addUser.password,
        });

        await firebaseAdmin.firestore()
          .collection('admin')
          .doc(newUser.uid)
          .create({
            email: addUser.email,
            firstName: addUser.firstName,
            lastName: addUser.lastName,
            imageURL: null,
            active: true,
            roles: addUser.roles,
          });

      });

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto('User Added', HttpStatus.OK),
      );
  }

}
