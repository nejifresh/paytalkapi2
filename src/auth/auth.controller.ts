import { Body, Controller, Post, Res, UseGuards, UsePipes } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/LoginDTO';
import { Response } from 'express';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { LoginResponseDto } from './dto/LoginResponseDTO';
import { AddUserDTO } from './dto/AddUserDTO';
import { AuthGuard } from './auth-guard.service';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Post('login')
  @ApiOperation({ summary: 'Log in' })
  @ApiOkResponse({
    description: 'Login token',
    type: LoginResponseDto,
  })
  @UsePipes(DTOValidationPipe)
  login(
    @Body() loginDto: LoginDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.authService.login(loginDto, response);
  }


  @UseGuards(AuthGuard)
  @Post('addUser')
  @ApiOperation({ summary: 'Add New User' })
  @UsePipes(DTOValidationPipe)
  addUser(
    @Body() addUserDTO: AddUserDTO,
    @Res() response: Response,
  ): Promise<Response> {
    return this.authService.addNewUser(addUserDTO, response);
  }
}
