import { BadRequestException, createParamDecorator, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { firebaseAdmin } from '../../commons/firebaseConfig';


export const GetUid = createParamDecorator(async (data: unknown, ctx: ExecutionContext): Promise<string> => {
     const request = ctx.switchToHttp().getRequest();
     const headers = request.headers;
     const { authorization } = headers;
     if (!authorization) {
          throw new BadRequestException('authorization header is missing');
     }

     if (!authorization.startsWith('Bearer')) {
          throw new BadRequestException('authorization header is not valid');
     }

     const header = authorization.split(' ');

     if (header.length !== 2) {
          throw new BadRequestException('authorization header is not valid');
     }
     const token = header[1];

     return await firebaseAdmin.auth().verifyIdToken(token)
       .then((decodedToken) => {
            return decodedToken.sub;
       })
       .catch(() => {
            throw new UnauthorizedException('Token is not valid');
       });
});

