import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class GetDriverByOrganisationDto {
  @ApiProperty()
  @IsDefined()
  organisationId: string;
}
