import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class FindDriverDto {

  @ApiProperty()
  @IsDefined()
  longitude: number

  @ApiProperty()
  @IsDefined()
  latitude: number

  @ApiProperty()
  @IsDefined()
  radius: number
}
