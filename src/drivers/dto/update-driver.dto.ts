import { PartialType } from '@nestjs/mapped-types';
import { FindDriverDto } from './find-driver.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class UpdateDriverDto{


  @ApiProperty()
  @IsDefined()
  driverId: string;

  @ApiProperty()
  @IsDefined()
  longitude: number

  @ApiProperty()
  @IsDefined()
  latitude: number

}
