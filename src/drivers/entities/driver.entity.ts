import {BaseEntity, Column, Entity, PrimaryColumn} from 'typeorm';

@Entity('Drivers')
export class Driver extends BaseEntity  {

  @PrimaryColumn()
  DriverID: string;

  @Column()
  OrganizationID: string;

  @Column({type:'float'})
  LastLocationLongitude: number;

  @Column({type:'float'})
  LastLocationLatitude: number;

  @Column()
  LastLocationUpdate: Date;

  @Column()
  IsActive: boolean;

  @Column()
  DateAdded: Date;

  @Column()
  IsOnline: boolean;

  @Column()
  IsOnATrip: boolean;
}