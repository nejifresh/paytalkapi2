import { Body, Controller, Get, Param, Post, Res } from '@nestjs/common';
import { DriversService } from './drivers.service';
import { FindDriverDto } from './dto/find-driver.dto';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { UpdateDriverDto } from './dto/update-driver.dto';

@Controller('driver')
@ApiTags('Drivers')
export class DriversController {
  constructor(private readonly driversService: DriversService) {
  }

  @Post()
  @ApiOperation({ summary: 'Get Available Drivers' })
  @ApiOkResponse()
  findDriver(@Body() findDriverDto: FindDriverDto, @Res() res: Response) {
    return this.driversService.getDrivers(findDriverDto, res);
  }

  @Get('company/:id')
  @ApiOperation({ summary: 'Get Available Drivers in Company' })
  @ApiOkResponse()
  getDriverByCompany(@Param('id') id: string, @Res() res: Response) {
    return this.driversService.getDriversByOrganisation(id, res);
  }

  @Post('location')
  @ApiOperation({ summary: 'Update Driver Location' })
  @ApiOkResponse()
  updateDriverLocation(@Body() updateDriverDto: UpdateDriverDto, @Res() res: Response) {
    return this.driversService.updateDriverLocation(updateDriverDto, res);
  }

  @ApiOperation({ summary: 'Toggle Driver Online Status' })
  @ApiOkResponse()
  @Get('online/:id')
  toggleOnline(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.driversService.toggleOnline(id, response);
  }

  @ApiOperation({ summary: 'Toggle Driver Trip Status' })
  @ApiOkResponse()
  @Get('trip/:id')
  toggleTrip(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.driversService.toggleTrip(id, response);
  }

  @ApiOperation({ summary: 'Get Driver Location' })
  @ApiOkResponse()
  @Get('location/:id')
  getDriverLocation(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.driversService.getLocation(id, response);
  }


}
