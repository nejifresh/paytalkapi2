import { HttpStatus, Injectable, Res } from '@nestjs/common';
import { Response } from 'express';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { getManager, Repository } from 'typeorm';
import { FindDriverDto } from './dto/find-driver.dto';
import { UpdateDriverDto } from './dto/update-driver.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Driver } from './entities/driver.entity';
import firebase from 'firebase';
import { IDriverSql } from '../commons/interfaces/paytalk-driver-sql.interface';
import { firebaseAdmin } from '../commons/firebaseConfig';
import GeoPoint = firebase.firestore.GeoPoint;

@Injectable()
export class DriversService {

  constructor(
    @InjectRepository(Driver) private driverRepository: Repository<Driver>) {
  }

  async getDrivers(
    dto: FindDriverDto,
    @Res() response: Response,
  ): Promise<Response> {

    const entityManager = getManager();

    const drivers : Array<IDriverSql> = await entityManager.query(
      `AvailableDriverSearch_DriverID @0,@1,@2`,
      [dto.latitude, dto.longitude, dto.radius]
    );

    for (const driver of drivers) {
      const payload = {
        notification: {
          title: "New Ride Request",
          body: `New Ride from here to Big Mac`
        }
      };

      firebaseAdmin.messaging().sendToTopic(driver.driverId, payload)
        .then(function(response) {
          console.log("Successfully sent message:", response);
        })
        .catch(function(error) {
          console.log("Error sending message:", error);
        });console.log(driver)
    }

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          "Request sent to all available Drivers",
          HttpStatus.OK,
        ),
      );
  }

  async updateDriverLocation(
    dto: UpdateDriverDto,
    @Res() response: Response,
  ): Promise<Response> {

   const driver = await this.driverRepository.findOneOrFail(dto.driverId)

    driver.LastLocationLatitude = dto.latitude
    driver.LastLocationLongitude = dto.longitude

    this.driverRepository.save(driver);

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          'Driver Location Updated',
          HttpStatus.OK,
        ),
      );
  }

  async toggleOnline(
    id: string,
    @Res() response: Response,
  ): Promise<Response> {

    const driver = await this.driverRepository.findOneOrFail(id)

    driver.IsOnline = !driver.IsOnline

    this.driverRepository.save(driver);

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          'Driver Online Status toggled',
          HttpStatus.OK,
        ),
      );
  }

  async toggleTrip(
    id: string,
    @Res() response: Response,
  ): Promise<Response> {

    const driver = await this.driverRepository.findOneOrFail(id)

    driver.IsOnATrip = !driver.IsOnATrip

    this.driverRepository.save(driver);

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          'Driver Trip Status toggled',
          HttpStatus.OK,
        ),
      );
  }

  async getLocation(
    id: string,
    @Res() response: Response,
  ): Promise<Response> {

    const driver = await this.driverRepository.findOneOrFail(id)
    
    const location = new GeoPoint(driver.LastLocationLatitude,driver.LastLocationLongitude)

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          location,
          HttpStatus.OK,
        ),
      );
  }

  async getDriversByOrganisation(
    id: string,
    @Res() response: Response,
  ): Promise<Response> {

    const entityManager = getManager();

    const drivers: Array<IDriverSql> = await entityManager.query(
      `DriverSearch_CompanyID @0`,
      [id],
    );

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          drivers,
          HttpStatus.OK,
        ),
      );
  }

}
