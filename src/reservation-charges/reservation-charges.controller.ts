import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { ReservationChargesService } from './reservation-charges.service';
import { CreateReservationChargeDto } from './dto/create-reservation-charge.dto';
import { UpdateReservationChargeDto } from './dto/update-reservation-charge.dto';

@Controller('reservation-charges')
export class ReservationChargesController {
  constructor(private readonly reservationChargesService: ReservationChargesService) {}

  @Post()
  create(@Body() createReservationChargeDto: CreateReservationChargeDto) {
    return this.reservationChargesService.create(createReservationChargeDto);
  }

  @Get()
  findAll() {
    return this.reservationChargesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reservationChargesService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateReservationChargeDto: UpdateReservationChargeDto) {
    return this.reservationChargesService.update(+id, updateReservationChargeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reservationChargesService.remove(+id);
  }
}
