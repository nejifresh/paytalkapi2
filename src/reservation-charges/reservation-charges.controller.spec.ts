import { Test, TestingModule } from '@nestjs/testing';
import { ReservationChargesController } from './reservation-charges.controller';
import { ReservationChargesService } from './reservation-charges.service';

describe('ReservationChargesController', () => {
  let controller: ReservationChargesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReservationChargesController],
      providers: [ReservationChargesService],
    }).compile();

    controller = module.get<ReservationChargesController>(ReservationChargesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
