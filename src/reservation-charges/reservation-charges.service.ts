import { Injectable } from '@nestjs/common';
import { CreateReservationChargeDto } from './dto/create-reservation-charge.dto';
import { UpdateReservationChargeDto } from './dto/update-reservation-charge.dto';

@Injectable()
export class ReservationChargesService {
  create(createReservationChargeDto: CreateReservationChargeDto) {
    return 'This action adds a new reservationCharge';
  }

  findAll() {
    return `This action returns all reservationCharges`;
  }

  findOne(id: number) {
    return `This action returns a #${id} reservationCharge`;
  }

  update(id: number, updateReservationChargeDto: UpdateReservationChargeDto) {
    return `This action updates a #${id} reservationCharge`;
  }

  remove(id: number) {
    return `This action removes a #${id} reservationCharge`;
  }
}
