import { PartialType } from '@nestjs/mapped-types';
import { CreateReservationChargeDto } from './create-reservation-charge.dto';

export class UpdateReservationChargeDto extends PartialType(CreateReservationChargeDto) {}
