export class ReservationCharge {
  amountCharged: number;
  chargeTransactionId: string;
  currency: string;
  customerId: string;
  dateOfCharge: any;
  owner: string;
  paymentGateway: string;
  profile: string;
  reservationId: string;
  status: string;
}

export enum ReservationChargeStatus {
  success = 'SUCCESS',
  failed = 'FAILED',
}
