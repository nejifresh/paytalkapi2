import { Test, TestingModule } from '@nestjs/testing';
import { ReservationChargesService } from './reservation-charges.service';

describe('ReservationChargesService', () => {
  let service: ReservationChargesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReservationChargesService],
    }).compile();

    service = module.get<ReservationChargesService>(ReservationChargesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
