import { Module } from '@nestjs/common';
import { ReservationChargesService } from './reservation-charges.service';
import { ReservationChargesController } from './reservation-charges.controller';

@Module({
  controllers: [ReservationChargesController],
  providers: [ReservationChargesService]
})
export class ReservationChargesModule {}
