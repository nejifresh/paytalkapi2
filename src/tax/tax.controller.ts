import { Body, Controller, Post, Res, UseGuards, UsePipes } from '@nestjs/common';
import { TaxService } from './tax.service';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { CalculateTaxDto } from './dto/calculateTax.dto';
import { Response } from 'express';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { ShowTaxDto } from './dto/showTax.dto';

@Controller('tax')
@ApiBearerAuth()
@ApiTags('tax')
@UseGuards(AuthGuard)
export class TaxController {
  constructor(private readonly taxService: TaxService) {}

  @Post('calculate')
  @ApiOperation({ summary: 'Calculate Tax' })
  @UsePipes(DTOValidationPipe)
  calculateTax(@Body() calculateTaxDto: CalculateTaxDto,
         @Res() response: Response) {
    return this.taxService.calculateTax(calculateTaxDto,response);
  }

  @Post('show')
  @ApiOperation({ summary: 'Show Tax For Location' })
  @UsePipes(DTOValidationPipe)
  showTax(@Body() showTaxDto: ShowTaxDto,
         @Res() response: Response) {
    return this.taxService.showTaxForLOcation(showTaxDto,response);
  }

}
