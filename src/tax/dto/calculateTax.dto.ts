import { ApiProperty } from '@nestjs/swagger';

export class CalculateTaxDto {
  @ApiProperty()
  fromCountry: string

  @ApiProperty()
  fromZip: string

  @ApiProperty()
  fromState: string

  @ApiProperty()
  fromCity: string

  @ApiProperty()
  fromStreet: string

  @ApiProperty()
  toCountry: string

  @ApiProperty()
  toZip: string

  @ApiProperty()
  toState: string

  @ApiProperty()
  toCity: string

  @ApiProperty()
  toStreet: string

  @ApiProperty()
  amount: number

}