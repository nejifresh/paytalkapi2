import { ApiProperty } from '@nestjs/swagger';

export class ShowTaxDto {
  @ApiProperty()
  country: string

  @ApiProperty()
  zip: string

  @ApiProperty()
  state: string

  @ApiProperty()
  city: string

  @ApiProperty()
  street: string
}