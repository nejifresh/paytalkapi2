import { HttpStatus, Injectable, Res } from '@nestjs/common';
import { CalculateTaxDto } from './dto/calculateTax.dto';
import { taxjar } from './taxjar.config';
import { logger } from '../commons/firebaseConfig';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { Response } from 'express';
import { ErrorResponseDto } from '../commons/dto/errorResponse.dto';
import { ShowTaxDto } from './dto/showTax.dto';

@Injectable()
export class TaxService {
  calculateTax(calculateTaxDto: CalculateTaxDto,
               @Res() response: Response): Promise<Response> {


    return taxjar.taxForOrder({
      from_country: calculateTaxDto.fromCountry,
      from_zip: calculateTaxDto.fromZip,
      from_state: calculateTaxDto.fromZip,
      from_city: calculateTaxDto.fromCity,
      from_street: calculateTaxDto.fromStreet,
      to_country: calculateTaxDto.toCountry,
      to_zip: calculateTaxDto.toZip,
      to_state: calculateTaxDto.toState,
      to_city: calculateTaxDto.toCity,
      to_street: calculateTaxDto.toStreet,
      amount: calculateTaxDto.amount,
      shipping: 0
    }).then(tax => {
      return response
        .status(HttpStatus.OK)
        .json(new SuccessResponseDto(tax, HttpStatus.OK));
    })
      .catch(error => {
        logger.error(error)
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(
            new ErrorResponseDto(
              `Error Calculating Tax ${error.message}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            ),
          );
      })
  }

  showTaxForLOcation(showTaxDto: ShowTaxDto,
               @Res() response: Response): Promise<Response> {


    return taxjar.ratesForLocation(showTaxDto.zip,{
      street: showTaxDto.street,
      city: showTaxDto.city,
      state: showTaxDto.state,
      country: showTaxDto.country

    }).then(tax => {
      return response
        .status(HttpStatus.OK)
        .json(new SuccessResponseDto(tax, HttpStatus.OK));
    })
      .catch(error => {
        logger.error(error)
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(
            new ErrorResponseDto(
              `Error Calculating Tax ${error.message}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            ),
          );
      })
  }

}
