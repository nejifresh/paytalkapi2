import Taxjar from 'taxjar';
import * as dotEnv from 'dotenv';
dotEnv.config();



export const taxjar = new Taxjar({
  apiKey: process.env.TAXJAR_API_KEY
});