import { Module, HttpModule } from '@nestjs/common';
import { PlatformController } from './platform.controller';
import { PlatformService } from './platform.service';

@Module({
  controllers: [PlatformController],
  imports: [
    HttpModule.register({
      baseURL: 'https://api-m.sandbox.paypal.com/v1/',
      headers: {
        Authorization:
          'Basic ' +
          Buffer.from(
            `${process.env.paypalSandboxClientId}:${process.env.paypalSanboxClientSecret}`,
          ).toString('base64'),
      },
    }),
  ],
  providers: [PlatformService],
})
export class PlatformModule {}
