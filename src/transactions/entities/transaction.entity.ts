import firebase from 'firebase';
import { adminTypes } from '../../commons/firebaseConfig';
import Timestamp = firebase.firestore.Timestamp;

export class Transaction {
  amount: number;
  currency: string;
  dateOfTransaction: Timestamp;
  gateway: string;
  transactionId: string;
  type?: string;
  status: string;
  senderUid: string;
  senderEmail: string;
  beneficiary?: Beneficiary;
  campaign?: Campaign;
  organizationId: string;
  imageUrl: string;
  splitRatio?: number;
  platformCommission?: number;
  gatewayCommission?: number;
  amountCredited?: number;
  accountId?: string;
  customerId?: string;
  interval?: string;
  productId?: string;
  paymentMethodId?: string;

  constructor(
    amount: number,
    currency: string,
    organizationId: string,
    senderUid: string,
    senderEmail: string,
    gateway: string,
    tref: string,
    beneficiary: Beneficiary,
    campaign: Campaign,
    imageUrl: string,
    type?: string,
    accountId?: string,
    customerId?: string,
    interval?: string,
    productId?: string,
    paymentMethodId?: string,
  ) {
    this.amount = amount;
    this.currency = currency;
    this.dateOfTransaction = adminTypes.firestore.Timestamp.now();
    this.gateway = gateway;
    this.transactionId = tref;
    this.type = type ? type : 'donation';
    this.status = 'pending';
    this.senderEmail = senderEmail;
    this.senderUid = senderUid;
    this.beneficiary = beneficiary;
    this.campaign = campaign;
    this.organizationId = organizationId;
    this.imageUrl = imageUrl;
    this.accountId = accountId ? accountId : null;
    this.customerId = customerId ? customerId : null;
    this.interval = interval ? interval : null;
    this.productId = productId ? productId : null;
    this.paymentMethodId = paymentMethodId ? paymentMethodId : null;
  }
}

export class Beneficiary {
  profileId: string;
  name: string;
}

export class Campaign {
  title: string;
  id: string;
}

export class Sender {
  email: string;
  //name: string;
  uid: string;
}
