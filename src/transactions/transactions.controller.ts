import { Body, Controller, Get, Param, Post, Query, Res, UseGuards, UsePipes } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { Response } from 'express';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { TransactionDto } from './dto/transaction.dto';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { GetUid } from '../auth/decorators/getAuthenticatedUser.decorator';
import { CreateTransactionDTO } from './dto/createTransactionDTO';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';

@Controller('transactions')
@ApiBearerAuth()
@ApiTags('transactions')
@UseGuards(AuthGuard)
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) {}

  @Post()
  @ApiOperation({ summary: 'Create Transaction' })
  @ApiCreatedResponse({
    description: 'Transaction',
    type: TransactionDto,
  })
  @UsePipes(DTOValidationPipe)
  create(
    @Body() createTransactionDTO: CreateTransactionDTO,
    @GetUid() loggedInUserId: string,
    @Res() response: Response,
  ): Promise<Response> {
    return null; //this.charitiesService.create(createCharityDto, loggedInUserId, response);
  }

  @Get()
  @ApiOperation({ summary: 'Get Transactions' })
  @ApiOkResponse({
    description: 'Organisation',
    type: TransactionDto,
  })
  @ApiImplicitQuery({
    name: 'propName',
    description: 'Property Name',
    required: false,
    type: Number,
  })
  @ApiImplicitQuery({
    name: 'propVal',
    description: 'Property Value',
    required: false,
    type: Number,
  })
  findAll(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ): Promise<Response> {
    if (propName && propVal) {
      const filter = { propName, propVal };
      return this.transactionsService.findAll(response, filter);
    } else {
      return this.transactionsService.findAll(response);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get Transaction' })
  @ApiOkResponse({
    description: 'Transaction',
    type: TransactionDto,
  })
  findOne(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.transactionsService.findOne(id, response);
  }
}
