import { ApiProperty } from '@nestjs/swagger';
import firebase from 'firebase';
import { Beneficiary, Campaign } from '../entities/transaction.entity';
import Timestamp = firebase.firestore.Timestamp;

export class TransactionDto {
  @ApiProperty()
  amount: number;
  @ApiProperty()
  currency: string;
  /*@ApiProperty()
  command: string;*/
  @ApiProperty()
  dateOfTransaction: Timestamp;

  @ApiProperty()
  gateway: string;

  @ApiProperty()
  orderRef: string;

  @ApiProperty()
  transactionId: string;

  @ApiProperty()
  type: string;

  @ApiProperty()
  status: string;

  @ApiProperty()
  senderUid: string;

  @ApiProperty()
  senderEmail: string;

  @ApiProperty()
  beneficiary: Beneficiary = new Beneficiary();

  @ApiProperty({ type: Campaign, default: Campaign })
  campaign: Campaign = new Campaign();

  @ApiProperty()
  organizationId: string;

  @ApiProperty()
  imageUrl: string;

  constructor(transaction: any) {
    this.amount = transaction.amount;
    this.currency = transaction.currency;
    //this.command = transaction.command;
    this.dateOfTransaction = transaction.dateOfTransaction.toDate();
    this.gateway = transaction.gateway;
    this.orderRef = transaction.orderRef;
    this.transactionId = transaction.transactionId;
    this.type = transaction.type;
    this.status = transaction.status;
    this.senderEmail = transaction.senderEmail;
    this.senderUid = transaction.senderUid;
    this.beneficiary = transaction.beneficiary;
    this.campaign = transaction.campaign;
    this.organizationId = transaction.organizationId;
    this.imageUrl = transaction.imageUrl;
  }
}
