import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class CreateTransactionDTO {

  @ApiProperty()
  @IsDefined()
  amount: number;

  @ApiProperty()
  @IsDefined()
  currency: string;

  @ApiProperty()
  @IsDefined()
  command: string;

  @ApiProperty()
  @IsDefined()
  type: string;

  @ApiProperty()
  @IsDefined()
  organizationId: string;

  @ApiProperty()
  @IsDefined()
  profileId: string;

  @ApiProperty()
  @IsDefined()
  campaignId: string;
}
