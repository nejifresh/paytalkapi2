import { HttpStatus, Injectable, NotFoundException, Res } from '@nestjs/common';
import { Response } from 'express';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { Transaction } from './entities/transaction.entity';
import { TransactionDto } from './dto/transaction.dto';
import { CreateTransactionDTO } from './dto/createTransactionDTO';

@Injectable()
export class TransactionsService {
  async create(
    createTransactionDTO: CreateTransactionDTO,
    loggedInUserId: string,
    @Res() response: Response,
  ): Promise<Response> {
    const orgSnapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(createTransactionDTO.organizationId)
      .get();

    if (orgSnapshot.exists) {
      throw new NotFoundException('Organization Id is not valid');
    }

    const userSnapshot = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(loggedInUserId)
      .get();

    const user = userSnapshot.data();

    //const transaction = new Transaction(createTransactionDTO,loggedInUserId,'paystack',);

    return response.status(HttpStatus.CREATED).json(null);
  }

  async findAll(
    @Res() response: Response,
    filter?: { propName: string; propVal: string },
  ): Promise<Response> {
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('transactions2')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('transactions2')
        .get();
    }

    const transactions: Array<Transaction> = [];

    snapshot.forEach(doc => {
      transactions.push(new TransactionDto(doc.data()));
    });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(transactions, HttpStatus.OK));
  }

  async findOne(id: string, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('transactions2')
      .doc(id)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('Transaction Id is not valid');
    }

    const transaction = new TransactionDto(snapshot.data());

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(transaction, HttpStatus.OK));
  }
}
