import { ApiProperty } from '@nestjs/swagger';
import { SubAccountDto } from './sub-account.dto';

export class SubAccountResponseDTO {

  @ApiProperty()
  public status: string;

  @ApiProperty()
  private message: string;

  @ApiProperty({type:SubAccountDto})
  private data: SubAccountDto;


  constructor(status: string, message: string, data: SubAccountDto) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}
