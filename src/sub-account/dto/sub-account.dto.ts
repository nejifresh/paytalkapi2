import { ApiProperty } from '@nestjs/swagger';

export class SubAccountDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  subaccount_id: string;

  @ApiProperty()
  account_number: string;

  @ApiProperty()
  account_bank: string;

  @ApiProperty()
  bank_name: string;

  @ApiProperty()
  full_name: string;

  @ApiProperty()
  split_type: string;

  @ApiProperty()
  split_value: string;

  @ApiProperty()
  created_at: Date;
}
