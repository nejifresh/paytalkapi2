import { ApiProperty } from '@nestjs/swagger';

export class SubAccountErrorResponseDTO {

  @ApiProperty()
  public status: string;

  @ApiProperty()
  private message: string;

  @ApiProperty()
  private data : string;


  constructor(status: string, message: string, data: string) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}
