import { IsDefined } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateSubAccountDto {

  @IsDefined()
  @ApiProperty()
  account_bank: string;

  @IsDefined()
  @ApiProperty()
  account_number: string;

  @IsDefined()
  @ApiProperty()
  business_name: string;

  @IsDefined()
  @ApiProperty()
  business_email: string;

  @IsDefined()
  @ApiProperty()
  business_contact: string;

  @IsDefined()
  @ApiProperty()
  business_mobile: string;

  @IsDefined()
  @ApiProperty()
  country: string;

  @IsDefined()
  @ApiProperty()
  split_type: string;

  @IsDefined()
  @ApiProperty()
  split_value: string;

  @IsDefined()
  @ApiProperty()
  business_contact_mobile: string;


  constructor(account_bank: string, account_number: string, business_name: string, business_email: string, business_contact: string, business_mobile: string, country: string, split_type: string, split_value: string, business_contact_mobile: string) {
    this.account_bank = account_bank;
    this.account_number = account_number;
    this.business_name = business_name;
    this.business_email = business_email;
    this.business_contact = business_contact;
    this.business_mobile = business_mobile;
    this.country = country;
    this.split_type = split_type;
    this.split_value = split_value;
    this.business_contact_mobile = business_contact_mobile;
  }
}
