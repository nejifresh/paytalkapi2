import { BadRequestException, HttpStatus, Injectable, Res } from '@nestjs/common';
import { CreateSubAccountDto } from './dto/create-sub-account.dto';
import { UpdateSubAccountDto } from './dto/update-sub-account.dto';
import { Response } from 'express';
import { Flutterwave } from '../commons/flutterwave.config';
import { SubAccountResponseDTO } from './dto/sub-account.response.dto';

@Injectable()
export class SubAccountService {
 async create(createSubAccountDto: CreateSubAccountDto,
         @Res() response: Response): Promise<Response> {

   const subaccount: SubAccountResponseDTO = await Flutterwave.Subaccount.create(createSubAccountDto);

    if(subaccount.status === 'error'){
      return response
        .status(HttpStatus.BAD_REQUEST)
        .json(
          subaccount,
        );
    }

   return response
     .status(HttpStatus.OK)
     .json(
       subaccount,
     );
 }

  async createSubaccount(createSubAccountDto: CreateSubAccountDto): Promise<SubAccountResponseDTO> {

    const subaccount: SubAccountResponseDTO = await Flutterwave.Subaccount.create(createSubAccountDto);

    if (subaccount.status === 'error') {
      throw new BadRequestException('Error Occurred while creating');
    }

    return subaccount;

  }

  async findAll(@Res() response: Response): Promise<Response> {
    const subaccounts: SubAccountResponseDTO = await Flutterwave.Subaccount.fetch_all();

    return response
      .status(HttpStatus.OK)
      .json(
        subaccounts,
      );
  }

  findOne(id: number) {
    return `This action returns a #${id} subAccount`;
  }

  update(id: number, updateSubAccountDto: UpdateSubAccountDto) {
    return `This action updates a #${id} subAccount`;
  }

  remove(id: number) {
    return `This action removes a #${id} subAccount`;
  }
}
