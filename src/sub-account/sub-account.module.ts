import { Module } from '@nestjs/common';
import { SubAccountService } from './sub-account.service';
import { SubAccountController } from './sub-account.controller';

@Module({
  controllers: [SubAccountController],
  providers: [SubAccountService],
  exports: [SubAccountService],
})
export class SubAccountModule {}
