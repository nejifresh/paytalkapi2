import { Body, Controller, Delete, Get, Param, Post, Put, Res, UseGuards, UsePipes } from '@nestjs/common';
import { SubAccountService } from './sub-account.service';
import { CreateSubAccountDto } from './dto/create-sub-account.dto';
import { UpdateSubAccountDto } from './dto/update-sub-account.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { Response } from 'express';
import { SubAccountResponseDTO } from './dto/sub-account.response.dto';
import { SubAccountErrorResponseDTO } from './dto/sub-account.ErrorResponse.dto';

@Controller('subAccount')
@ApiBearerAuth()
@ApiTags('subAccount')
@UseGuards(AuthGuard)
export class SubAccountController {
  constructor(private readonly subAccountService: SubAccountService) {}

  @Post()
  @ApiOperation({ summary: 'Create Sub Account' })
  @ApiOkResponse({
    description: 'Created Sub Account',
    type: SubAccountResponseDTO,
  })
  @ApiBadRequestResponse({
    description: 'Error',
    type: SubAccountErrorResponseDTO,
  })
  @UsePipes(DTOValidationPipe)
  create(@Body() createSubAccountDto: CreateSubAccountDto,
         @Res() response: Response): Promise<Response> {
    return this.subAccountService.create(createSubAccountDto,response);
  }

  @Get('all')
  findAll(@Res() response: Response): Promise<Response> {
    return this.subAccountService.findAll(response);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.subAccountService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateSubAccountDto: UpdateSubAccountDto) {
    return this.subAccountService.update(+id, updateSubAccountDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.subAccountService.remove(+id);
  }
}
