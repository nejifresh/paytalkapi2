import { BadRequestException, HttpStatus, Injectable, NotFoundException, Res } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Response } from 'express';
import { FulfillOrderDto } from './dto/fulfill-order.dto';
import { adminTypes, firebaseAdmin } from '../commons/firebaseConfig';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';

@Injectable()
export class OrderService {
  fulfillOrder(fulfillOrderDto: FulfillOrderDto,
         @Res() response: Response,):Promise<Response>  {


    return firebaseAdmin.firestore().collection('transactions2')
      .doc(fulfillOrderDto.transactionId)
      .get()
      .then(transactionDoc =>{
          if (!transactionDoc.exists) {
            throw new BadRequestException('Transaction Id is not valid');
          }
          const {dealId,dealReference} = transactionDoc.data();

          if(dealId != fulfillOrderDto.dealId){
            throw new BadRequestException('Deal Id is not valid');
          }

        if(dealReference != fulfillOrderDto.dealReference){
          throw new BadRequestException('Deal Reference is not valid');
        }

        return transactionDoc.ref
          .update({fulfilled:true,
          fulfillmentDate: adminTypes.firestore.Timestamp.now()})

      }).then(()=>{
        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto("Deal Fulfilled", HttpStatus.OK));
      })
  }

  refund(fulfillOrderDto: FulfillOrderDto,
         @Res() response: Response,):Promise<Response>  {

    return firebaseAdmin.firestore().collection('transactions2')
      .doc(fulfillOrderDto.transactionId)
      .get()
      .then(transactionDoc =>{
        if (!transactionDoc.exists) {
          throw new BadRequestException('Transaction Id is not valid');
        }
        const {dealId,dealReference,refunded} = transactionDoc.data();

        if(dealId != fulfillOrderDto.dealId){
          throw new BadRequestException('Deal Id is not valid');
        }

        if(dealReference != fulfillOrderDto.dealReference){
          throw new BadRequestException('Deal Reference is not valid');
        }

        if(refunded){
          throw new BadRequestException('Deal Already Refunded is not valid');
        }

        return transactionDoc.ref
          .update({refunded:true,
            refundDate: adminTypes.firestore.Timestamp.now()})

      }).then(()=>{
        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto("Deal Refund Triggered", HttpStatus.OK));
      })
  }

  calcel(fulfillOrderDto: FulfillOrderDto,
         @Res() response: Response,):Promise<Response>  {

    return firebaseAdmin.firestore().collection('transactions2')
      .doc(fulfillOrderDto.transactionId)
      .get()
      .then(transactionDoc =>{
        if (!transactionDoc.exists) {
          throw new BadRequestException('Transaction Id is not valid');
        }
        const {dealId,dealReference,cancel,fulfilled} = transactionDoc.data();

        if(dealId != fulfillOrderDto.dealId){
          throw new BadRequestException('Deal Id is not valid');
        }

        if(dealReference != fulfillOrderDto.dealReference){
          throw new BadRequestException('Deal Reference is not valid');
        }

        if(fulfilled){
          throw new BadRequestException('Deal Already Fulfilled is not valid');
        }

        if(cancel){
          throw new BadRequestException('Deal Already Refunded is not valid');
        }

        return transactionDoc.ref
          .update({cancel:true,
            cancelDate: adminTypes.firestore.Timestamp.now()})

      }).then(()=>{
        return response
          .status(HttpStatus.OK)
          .json(new SuccessResponseDto("Deal Canceled", HttpStatus.OK));
      })
  }
}
