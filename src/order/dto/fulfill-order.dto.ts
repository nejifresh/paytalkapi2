import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class FulfillOrderDto {

  @ApiProperty()
  @IsDefined()
  transactionId: string

  @ApiProperty()
  @IsDefined()
  dealId: string

  @ApiProperty()
  @IsDefined()
  dealReference: string

}