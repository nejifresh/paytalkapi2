import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, UsePipes, Res } from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { ApiBearerAuth, ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { Charity } from '../organizations/entities/charity.entity';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { Response } from 'express';
import { FulfillOrderDto } from './dto/fulfill-order.dto';

@Controller('order')
@ApiBearerAuth()
@ApiTags('order')
@UseGuards(AuthGuard)
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Post('fulfill')
  @ApiOperation({ summary: 'Fulfill Deal' })
  @UsePipes(DTOValidationPipe)
  fulfill(@Body() fulfillOrderDto:FulfillOrderDto,@Res() response: Response) {
    return this.orderService.fulfillOrder(fulfillOrderDto,response);
  }

  @Post('cancel')
  @ApiOperation({ summary: 'Cancel Deal' })
  @UsePipes(DTOValidationPipe)
  cancel(@Body() fulfillOrderDto:FulfillOrderDto,@Res() response: Response) {
    return this.orderService.calcel(fulfillOrderDto,response);
  }

  @Post('refund')
  @ApiOperation({ summary: 'Refund Deal' })
  @UsePipes(DTOValidationPipe)
  refund(@Body() fulfillOrderDto:FulfillOrderDto,@Res() response: Response) {
    return this.orderService.refund(fulfillOrderDto,response);
  }

}
