import { ApiProperty } from '@nestjs/swagger';
import { CreateCharityDto } from '../dto/create-charity.dto';
import firebase from 'firebase';
import { adminTypes } from '../../commons/firebaseConfig';
import Timestamp = firebase.firestore.Timestamp;
import { IsDefined, IsNotEmpty } from 'class-validator';
import GeoPoint = firebase.firestore.GeoPoint;

export class Charity {
  @ApiProperty()
  category: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  dateRegistered: Timestamp;
  @ApiProperty()
  defaultAdmin: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  paymentGateway: string;
  @ApiProperty()
  about: string;
  @ApiProperty()
  certificateOfIncorporationURL: string;
  @ApiProperty()
  logoURL: string;
  @ApiProperty()
  addedByUserId: string;
  @ApiProperty()
  profileCount: number;
  @ApiProperty()
  totalCampaigns: number;

  splitRatio: number;

  defaultProfile: string;
  active: boolean;

  @ApiProperty()
  currency: string;

  @ApiProperty()
  country: string;

  @ApiProperty()
  location: GeoPoint

  constructor(createCharityDto: CreateCharityDto, addedByUserId: string) {
    this.category = createCharityDto.category;
    this.name = createCharityDto.name;
    this.website = createCharityDto.website;
    this.paymentGateway = createCharityDto.paymentGateway;
    this.about = createCharityDto.about;
    this.addedByUserId = addedByUserId;
    this.dateRegistered = adminTypes.firestore.Timestamp.now();
    this.active = true;
    this.splitRatio = createCharityDto.splitRatio;
    this.profileCount = 0;
    this.totalCampaigns = 0;
    this.country = createCharityDto.country;
    this.currency = createCharityDto.currency;
    this.location = new adminTypes.firestore.GeoPoint(createCharityDto.latitude,createCharityDto.longitude)
  }

}
