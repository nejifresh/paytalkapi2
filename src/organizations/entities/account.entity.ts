import { ApiProperty } from '@nestjs/swagger';
import { CreateCharityDto } from '../dto/create-charity.dto';

export class Account {
  @ApiProperty()
  accountName: string;
  @ApiProperty()
  accountNumber: string;
  @ApiProperty()
  bankName: string;
  @ApiProperty()
  bankCode: string;
  @ApiProperty()
  paystackSubAccount: string;
  @ApiProperty()
  flutterwaveSubAccount: string;


  constructor(createOrganizationsDto: CreateCharityDto) {
    this.accountName = createOrganizationsDto.accountName;
    this.accountNumber = createOrganizationsDto.accountNumber;
    this.bankName = createOrganizationsDto.bankName;
  }
}
