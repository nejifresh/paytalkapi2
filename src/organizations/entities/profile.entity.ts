import { ApiProperty } from '@nestjs/swagger';
import { CreateCharityDto } from '../dto/create-charity.dto';
import { Account } from './account.entity';
import firebase from 'firebase';
import { adminTypes } from '../../commons/firebaseConfig';
import Timestamp = firebase.firestore.Timestamp;
import GeoPoint = firebase.firestore.GeoPoint;

export class Profile {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  country: string;
  @ApiProperty()
  address: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  dateCreated: Timestamp;
  @ApiProperty()
  defaultProfile: boolean;
  @ApiProperty({ type: Account })
  account: Account;
  @ApiProperty()
  paypalEmail: string;
  @ApiProperty()
  stripeConnectedAccount: string;
  @ApiProperty()
  location: GeoPoint


  constructor(
    createOrganizationsDto: CreateCharityDto,
    defaultProfile: boolean,
  ) {
    this.country = createOrganizationsDto.country;
    this.address = createOrganizationsDto.address;
    this.email = createOrganizationsDto.email;
    this.phoneNumber = createOrganizationsDto.phoneNumber;
    this.dateCreated = adminTypes.firestore.Timestamp.now();
    this.defaultProfile = defaultProfile;
    this.name = `${createOrganizationsDto.name} default`;
    this.paypalEmail = createOrganizationsDto.paypalEmail;
    this.location = new adminTypes.firestore.GeoPoint(createOrganizationsDto.latitude,createOrganizationsDto.longitude)
  }
}
