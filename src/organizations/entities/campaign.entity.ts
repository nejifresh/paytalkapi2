import { ApiProperty } from '@nestjs/swagger';

export class Campaign {


  @ApiProperty()
  addedByUserId: string;
  @ApiProperty()
  dateCreated: any;
  @ApiProperty()
  description: string;
  @ApiProperty()
  title: string;
  @ApiProperty()
  targetAmount: number;

  @ApiProperty()
  campaignId: string;

  plans: any;

  @ApiProperty()
  stripeProductId: string;


}
