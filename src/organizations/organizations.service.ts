import { HttpStatus, Injectable, Logger, NotFoundException, Res } from '@nestjs/common';
import { CreateCharityDto } from './dto/create-charity.dto';
import { UpdateCharityDto } from './dto/update-charity.dto';
import { Response } from 'express';
import { adminTypes, firebaseAdmin, logger } from '../commons/firebaseConfig';
import { Charity } from './entities/charity.entity';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { CharityResponseDto } from './dto/CharityResponse.dto';
import { Profile } from './entities/profile.entity';
import { GetCharityResponseDto } from './dto/GetCharityResponse.dto';
import { ProfileDTO } from './dto/profile.dto';
import { Campaign } from './entities/campaign.entity';
import { CampaignDTO } from './dto/campaign.dto';
import { OgranizationUserDTO } from './dto/ogranization.user.dto';
import { OgranizationUser } from '../organizationUsers/entities/organizationUser.entity';
import Paystack from 'paystack';
import { paystack } from '../commons/paystack.config';
import { Flutterwave } from '../commons/flutterwave.config';
import { SendGridService } from '@anchan828/nest-sendgrid';
import { SubAccountService } from '../sub-account/sub-account.service';
import * as base64Img from 'base64-img';
import * as os from 'os';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';
import { ITempOrganization } from '../commons/interfaces/tempOrganization.interface';
import { ApproveOrganizationDto } from './dto/approve-organization.dto';
import { RejectOrganizationDTO } from './dto/reject-organization.dto';
import { TempOrganizationDTO } from './dto/TempOrganizationDTO';
import { GetPendingOrgDTO } from './dto/GetPendingOrgDTO';
import { TempProfileDTO } from './dto/TempProfileDTO';
import { TempProfileInterface } from './dto/interface/tempProfile.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentGatewaysEntity } from '../commons/entities/paymentGateways.entity';
import { OrganisationTypes } from '../commons/entities/OrganisationTypes.entity';
import { AddGatewayDto } from './dto/addGateway.dto';
import { Organization } from '../commons/entities/organization.entity';
import { GetEventByLocationDto } from '../events/dto/getEventByLocation.dto';

@Injectable()
export class OrganizationsService {
  constructor(
    private readonly sendGrid: SendGridService,
    private readonly subAccountService: SubAccountService,
    @InjectRepository(PaymentGatewaysEntity)
    private paymentGatewayRepository: Repository<PaymentGatewaysEntity>,
    @InjectRepository(OrganisationTypes)
    private organisationTypeRepository: Repository<OrganisationTypes>,
    @InjectRepository(Organization)
    private organisationRepository: Repository<Organization>,
  ) {
  }

  create(
    createCharityDto: CreateCharityDto,
    loggedInUserId: string,
    @Res() response: Response,
  ): Promise<Response> {
    let orgDocRef: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>;
    let profileDocRef: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>;
    let imageURL: any;

    const charity: Charity = new Charity(createCharityDto, loggedInUserId);

    const tmpdir = os.tmpdir();

    return firebaseAdmin
      .firestore()
      .collection('organizations')
      .add({ ...charity })
      .then(docRef => {
        orgDocRef = docRef;
        return docRef;
      })
      .then(docRef => {
        const profile: Profile = new Profile(createCharityDto, true);

        if(createCharityDto.accountNumber){
          profile.account = {
            accountName: createCharityDto.accountName,
            accountNumber: createCharityDto.accountNumber,
            bankName: createCharityDto.bankName,
            bankCode: createCharityDto.bankCode.toString(),
            paystackSubAccount: '',
            flutterwaveSubAccount: '',
          };
        }

        return docRef.collection('profiles').add({ ...profile });
      })
      .then(async profileRef => {
        const bucket = firebaseAdmin.storage().bucket();

        const fileUUID = uuidv4();

        const logoPath = base64Img.imgSync(
          `data:${createCharityDto.MIMEType};base64,${createCharityDto.logoBase64String}`,
          path.join(tmpdir),
          `${orgDocRef.id} logo`,
        );

        imageURL = await firebaseAdmin
          .storage()
          .bucket()
          .upload(logoPath, {
            metadata: {
              contentType: `${createCharityDto.MIMEType}`,
              metadata: { firebaseStorageDownloadTokens: fileUUID },
            },
          })
          .then(data => {
            const file = data[0];
            return Promise.resolve(
              'https://firebasestorage.googleapis.com/v0/b/' +
                bucket.name +
                '/o/' +
                encodeURIComponent(file.name) +
                '?alt=media&token=' +
                fileUUID,
            );
          });

        profileDocRef = profileRef;
        const defaultProfileVal = profileRef.id;
        return firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(orgDocRef.id)
          .update({
            defaultProfile: defaultProfileVal,
            imageUrl: imageURL,
          });
      })
      .then(async () => {
        /*const orgAdmin: OgranizationUser = new OgranizationUser(
          createCharityDto.email,
          loggedInUserId,
        );
        orgAdmin.organizations.push({
          organizationId: orgDocRef.id,
          profileId: profileDocRef.id,
        });

        orgAdmin.organizationIds.push(orgDocRef.id);

        return firebaseAdmin
          .firestore()
          .collection('organizationUsers')
          .add({ ...orgAdmin });*/

        const orgUserSnap = await firebaseAdmin
          .firestore()
          .collection('organizationUsers')
          .where('email','==', `${createCharityDto.email}`).get()

        if(orgUserSnap.empty) {

          const user: OgranizationUser = new OgranizationUser(
            createCharityDto.email,
            loggedInUserId,
          );

          user.organizations.push({
            organizationId: orgDocRef.id,
            profileId: profileDocRef.id,
            role: 'ADMIN',
          });

          user.organizationIds.push(orgDocRef.id);

          const userRef = await firebaseAdmin
            .firestore()
            .collection('users')
            .where('email', '==', `${createCharityDto.email}`).get()

          if (userRef.empty) {
            return firebaseAdmin
              .firestore()
              .collection('organizationUsers')
              .add({ ...user });
        }else {

            const appUserDoc = userRef.docs[0];

             await firebaseAdmin
               .firestore()
               .collection('organizationUsers')
               .doc(appUserDoc.id)
               .create({ ...user });

            return firebaseAdmin
              .firestore()
              .collection('organizationUsers')
              .doc(appUserDoc.id);

          }
        }else {
          const orgUserRef = orgUserSnap.docs[0];

          const {organizations, organizationIds} = orgUserRef.data();

          organizations.push({
            organizationId: orgDocRef.id,
            profileId: profileDocRef.id,
          });

          organizationIds.push(orgUserRef.id);

          await orgUserRef.ref.update({organizations,organizationIds})

          return  orgUserRef.ref;
        }
      })
      .then(orgUserDoc => {
        firebaseAdmin
          .firestore()
          .collection('organizations')
          .doc(orgDocRef.id)
          .update({ defaultAdmin: orgUserDoc.id });

        return orgDocRef.get();
      })
      .then(orgDoc => {
        return new CharityResponseDto(orgDoc.id, orgDoc.data());
      })
      .then(createdOrg => {
        return response
          .status(HttpStatus.CREATED)
          .json(new SuccessResponseDto(createdOrg, HttpStatus.CREATED));
      })
      .catch(error => {
        logger.error(error)
        Logger.error(`An Error Occurred`, error, 'Organizations Service');
        return response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json(
            new SuccessResponseDto(
              `Error Creating Organization ${error.message}`,
              HttpStatus.INTERNAL_SERVER_ERROR,
            ),
          );
      });
  }

  async findAll(
    @Res() response: Response,
    filter?: { propName: string; propVal: string },
  ): Promise<Response> {
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('organizations')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('organizations')
        .get();
    }

    const charities: Array<CharityResponseDto> = [];

    snapshot.forEach(doc => {
      charities.push(new CharityResponseDto(doc.id, doc.data()));
    });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(charities, HttpStatus.OK));
  }

  async findAllPaymentGateways(
    @Res() response: Response,
  ): Promise<Response> {
    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(await this.paymentGatewayRepository.find(), HttpStatus.OK));
  }

  async findAllOrganisationTypes(
    @Res() response: Response,
  ): Promise<Response> {
    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(await this.organisationTypeRepository.find(), HttpStatus.OK));
  }

  async findAllPending(
    @Res() response: Response,
    filter?: { propName: string; propVal: string },
  ): Promise<Response> {
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('temporaryOrganizations')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('temporaryOrganizations')
        .get();
    }

    const charities: Array<TempOrganizationDTO> = [];

    snapshot.forEach(doc => {
      charities.push(new TempOrganizationDTO(doc.data() as ITempOrganization));
    });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(charities, HttpStatus.OK));
  }

  async findOne(id: string, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(id)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('Organization Id is not valid');
    }

    const profilesSnapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(id)
      .collection('profiles')
      .get();

    const campaignsSnapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(id)
      .collection('campaigns')
      .get();

    const usersSnapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(id)
      .collection('users')
      .get();

    const profiles: Array<Profile> = [];
    const campaigns: Array<Campaign> = [];
    const users: Array<OgranizationUser> = [];

    profilesSnapshot.forEach(doc =>
      profiles.push(new ProfileDTO(doc.id, doc.data())),
    );

    campaignsSnapshot.forEach(doc =>
      campaigns.push(new CampaignDTO(doc.id, doc.data())),
    );

    usersSnapshot.forEach(doc =>
      users.push(new OgranizationUserDTO(doc.id, doc.data())),
    );

    const charity = new GetCharityResponseDto(snapshot.id, snapshot.data());
    charity.profiles = profiles;
    charity.campaigns = campaigns;
    charity.users = users;

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(charity, HttpStatus.OK));
  }


  async approve(approveDTO: ApproveOrganizationDto, @Res() response: Response,
                loggedInUserId: string): Promise<Response> {
    let snapshot = await firebaseAdmin
      .firestore()
      .collection('temporaryOrganizations')
      .doc(approveDTO.organizationId)
      .get();


    if (!snapshot.exists) {
      throw new NotFoundException('Organization Id is not valid');
    }

    const profileData = await firebaseAdmin
      .firestore()
      .collection('temporaryOrganizations')
      .doc(approveDTO.organizationId)
      .collection('profiles')
      .get();

    const profile = await profileData.docs[0];


    await firebaseAdmin
      .firestore()
      .collection('temporaryOrganizations')
      .doc(approveDTO.organizationId)
      .update({
        active: true,
        serviceCharge: profile.data().serviceCharge,
        splitRatio: 5,
      });

    await firebaseAdmin
      .firestore()
      .collection('temporaryOrganizations')
      .doc(approveDTO.organizationId)
      .update({
        status: 'APPROVED',
      });

    snapshot = await snapshot.ref.get();

    await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(approveDTO.organizationId)
      .create(snapshot.data());


    const profileInfo = profile.data() as TempProfileInterface;

    profileInfo.name = `${profileInfo.name} Admin`;

    await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(approveDTO.organizationId)
      .collection('profiles')
      .doc(profile.id)
      .create(profileInfo);

    profileInfo.name = profile.data().name;
    profileInfo.defaultProfile = false;

    const duplicateProfile = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(approveDTO.organizationId)
      .collection('profiles')
      .add(profileInfo);

    const { defaultAdmin } = snapshot.data();

    const userAuth = await firebaseAdmin.auth().getUser(defaultAdmin);

    const orgUserSnap = await firebaseAdmin
      .firestore()
      .collection('organizationUsers')
      .where('email', '==', `${userAuth.email}`).get();


    if (orgUserSnap.empty) {

      const user: OgranizationUser = new OgranizationUser(
        userAuth.email,
        loggedInUserId,
      );

      user.organizations.push({
        organizationId: approveDTO.organizationId,
        profileId: profile.id,
        role: 'ADMIN',
      });

      user.organizations.push({
        organizationId: approveDTO.organizationId,
        profileId: duplicateProfile.id,
        role: 'ADMIN',
      });

      user.organizationIds.push(approveDTO.organizationId);


      await firebaseAdmin
        .firestore()
        .collection('organizationUsers')
        .doc(userAuth.uid)
        .create({ ...user });

    } else {
      const orgUserRef = orgUserSnap.docs[0];

      const { organizations, organizationIds } = orgUserRef.data();

      organizations.push({
        organizationId: approveDTO.organizationId,
        profileId: profile.id,
      });

      organizations.push({
        organizationId: approveDTO.organizationId,
        profileId: duplicateProfile.id,
      });

      organizationIds.push(orgUserRef.id);

      await orgUserRef.ref.update({ organizations, organizationIds });
    }

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto('Organization Approved', HttpStatus.OK));
  }

  async reject(rejectDTO: RejectOrganizationDTO, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(rejectDTO.organizationId)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('Organization Id is not valid');
    }

    await snapshot.ref.update({ status: 'REJECTED', rejectionMessage: rejectDTO.rejectionMessage });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto('Organization Rejected', HttpStatus.OK));
  }

  async findOnePending(id: string, @Res() response: Response): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('temporaryOrganizations')
      .doc(id)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('Organization Id is not valid');
    }

    const profilesSnapshot = await firebaseAdmin
      .firestore()
      .collection('temporaryOrganizations')
      .doc(id)
      .collection('profiles')
      .get();


    const profiles: Array<TempProfileDTO> = [];

    profilesSnapshot.forEach(doc =>
      profiles.push(new TempProfileDTO(doc.data() as TempProfileInterface)),
    );


    const charity = new GetPendingOrgDTO(snapshot.data() as ITempOrganization);
    charity.profiles = profiles;

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(charity, HttpStatus.OK));
  }

  async update(
    id: string,
    updateCharityDto: UpdateCharityDto,
    @Res() response: Response,
  ): Promise<Response> {
    await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(id)
      .update(updateCharityDto);

    return this.findOne(id, response);
  }

  async remove(id: string, @Res() response: Response): Promise<Response> {
    await firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(id)
      .delete();

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto('Charity Deleted', HttpStatus.OK));
  }

  createSubAccount(
    name: string,
    bankCode: number,
    accountNumber: string,
    split: number,
  ): Promise<Paystack.Response> {
    return paystack.subaccount.create({
      account_number: accountNumber,
      business_name: name,
      percentage_charge: split,
      settlement_bank: bankCode.toString(),
    });
  }

  createPaymentPlans(organizationId: string): void {
    const planDocRef = firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(organizationId)
      .collection('plans');

    let orgName: string;

    firebaseAdmin
      .firestore()
      .collection('organizations')
      .doc(organizationId)
      .get()
      .then(async orgRef => {
        const { name } = orgRef.data();

        orgName = name;

        const payload = {
          name: `${name} Monthly`,
          interval: 'monthly',
        };

        const response = await Flutterwave.PaymentPlan.create(payload);

        const { data } = response;

        return planDocRef.add(data);
      })
      .then(async () => {
        const payload = {
          name: `${orgName} yearly`,
          interval: 'yearly',
        };

        const response = await Flutterwave.PaymentPlan.create(payload);

        const { data } = response;

        return planDocRef.add(data);
      })
      .then(async () => {
        const payload = {
          name: `${orgName} weekly`,
          interval: 'weekly',
        };
        const response = await Flutterwave.PaymentPlan.create(payload);

        const { data } = response;

        return planDocRef.add(data);
      })
      .catch(error => logger.error(error));
  }

  async addPaymentGateway(gatewayDto: AddGatewayDto, @Res() response: Response): Promise<Response> {

    const paymentGateway = await new PaymentGatewaysEntity(gatewayDto.name).save();

    await firebaseAdmin.firestore().collection('paymentGateways')
      .doc(paymentGateway.PaymentGatewayID)
      .create({
        name: paymentGateway.Name,
        dateAdded: adminTypes.firestore.Timestamp.now(),
      });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(paymentGateway, HttpStatus.OK));
  }

  async updatePaymentGateway(id: string, gatewayDto: AddGatewayDto, @Res() response: Response): Promise<Response> {

    const paymentGateway = await this.paymentGatewayRepository.findOneOrFail(id);

    paymentGateway.Name = gatewayDto.name;

    await paymentGateway.save();

    await firebaseAdmin.firestore().collection('paymentGateways')
      .doc(paymentGateway.PaymentGatewayID)
      .update({
        name: paymentGateway.Name,
      });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(paymentGateway, HttpStatus.OK));
  }

  async deletePaymentGateway(id: string, @Res() response: Response): Promise<Response> {

    const paymentGateway = await this.paymentGatewayRepository.findOneOrFail(id);

    await firebaseAdmin.firestore().collection('paymentGateways')
      .doc(paymentGateway.PaymentGatewayID)
      .delete();

    paymentGateway.remove();

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(`Payment Gateway ${paymentGateway.Name} Deleted`, HttpStatus.OK));
  }

  async addOrganisationType(gatewayDto: AddGatewayDto, @Res() response: Response): Promise<Response> {

    const organisationType = await new OrganisationTypes(gatewayDto.name).save();

    await firebaseAdmin.firestore().collection('organizationTypes')
      .doc(organisationType.OrganizationTypeID)
      .create({
        name: organisationType.Name,
        dateAdded: adminTypes.firestore.Timestamp.now(),
      });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(organisationType, HttpStatus.OK));
  }

  async updateOrganisationType(id: string, dto: AddGatewayDto, @Res() response: Response): Promise<Response> {

    const organisationType = await this.organisationTypeRepository.findOneOrFail(id);

    organisationType.Name = dto.name;

    await organisationType.save();

    await firebaseAdmin.firestore().collection('organizationTypes')
      .doc(organisationType.OrganizationTypeID)
      .update({
        name: organisationType.Name,
      });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(organisationType, HttpStatus.OK));
  }

  async deleteOrganisationType(id: string, @Res() response: Response): Promise<Response> {

    const organisationType = await this.organisationTypeRepository.findOneOrFail(id);

    await firebaseAdmin.firestore().collection('organizationTypes')
      .doc(organisationType.OrganizationTypeID)
      .delete();

    organisationType.remove();

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(`Organization Type ${organisationType.Name} Deleted`, HttpStatus.OK));
  }

  async findAllByLocation(payload: GetEventByLocationDto, @Res() response: Response) {
    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          await this.organisationRepository.manager.query(
            `RandomOrganizationsByLocation @0,@1`,
            [payload.latitude, payload.longitude],
          ),
          HttpStatus.OK,
        ),
      );
  }

  async getTotal(@Res() response: Response) {

    const total = await this.organisationRepository.count();
    const totalActive = await this.organisationRepository.count({ where: { Status: true } });
    const totalInactive = await this.organisationRepository.count({ where: { Status: false } });

    return response
      .status(HttpStatus.OK)
      .json(
        new SuccessResponseDto(
          { total, totalActive, totalInactive },
          HttpStatus.OK,
        ),
      );
  }
}
