import { Module } from '@nestjs/common';
import { OrganizationsService } from './organizations.service';
import { OrganizationsController } from './organizations.controller';
import { SubAccountModule } from '../sub-account/sub-account.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrganisationTypes } from '../commons/entities/OrganisationTypes.entity';
import { PaymentGatewaysEntity } from '../commons/entities/paymentGateways.entity';
import { Organization } from '../commons/entities/organization.entity';

@Module({
  controllers: [OrganizationsController],
  providers: [OrganizationsService],
  imports: [SubAccountModule, TypeOrmModule.forFeature([
    OrganisationTypes,
    PaymentGatewaysEntity,
    Organization,
  ])],
})
export class OrganizationsModule {
}
