import { Body, Controller, Delete, Get, Param, Post, Put, Query, Res, UseGuards, UsePipes } from '@nestjs/common';
import { OrganizationsService } from './organizations.service';
import { CreateCharityDto } from './dto/create-charity.dto';
import { UpdateCharityDto } from './dto/update-charity.dto';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '../auth/auth-guard.service';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import { Charity } from './entities/charity.entity';
import { Response } from 'express';
import { GetUid } from '../auth/decorators/getAuthenticatedUser.decorator';
import { CharitiesDto } from './dto/getCharities.dto';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import { ApproveOrganizationDto } from './dto/approve-organization.dto';
import { RejectOrganizationDTO } from './dto/reject-organization.dto';
import { TempOrganizationDTO } from './dto/TempOrganizationDTO';
import { GetPendingOrgDTO } from './dto/GetPendingOrgDTO';
import { AddGatewayDto } from './dto/addGateway.dto';
import { GetEventByLocationDto } from '../events/dto/getEventByLocation.dto';

@Controller('charities')
@ApiBearerAuth()
@ApiTags('Organisations')
export class OrganizationsController {
  constructor(private readonly charitiesService: OrganizationsService) {
  }

  @Post()
  @ApiOperation({ summary: 'Create Organisation' })
  @UseGuards(AuthGuard)
  @ApiCreatedResponse({
    description: 'Organisation',
    type: Charity,
  })
  @UsePipes(DTOValidationPipe)
  create(
    @Body() createCharityDto: CreateCharityDto,
    @GetUid() loggedInUserId: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.create(
      createCharityDto,
      loggedInUserId,
      response,
    );
  }

  @ApiOperation({ summary: 'Get Organisations' })
  @ApiOkResponse({
    description: 'Charity Organisations',
    type: CharitiesDto,
  })
  @UseGuards(AuthGuard)
  @Get()
  findAll(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ): Promise<Response> {
    if (propName && propName.length > 0) {
      const filter = {
        propName,
        propVal,
      };

      return this.charitiesService.findAll(response, filter);
    } else {
      return this.charitiesService.findAll(response);
    }
  }


  @ApiOperation({ summary: 'Get Pending Organisations' })
  @ApiOkResponse({
    description: 'Pending Organisations',
    type: TempOrganizationDTO,
  })
  @ApiImplicitQuery({
    name: 'propName',
    description: 'Property Name',
    required: false,
    type: String,
  })
  @ApiImplicitQuery({
    name: 'propVal',
    description: 'Property Value',
    required: false,
    type: String,
  })
  @UseGuards(AuthGuard)
  @Get('pending')
  findAllPending(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ): Promise<Response> {
    if (propName && propName.length > 0) {
      const filter = {
        propName,
        propVal,
      };

      return this.charitiesService.findAllPending(response, filter);
    } else {
      return this.charitiesService.findAllPending(response);
    }
  }

  @ApiOperation({ summary: 'Get Organisation' })
  @ApiOkResponse({
    description: 'Organisation',
    type: GetPendingOrgDTO,
  })
  @Get(':id')
  findOne(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.findOne(id, response);
  }

  @ApiOperation({ summary: 'Approve Organisation' })
  @UseGuards(AuthGuard)
  @Post('approve')
  approve(
    @Body() approveDto: ApproveOrganizationDto,
    @Res() response: Response,
    @GetUid() loggedInUserId: string,
  ): Promise<Response> {
    return this.charitiesService.approve(approveDto, response, loggedInUserId);
  }

  @ApiOperation({ summary: 'Reject Organisation' })
  @UseGuards(AuthGuard)
  @Post('reject')
  reject(
    @Body() rejectOrganization: RejectOrganizationDTO,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.reject(rejectOrganization, response);
  }


  @ApiOperation({ summary: 'Get Pending Organisation' })
  @ApiOkResponse({
    description: 'Pending Organisations',
    type: GetPendingOrgDTO,
  })
  @UseGuards(AuthGuard)
  @Get('pending/:id')
  findOnePending(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.findOnePending(id, response);
  }

  @ApiOperation({ summary: 'Update Organisation' })
  @ApiOkResponse({
    description: 'Organisation',
    type: Charity,
  })
  @UseGuards(AuthGuard)
  @UsePipes(DTOValidationPipe)
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateCharityDto: UpdateCharityDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.update(id, updateCharityDto, response);
  }

  @ApiOperation({ summary: 'Delete Organisation' })
  @ApiOkResponse({
    description: 'Delete Organisation',
    type: String,
  })
  @UseGuards(AuthGuard)
  @Delete(':id')
  remove(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.remove(id, response);
  }

  @ApiOperation({ summary: 'Add Organization Type' })
  @UseGuards(AuthGuard)
  @Post('organization-types')
  addOrganizationTypes(
    @Body() dto: AddGatewayDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.addOrganisationType(dto, response);
  }

  @ApiOperation({ summary: 'Update Organization Type' })
  @UseGuards(AuthGuard)
  @Put('organization-types/:id')
  updateOrganizationTypes(
    @Param('id') id: string,
    @Body() dto: AddGatewayDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.updateOrganisationType(id, dto, response);
  }

  @ApiOperation({ summary: 'Delete Organization Type' })
  @UseGuards(AuthGuard)
  @Delete('organization-types/:id')
  deleteOrganizationTypes(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.deleteOrganisationType(id, response);
  }


  @ApiOperation({ summary: 'Get all Organizations Types' })
  @Get('/organization-types/all')
  findAllOrganizationTypes(
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.findAllOrganisationTypes(response);
  }


  @ApiOperation({ summary: 'Add Payment Gateway' })
  @UseGuards(AuthGuard)
  @Post('payment-geteway')
  addPaymentGateway(
    @Body() addGatewayDto: AddGatewayDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.addPaymentGateway(addGatewayDto, response);
  }

  @ApiOperation({ summary: 'Update Payment Gateway' })
  @UseGuards(AuthGuard)
  @Put('payment-geteway/:id')
  updatePaymentGateway(
    @Param('id') id: string,
    @Body() addGatewayDto: AddGatewayDto,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.updatePaymentGateway(id, addGatewayDto, response);
  }

  @ApiOperation({ summary: 'Delete Payment Gateway' })
  @UseGuards(AuthGuard)
  @Delete('payment-geteway/:id')
  deletePaymentGateway(
    @Param('id') id: string,
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.deletePaymentGateway(id, response);
  }


  @ApiOperation({ summary: 'Get all Payment Gateway' })
  @Get('/payment-gateways/all')
  findAllPaymentGateways(
    @Res() response: Response,
  ): Promise<Response> {
    return this.charitiesService.findAllPaymentGateways(response);
  }

  @Post('random')
  findAllByLocation(@Body() payload: GetEventByLocationDto, @Res() response: Response) {
    return this.charitiesService.findAllByLocation(payload, response);
  }

  @Get('get/total')
  getTotal(@Res() response: Response) {
    return this.charitiesService.getTotal(response);
  }
}
