import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { Helpers } from '../../utils/helpers';
import { validate as validater } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class UniqueEmailValidationPipe implements PipeTransform {

  async transform(value: any, { metatype }: ArgumentMetadata): Promise<any> {
    // CHECKS FOR REQUIRED FIELDS
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);

    const errors = await validater(object);
    if (errors.length > 0)
      throw new BadRequestException(Object.values(errors[0].constraints)[0]);

    // PERFORM ASYNCRHONOUS VALIDATION
    const { email } = Helpers.recursiveTrim(value);


    // return trimmed value
    return Helpers.recursiveTrim(value);
  }

  // ES6 IS COMPLAINING BUT NA SO I SEE AM FOR DOCS

  // eslint-disable-next-line @typescript-eslint/ban-types
  private toValidate(metatype: Function): boolean {
    // eslint-disable-next-line @typescript-eslint/ban-types
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }

}
