import { ApiProperty } from '@nestjs/swagger';

export class CharityResponseDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  category: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  dateRegistered: Date;

  @ApiProperty()
  userId: string;

  @ApiProperty()
  website: string;

  @ApiProperty()
  paymentGateway: string;

  @ApiProperty()
  about: string;

  @ApiProperty()
  certificateOfIncorporationURL: string;

  @ApiProperty()
  imageUrl: string;

  @ApiProperty()
  addedByUserId: string;

  profileCount: number;

  @ApiProperty()
  totalCampaigns: number;


  constructor(id: string, charity: any) {
    this.id = id;
    this.category = charity.category;
    this.name = charity.name;
    this.dateRegistered = charity.dateRegistered.toDate();
    this.userId = charity.userId;
    this.website = charity.website;
    this.paymentGateway = charity.paymentGateway;
    this.about = charity.about;
    this.certificateOfIncorporationURL = charity.certificateOfIncorporationURL;
    this.imageUrl = charity.imageUrl;
    this.addedByUserId = charity.addedByUserId;
  }
}
