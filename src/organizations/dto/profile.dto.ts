import { ApiProperty } from '@nestjs/swagger';
import firebase from 'firebase';
import GeoPoint = firebase.firestore.GeoPoint;

export class ProfileDTO {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  country: string;
  @ApiProperty()
  address: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  dateCreated: any;
  @ApiProperty()
  defaultProfile: boolean;
  @ApiProperty()
  account: any;
  @ApiProperty()
  paypalEmail: string;
  @ApiProperty()
  stripeConnectedAccount: string;
  @ApiProperty()
  location: GeoPoint

  constructor(id: string, profile: any) {
    this.id = id;
    this.country = profile.country;
    this.address = profile.address;
    this.email = profile.email;
    this.phoneNumber = profile.phoneNumber;
    this.dateCreated = profile.dateCreated.toDate();
    this.defaultProfile = profile.defaultProfile;
    this.account = profile.account;
    this.name = profile.name;
    this.paypalEmail = profile.paypalEmail;
    this.stripeConnectedAccount = profile.stripeConnectedAccount;
    this.location = profile.location;
  }
}
