import { ApiProperty } from '@nestjs/swagger';

export class OgranizationUserDTO {
  @ApiProperty()
  id: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  uid: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  role: string;

  @ApiProperty()
  dateAdded: Date;

  @ApiProperty()
  addedByUserId: string;

  @ApiProperty({ type: [String] })
  organizations: Array<{
    organizationId: string;
    profileId: string;
    role: string;
  }>;

  @ApiProperty({ type: [String] })
  organizationIds: Array<string> = [];

  accountSetupCompleted: boolean;
  passwordChanged: boolean;

  constructor(id: string, organizationUser: any) {
    this.id = id;
    this.firstName = organizationUser.firstName;
    this.lastName = organizationUser.lastName;
    this.email = organizationUser.email;
    this.role = organizationUser.role;
    this.dateAdded = organizationUser.dateAdded.toDate();
    this.addedByUserId = organizationUser.addedByUserId;
    this.uid = organizationUser.uid;
  }
}
