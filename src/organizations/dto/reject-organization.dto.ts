import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class RejectOrganizationDTO {
  @ApiProperty()
  @IsDefined()
  organizationId: string;

  @ApiProperty()
  @IsDefined()
  rejectionMessage: string;
}
