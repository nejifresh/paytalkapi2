import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class Logo {
  @ApiProperty()
  @IsDefined()
  logoBase64String: string;
  @ApiProperty()
  @IsDefined()
  MIMEType: string;

}
