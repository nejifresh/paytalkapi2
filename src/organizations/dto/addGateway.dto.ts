import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class AddGatewayDto {

  @ApiProperty()
  @IsDefined()
  name: string;
}
