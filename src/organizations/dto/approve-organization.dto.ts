import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class ApproveOrganizationDto {
  @ApiProperty()
  @IsDefined()
  organizationId: string;
}
