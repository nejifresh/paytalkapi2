import { Charity } from '../entities/charity.entity';
import { ApiProperty } from '@nestjs/swagger';
import { CharityResponseDto } from './CharityResponse.dto';

export class CharitiesDto {
  @ApiProperty({ type: [CharityResponseDto] })
  charities: Array<Charity>;

}
