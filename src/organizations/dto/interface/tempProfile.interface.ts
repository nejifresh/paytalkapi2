import { firestore } from 'firebase-admin';
import GeoPoint = firestore.GeoPoint;
import Timestamp = firestore.Timestamp;

export interface TempProfileInterface {

  zipCode: string;
  defaultProfile: boolean;
  address: string;
  email: string;
  id: string;
  location: GeoPoint;
  dateCreated: Timestamp;
  paypalEmail: string;
  totalCampaigns: number;
  name: string;
  taxPercent: number;
  phoneNumber: string;
  minimumDuration: number;
  serviceCharge: number;
  totalCapacity: number;
  countryCode: string;

}
