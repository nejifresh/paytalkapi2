import { ApiProperty } from '@nestjs/swagger';
import { ITempOrganization } from '../../commons/interfaces/tempOrganization.interface';

export class TempOrganizationDTO {

  @ApiProperty()
  name: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  category: string;
  @ApiProperty()
  currency: string;
  @ApiProperty()
  dateRegistered: Date;
  @ApiProperty()
  about: string;
  @ApiProperty()
  paymentGateway: string;
  @ApiProperty()
  defaultAdmin: string;
  @ApiProperty()
  type: string;
  @ApiProperty()
  splitRatio: number;
  @ApiProperty()
  imageUrl: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  rejectionMessage: string;
  @ApiProperty()
  organizationId: string;


  constructor(organization: ITempOrganization) {
    this.name = organization.name;
    this.website = organization.website;
    this.category = organization.category;
    this.currency = organization.currency;
    this.dateRegistered = organization.dateRegistered.toDate();
    this.about = organization.about;
    this.paymentGateway = organization.paymentGateway;
    this.defaultAdmin = organization.defaultAdmin;
    this.type = organization.type;
    this.splitRatio = organization.splitRatio;
    this.imageUrl = organization.imageUrl;
    this.status = organization.status;
    this.rejectionMessage = organization.rejectionMessage;
    this.organizationId = organization.organizationId;
  }
}
