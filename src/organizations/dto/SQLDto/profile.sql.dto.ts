import { ApiProperty } from '@nestjs/swagger';
import firebase from 'firebase';
import GeoPoint = firebase.firestore.GeoPoint;
import { Profile } from '../../../commons/entities/profiles.entity';

export class ProfileSQLDTO {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  country: string;
  @ApiProperty()
  address: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  dateCreated: any;
  @ApiProperty()
  defaultProfile: boolean;
  @ApiProperty()
  account: any;
  @ApiProperty()
  paypalEmail: string;
  @ApiProperty()
  stripeConnectedAccount: string;
  @ApiProperty()
  location: GeoPoint

  constructor(id: string, profile:Profile) {
    this.id = id;
    //this.country = profile.Country;
    this.address = profile.Address;
    this.email = profile.EmailAddress;
    this.phoneNumber = profile.PhoneNumber;
    this.dateCreated = profile.DateAdded;
    this.defaultProfile = profile.IsDefault;
    //this.account = profile.account;
    this.name = profile.Name;
    this.paypalEmail = profile.PaypalEmailAddress;
    //this.stripeConnectedAccount = profile.stripeConnectedAccount;
    this.location = new GeoPoint(profile.Latitude,profile.Longitude);
  }
}
