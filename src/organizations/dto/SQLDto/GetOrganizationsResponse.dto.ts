import { ApiProperty } from '@nestjs/swagger';
import { ProfileSQLDTO } from './profile.sql.dto';
import { Organization } from '../../../commons/entities/organization.entity';

export class GetOrganizationsResponseDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  category: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  dateRegistered: Date;
  @ApiProperty()
  userId: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  paymentGateway: string;
  @ApiProperty()
  about: string;
  @ApiProperty()
  certificateOfIncorporationURL: string;
  @ApiProperty()
  imageUrl: string;
  @ApiProperty()
  addedByUserId: string;
  @ApiProperty()
  splitRatio: number;

  @ApiProperty({ type: [ProfileSQLDTO] })
  profiles: Array<ProfileSQLDTO>;

 /* @ApiProperty({ type: [CampaignDTO] })
  campaigns: Array<Campaign>;*/


  constructor(id: string, charity: Organization) {
    this.id = id;
    //this.category = charity.category;
    this.name = charity.Name;
    this.dateRegistered = charity.DateAdded;
   //this.userId = charity.;
    this.website = charity.Website;
    this.paymentGateway = charity.PaymentGateWayID;
    this.about = charity.About;
    //this.certificateOfIncorporationURL = charity;
    //this.imageUrl = charity.;
    //this.addedByUserId = charity.addedByUserId;
    this.splitRatio = charity.SplitRatio;
  }
}
