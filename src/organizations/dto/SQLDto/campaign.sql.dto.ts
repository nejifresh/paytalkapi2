import { ApiProperty } from '@nestjs/swagger';

export class CampaignSQLDTO {

  @ApiProperty()
  id: string;

  @ApiProperty()
  addedByUserId: string;

  @ApiProperty()
  dateCreated: Date;

  @ApiProperty()
  description: string;

  @ApiProperty()
  title: string;

  @ApiProperty()
  targetAmount: number;

  @ApiProperty()
  campaignId: string;

  @ApiProperty()
  profileId: string;

  @ApiProperty()
  plans: any;

  @ApiProperty()
  stripeProductId: string;


  constructor(id: string, campaign: any) {
    this.id = id;
    this.addedByUserId = campaign.addedByUserId;
    this.dateCreated = campaign.dateCreated.toDate();
    this.description = campaign.description;
    this.title = campaign.title;
    this.targetAmount = campaign.targetAmount;
    this.campaignId = campaign.campaignId;
    this.profileId = campaign.profileId;
    this.plans = campaign.plans;
    this.stripeProductId = campaign.stripeProductId;
  }
}
