import { ApiProperty } from '@nestjs/swagger';
import { firestore } from 'firebase-admin';
import { TempProfileInterface } from './interface/tempProfile.interface';
import GeoPoint = firestore.GeoPoint;

export class TempProfileDTO {

  @ApiProperty()
  zipCode: string;
  @ApiProperty()
  defaultProfile: boolean;
  @ApiProperty()
  address: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  id: string;
  @ApiProperty()
  location: GeoPoint;
  @ApiProperty()
  dateCreated: Date;
  @ApiProperty()
  paypalEmail: string;
  @ApiProperty()
  totalCampaigns: number;
  @ApiProperty()
  name: string;
  @ApiProperty()
  taxPercent: number;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  minimumDuration: number;
  @ApiProperty()
  serviceCharge: number;
  @ApiProperty()
  totalCapacity: number;
  @ApiProperty()
  countryCode: string;


  constructor(profile: TempProfileInterface) {
    this.zipCode = profile.zipCode;
    this.defaultProfile = profile.defaultProfile;
    this.address = profile.address;
    this.email = profile.email;
    this.id = profile.id;
    this.location = profile.location;
    this.dateCreated = profile.dateCreated.toDate();
    this.paypalEmail = profile.paypalEmail;
    this.totalCampaigns = profile.totalCampaigns;
    this.name = profile.name;
    this.taxPercent = profile.taxPercent;
    this.phoneNumber = profile.phoneNumber;
    this.minimumDuration = profile.minimumDuration;
    this.serviceCharge = profile.serviceCharge;
    this.totalCapacity = profile.totalCapacity;
    this.countryCode = profile.countryCode;
  }
}
