import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsNotEmpty, IsOptional } from 'class-validator';

export class CreateCharityDto {

  @ApiProperty()
  accountName?: string;

  @ApiProperty()
  accountNumber?: string;

  @ApiProperty()
  bankName?: string;

  @ApiProperty()
  bankCode?: number;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  splitRatio: number;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  category: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  currency: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  country: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  name: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  website: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  paymentGateway: string;

  @IsNotEmpty()
  @IsDefined()
  @IsEmail()
  @ApiProperty()
  email: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  phoneNumber: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  about: string;

  @IsDefined()
  @ApiProperty()
  @IsNotEmpty()
  address: string;

  @ApiProperty()
  @IsOptional()
  @IsEmail()
  paypalEmail: string;

  @ApiProperty()
  @IsDefined()
  logoBase64String: string;
  
  @ApiProperty()
  @IsDefined()
  MIMEType: string;

  @ApiProperty()
  @IsOptional()
  @IsDefined()
  longitude: number

  @ApiProperty()
  @IsOptional()
  @IsDefined()
  latitude: number


  /*@ApiProperty({ type: Logo })
  @IsDefined()
  logo: Logo;*/

}
