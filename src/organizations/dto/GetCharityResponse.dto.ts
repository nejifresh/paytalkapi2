import { ApiProperty } from '@nestjs/swagger';
import { Profile } from '../entities/profile.entity';
import { Campaign } from '../entities/campaign.entity';
import { ProfileDTO } from './profile.dto';
import { CampaignDTO } from './campaign.dto';
import { OgranizationUserDTO } from './ogranization.user.dto';
import { OgranizationUser } from '../../organizationUsers/entities/organizationUser.entity';

export class GetCharityResponseDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  category: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  dateRegistered: Date;
  @ApiProperty()
  userId: string;
  @ApiProperty()
  website: string;
  @ApiProperty()
  paymentGateway: string;
  @ApiProperty()
  about: string;
  @ApiProperty()
  certificateOfIncorporationURL: string;
  @ApiProperty()
  imageUrl: string;
  @ApiProperty()
  addedByUserId: string;
  @ApiProperty()
  splitRatio: string;

  @ApiProperty({ type: [ProfileDTO] })
  profiles: Array<Profile>;
  @ApiProperty({ type: [CampaignDTO] })
  campaigns: Array<Campaign>;
  @ApiProperty({ type: [OgranizationUserDTO] })
  users: Array<OgranizationUser>;


  constructor(id: string, charity: any) {
    this.id = id;
    this.category = charity.category;
    this.name = charity.name;
    this.dateRegistered = charity.dateRegistered.toDate();
    this.userId = charity.userId;
    this.website = charity.website;
    this.paymentGateway = charity.paymentGateway;
    this.about = charity.about;
    this.certificateOfIncorporationURL = charity.certificateOfIncorporationURL;
    this.imageUrl = charity.imageUrl;
    this.addedByUserId = charity.addedByUserId;
    this.splitRatio = charity.splitRatio;
  }
}
