import { PartialType } from '@nestjs/mapped-types';
import { CreateCharityDto } from './create-charity.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNotEmpty } from 'class-validator';

export class UpdateCharityDto extends PartialType(CreateCharityDto) {

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  category: string;


  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  name: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  website: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  paymentGateway: string;

  @IsNotEmpty()
  @IsDefined()
  @ApiProperty()
  about: string;

}
