import { Injectable, HttpStatus, Res } from '@nestjs/common';
import { CreateDealDto } from './dto/create-deal.dto';
import { UpdateDealDto } from './dto/update-deal.dto';
import { Response } from 'express';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { Deal } from './entities/deal.entity';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { DealCategory } from './interface/deal-category.interface';

@Injectable()
export class DealsService {
  create(createDealDto: CreateDealDto) {
    return 'This action adds a new deal';
  }

  async findAll(
    @Res() response: Response,
    filter?: { propName: string; propVal: string },
  ) {
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('deals')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('deals')
        .get();
    }

    const deals: Array<Deal> = [];

    snapshot.forEach(doc => {
      const deal = new Deal();
      deal.id = doc.id;
      deal.active = doc.data().active;
      deal.availableUnits = doc.data().availableUnits;
      deal.categories = doc.data().categories;
      deal.currency = doc.data().currency;
      deal.dateAvailable = doc.data().dateAvailable?.toDate();
      deal.dateCreated = doc.data().dateCreated?.toDate();
      deal.description = doc.data().description;
      deal.video = doc.data().video;
      deal.title = doc.data().title;
      deal.sellingPrice = doc.data().sellingPrice;
      deal.refundPolicy = doc.data().refundPolicy;
      deal.refundDays = doc.data().refundDays;
      deal.profile = doc.data().profile;
      deal.owner = doc.data().owner;
      deal.originalPrice = doc.data().originalPrice;
      deal.organizationId = doc.data().organizationId;
      deal.numberOfUnits = doc.data().numberOfUnits;
      deal.isAvailable = doc.data().isAvailable;
      deal.images = doc.data().images;
      deal.highlight = doc.data().highlight;
      deal.disclaimer = doc.data().disclaimer;
      deal.endDate = doc.data().endDate?.toDate();

      deals.push(deal);
    });

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(deals, HttpStatus.OK));
  }

  findOne(id: number) {
    return `This action returns a #${id} deal`;
  }

  update(id: number, updateDealDto: UpdateDealDto) {
    return `This action updates a #${id} deal`;
  }

  remove(id: number) {
    return `This action removes a #${id} deal`;
  }

  async findCategoryById(id: string): Promise<DealCategory> {
    const snapshot = await firebaseAdmin
      .firestore()
      .doc(`dealCategories/${id}`)
      .get();

    const category = snapshot.data() as DealCategory;

    return category;
  }
}
