export interface DealCategory {
  airtable_id: string;
  name: string;
}
