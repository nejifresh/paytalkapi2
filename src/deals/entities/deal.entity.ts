export class Deal {
  id: string;
  active: boolean;
  availableUnits: number;
  categories: string[];
  currency: string;
  dateAvailable: any;
  dateCreated: any;
  description: string;
  disclaimer: string;
  endDate: any;
  highlight: string;
  images: string[];
  isAvailable: boolean;
  numberOfUnits: number;
  organizationId: string;
  originalPrice: number;
  owner: string;
  profile: string;
  refundDays: number;
  refundPolicy: string;
  sellingPrice: number;
  title: string;
  video: string;
}
