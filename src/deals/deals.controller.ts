import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Res,
  Query,
} from '@nestjs/common';
import { DealsService } from './deals.service';
import { CreateDealDto } from './dto/create-deal.dto';
import { UpdateDealDto } from './dto/update-deal.dto';
import { Response } from 'express';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOperation,
  ApiOkResponse,
} from '@nestjs/swagger';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../auth/auth-guard.service';
import { Deal } from './entities/deal.entity';

@Controller('deals')
@ApiBearerAuth()
@ApiTags('deals')
@UseGuards(AuthGuard)
export class DealsController {
  constructor(private readonly dealsService: DealsService) {}

  @Post()
  create(@Body() createDealDto: CreateDealDto) {
    return this.dealsService.create(createDealDto);
  }

  @ApiOperation({ summary: 'Get Deals' })
  @ApiOkResponse({
    description: 'Get & Filter Deals',
    type: Deal,
  })
  @Get()
  findAll(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ): Promise<any> {
    if (propName && propName.length > 0) {
      const filter = {
        propName,
        propVal,
      };

      return this.dealsService.findAll(response, filter);
    } else {
      return this.dealsService.findAll(response);
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dealsService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateDealDto: UpdateDealDto) {
    return this.dealsService.update(+id, updateDealDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dealsService.remove(+id);
  }

  @Get('/category/:id')
  findCategoryById(@Param('id') id: string) {
    return this.dealsService.findCategoryById(id);
  }
}
