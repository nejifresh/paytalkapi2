import { HttpService, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { CreateParkingDto } from './dto/create-parking.dto';
import { Response } from 'express';
import { adminTypes, firebaseAdmin } from '../commons/firebaseConfig';
import { SuccessResponseDto } from '../commons/dto/successResponse.dto';
import { ParkingInterface } from './entities/parking.interface';
import { IUser } from '../users/interface/user.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ParkingLots } from '../commons/entities/parkingLots.entity';
import { AxiosResponse } from 'axios';
import { ReferenceTime } from './entities/azureTime.interface';
import moment from 'moment-timezone';
import { WalletsService } from '../wallets/wallets.service';
import { ChargeWalletDto } from '../wallets/dto/charge-wallet.dto';
import { GetParkingLotDto } from './dto/get-parkingLot.dto';


@Injectable()
export class ParkingService {

  constructor(@InjectRepository(ParkingLots)
              private parkingRepository: Repository<ParkingLots>,
              private httpService: HttpService,
              private walletService: WalletsService) {
  }

  async create(createParkingDto: CreateParkingDto, response: Response) {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection('users')
      .doc(createParkingDto.userId)
      .get();

    if (!snapshot.exists) {
      throw new NotFoundException('user Id is not valid');
    }

    const user = snapshot.data() as IUser;

    const parkingZones: Array<ParkingLots> = await this.parkingRepository.manager.query(
      `ParkingLotsByZoneNumberAndLocation @0,@1,@2`,
      [createParkingDto.zoneNumber, createParkingDto.latitude, createParkingDto.longitude],
    );

    if (parkingZones.length === 0) {

      throw new NotFoundException('No Parking Zone found is not valid');
    }
    const parkingLot = parkingZones[0];


    let durationMins;

    if (createParkingDto.duration <= parkingLot.MinTimeMins) {
      durationMins = parkingLot.MinTimeMins;
    } else {
      const remainer = createParkingDto.duration % parkingLot.MinTimeMins;

      if (remainer > 0) {
        durationMins = (Math.floor(createParkingDto.duration / parkingLot.MinTimeMins) + 1) * parkingLot.MinTimeMins;
      } else {
        durationMins = (Math.floor(createParkingDto.duration / parkingLot.MinTimeMins)) * parkingLot.MinTimeMins;
      }
    }

    const azureTimeResponse: AxiosResponse<any> = await this.httpService.get(`https://atlas.microsoft.com/timezone/byCoordinates/json?api-version=1.0&query=${createParkingDto.latitude},${createParkingDto.longitude}&subscription-key=${process.env.azureMapsAPIKey}`).toPromise();

    const timeZone = azureTimeResponse.data.TimeZones[0].Id;

    const timeZoneData = azureTimeResponse.data.TimeZones[0].ReferenceTime as ReferenceTime;

    const time = moment.tz(timeZoneData.WallTime, timeZone);

    let amountCharged;

    const parkingSlots = Math.floor(durationMins / parkingLot.MinTimeMins);

    if (!this.isDay(time)) {
      amountCharged = parkingLot.EveningRate * parkingSlots;
    } else if (this.isWeekDay(time)) {
      amountCharged = parkingLot.MonFriDayRate * parkingSlots;
    } else {
      amountCharged = parkingLot.SatSunDayRate * parkingSlots;
    }

    const walletChargeDTO: ChargeWalletDto = {
      userId: createParkingDto.userId,
      amount: amountCharged,
      currency: parkingLot.Currency,
    };

    await this.walletService.chargeUserWallet(walletChargeDTO);

    const parkingRequest: ParkingInterface = {
      amountCharged: amountCharged + 0.30,
      currency: parkingLot.Currency,
      durationMins,
      email: user.email,
      licensePlateNumber: createParkingDto.licensePlate,
      locality: parkingLot.City,
      parkingLotId: parkingLot.ParkingLotID,
      requestDate: adminTypes.firestore.Timestamp.now(),
      requestLocation: new adminTypes.firestore.GeoPoint(createParkingDto.latitude, createParkingDto.longitude),
      status: 'PENDING',
      userId: createParkingDto.userId,
      zoneNumber: parkingLot.ZoneNumber,
    };

    await firebaseAdmin
      .firestore()
      .collection('parkingRequests')
      .add(parkingRequest);

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(parkingRequest, HttpStatus.OK));
  }

  async getParkingLot(getParkingLotDto: GetParkingLotDto, response: Response) {

    const parkingZones: Array<ParkingLots> = await this.parkingRepository.manager.query(
      `ParkingLotsByZoneNumberAndLocation @0,@1,@2`,
      [getParkingLotDto.zoneNumber, getParkingLotDto.latitude, getParkingLotDto.longitude],
    );

    if (parkingZones.length === 0) {

      throw new NotFoundException('No Parking Zone found is not valid');
    }

    const parkingLot = parkingZones[0];

    return response
      .status(HttpStatus.OK)
      .json(new SuccessResponseDto(parkingLot, HttpStatus.OK));
  }

  isWeekDay(date: moment.Moment): boolean {
    return date.day() > 0 && date.day() < 6;
  }

  isDay(date: moment.Moment): boolean {
    return date.hour() > 5 && date.hour() <= 17;
  }
}
