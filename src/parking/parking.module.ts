import { ParkingService } from './parking.service';
import { ParkingController } from './parking.controller';
import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParkingLots } from '../commons/entities/parkingLots.entity';
import { WalletsModule } from '../wallets/wallets.module';

@Module({
  imports: [HttpModule.register({
    headers: {
      'Content-Type': 'application/json',
    },
  }), TypeOrmModule.forFeature([
    ParkingLots,
  ]),
    WalletsModule],
  controllers: [ParkingController],
  providers: [ParkingService],
})
export class ParkingModule {
}
