import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsOptional } from 'class-validator';

export class CreateParkingDto {

  @ApiProperty()
  @IsDefined()
  zoneNumber: string;

  @ApiProperty()
  @IsDefined()
  licensePlate: string;

  @ApiProperty()
  @IsDefined()
  duration: number;

  @ApiProperty()
  @IsDefined()
  userId: string;

  @ApiProperty()
  @IsOptional()
  latitude: number;

  @ApiProperty()
  @IsOptional()
  longitude: number;
}
