import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsOptional } from 'class-validator';

export class GetParkingLotDto {

  @ApiProperty()
  @IsDefined()
  zoneNumber: string;

  @ApiProperty()
  @IsOptional()
  latitude: number;

  @ApiProperty()
  @IsOptional()
  longitude: number;
}
