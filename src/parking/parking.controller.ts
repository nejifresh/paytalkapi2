import { Body, Controller, Post, Res } from '@nestjs/common';
import { ParkingService } from './parking.service';
import { CreateParkingDto } from './dto/create-parking.dto';
import { Response } from 'express';
import { ApiTags } from '@nestjs/swagger';
import { GetParkingLotDto } from './dto/get-parkingLot.dto';


@ApiTags('parking')
@Controller('Parking')
export class ParkingController {
  constructor(private readonly parkingService: ParkingService) {
  }

  @Post()
  create(@Body() createParkingDto: CreateParkingDto, @Res() response: Response) {
    return this.parkingService.create(createParkingDto, response);
  }

  @Post('parking-lot')
  getParkingLot(@Body() getParkingLotDto: GetParkingLotDto, @Res() response: Response) {
    return this.parkingService.getParkingLot(getParkingLotDto, response);
  }

}
