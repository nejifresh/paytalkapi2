import { firestore } from 'firebase-admin';

export interface ParkingLotInterface {
    address: string;
    city: string;
    country: string;
    stateProvince: string;
    capacity: number;
    clearance: number;
    currency: string;
    monFriDayRate: number;
    satSunDayRate: number;
    eveningRate: number;
    minTimeMins: number;
    minWeekDayPurchase: number;
    maxWeekDayPurchase: number;
    minWeekendPurchase: number;
    maxWeekendPurchase: number;
    minEveningPurchase: number;
    maxEveningPurchase: number;
    zoneNumber: string;
    location: firestore.GeoPoint;
    imageUrl: string;
}
