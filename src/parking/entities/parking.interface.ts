import { firestore } from 'firebase-admin';
import Timestamp = firestore.Timestamp;
import GeoPoint = firestore.GeoPoint;

export interface ParkingInterface {

  zoneNumber: string;

  licensePlateNumber: string;

  durationMins: number;

  userId: string;

  requestDate: Timestamp;

  status: string;

  requestLocation: GeoPoint;

  email: string;

  locality: string;

  parkingLotId: string;

  currency: string;

  amountCharged: number;
}
