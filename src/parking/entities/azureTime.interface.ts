export interface ReferenceTime {
  Tag: string;
  StandardOffset: string;
  DaylightSavings: string;
  WallTime: string;
  PosixTzValidYear: string;
  PosixTz: string;
  Sunrise: Date;
  Sunset: Date;
}
