import { TrainingStatus } from './enums/training-status.enum';

export interface TiProfileInfo {
  profileId: string;
  locale: string;
  enrollmentStatus: TrainingStatus;
  createdDateTime: string;
  lastUpdatedDateTime: string;
  enrollmentsCount: number;
  enrollmentsLength: number;
  enrollmentsSpeechLength: number;
  remainingEnrollmentsSpeechLength: number;
  modelVersion: string;
}
