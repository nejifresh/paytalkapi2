export enum TrainingStatus {
  ENROLLING = 'Enrolling',
  TRAINING = 'Training',
  ENROLLED = 'Enrolled',
}
