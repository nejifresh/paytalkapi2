import { Test, TestingModule } from '@nestjs/testing';
import { TextIndependentService } from './text-independent.service';

describe('TextIndependentService', () => {
  let service: TextIndependentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TextIndependentService],
    }).compile();

    service = module.get<TextIndependentService>(TextIndependentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
