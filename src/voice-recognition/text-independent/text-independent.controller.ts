import {
  Controller,
  Get,
  Req,
  Res,
  Post,
  HttpException,
  Param,
  HttpStatus,
} from '@nestjs/common';
import { TextIndependentService } from './text-independent.service';
import { Request, Response } from 'express';
import { AxiosError } from 'axios';
import { UserId } from '../decorators/get-user-id.decorator';
import { FirebaseService } from '../../commons/services/firebase/firebase.service';

// TODO use controller specific exception filter
@Controller('speaker/independent')
export class TextIndependentController {
  constructor(
    private readonly textIndependentService: TextIndependentService,
    private readonly firebaseService: FirebaseService,
  ) {}

  /**
   *
   * @param req Request object
   * @param userId User firestore uid
   * @param res Response Object
   * @returns Response Object
   *
   * Creates user profile and stores
   * created profileId in user document
   */
  @Post('create-profile')
  createProfile(
    @Req() req: Request, // TODO remove
    @UserId() userId: string,
    @Res({ passthrough: true }) res: Response,
  ) {
    // TODO create filter to get UUID from request obj
    // TODO Remove commented code
    // const userId = req['uuid'];
    // const userId = 'jsLKY0RGxnWgTKQN4l0r6B5ATR63';

    const { locale } = req.body;
    let data;

    if (!locale)
      return res.status(400).json({ error: 'locale is not defined' });

    return this.textIndependentService
      .createProfile(locale)
      .then(result => {
        data = result.data;
        const { profileId } = data;

        return this.textIndependentService.saveProfileId(profileId, userId);
      })
      .then(_ => {
        // TODO Add error exception to document write fail
        return res.status(201).json(data);
      })
      .catch((error: AxiosError) => {
        const { status, statusText } = error.response;

        // TODO Add route-specific exceptions handling
        throw new HttpException(statusText, status);
      });
  }

  /**
   *
   * @param req Request bject
   * @param userId User firestore uid
   * @param res Response object
   * @returns Response object
   *
   * Uses user profileId and voice file to
   * enroll user in Azure speaker recognition
   */
  @Post('create-enrollment')
  createEnrollment(
    @Req() req: Request, // TODO remove
    @UserId() userId: string,
    @Res({ passthrough: true }) res: Response,
  ) {
    // TODO Test audio file upload

    const audioFile = req.body;

    return this.firebaseService.getUser(userId).then(user => {
      const { voice_id } = user;

      return this.textIndependentService
        .createEnrollment(voice_id, audioFile)
        .then(result => {
          const { data } = result;
          return res.status(201).json(data);
        })
        .catch((error: AxiosError) => {
          const { status, statusText } = error.response;
          throw new HttpException(statusText, status);
        });
    });
  }

  /**
   *
   * @param req Request object
   * @param userId User firestore uid
   * @param res Response object
   * @returns Response object
   *
   * Uses user profileId and voice file to
   * verify user's identity
   */
  @Post('verify-profile')
  verifyProfile(
    @Req() req: Request,
    @UserId() userId: string,
    @Res({ passthrough: true }) res: Response,
  ) {
    // TODO Test audio file upload

    const audioFile = req.body;

    return this.firebaseService.getUser(userId).then(user => {
      const { voice_id } = user;

      return this.textIndependentService
        .verifyProfile(voice_id, audioFile)
        .then(result => {
          const { data } = result;
          return data;
        })
        .then(data => {
          return res.status(200).json(data);
        })
        .catch((error: AxiosError) => {
          const { status, statusText } = error.response;
          throw new HttpException(statusText, status);
        });
    });
  }

  /**
   *
   * @param locale user language locale
   *
   * Retrieves list of phrases from Azure
   * speaker recognition API
   */
  @Get('list-phrases/:locale')
  listPhrases(@Param('locale') locale: string) {
    console.log(locale);
    if (!locale)
      throw new HttpException('locale is not defined', HttpStatus.BAD_REQUEST);

    return this.textIndependentService
      .getPhrases(locale)
      .then(result => {
        const { data } = result;
        return data;
      })
      .catch((error: AxiosError) => {
        const { status, statusText } = error.response;
        console.log(error);
        throw new HttpException(statusText, status);
      });
  }
}
