import { Test, TestingModule } from '@nestjs/testing';
import { TextIndependentController } from './text-independent.controller';

describe('TextIndependentController', () => {
  let controller: TextIndependentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TextIndependentController],
    }).compile();

    controller = module.get<TextIndependentController>(TextIndependentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
