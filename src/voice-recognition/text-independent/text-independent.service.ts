import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { TiProfileInfo } from '../interfaces/ti-profile-info.interface';
import { firebaseAdmin } from '../../commons/firebaseConfig';

@Injectable()
export class TextIndependentService {
  private readonly baseProfileEndpoint = '/text-independent/profiles';
  private readonly basePhraseEndpoint = `/text-independent/phrases`;

  constructor(private httpService: HttpService) {}

  createProfile(locale: string): Promise<AxiosResponse<TiProfileInfo>> {
    return this.httpService
      .post(this.baseProfileEndpoint, { locale })
      .toPromise();
  }

  saveProfileId(profileId: string, userId: string): Promise<any> {
    const docRef = firebaseAdmin
      .firestore()
      .collection('users')
      .doc(userId);

    return docRef.update({ voice_id: profileId });
  }

  // TODO Move to reuseables
  // async getUser(
  //   userId: string,
  // ): Promise<firebase.default.firestore.DocumentData> {
  //   const docRef = firebaseAdmin
  //     .firestore()
  //     .collection('users')
  //     .doc(userId);

  //   const doc = await docRef.get();
  //   const user = doc.data();

  //   return user;
  // }

  createEnrollment(
    profileId: string,
    audioFile: any,
  ): Promise<AxiosResponse<any>> {
    return this.httpService
      .post(
        `${this.baseProfileEndpoint}/${profileId}/enrollments?ignoreMinLength=true`,
        audioFile,
      )
      .toPromise();
  }

  verifyProfile(
    profileId: string,
    audioFile: any,
  ): Promise<AxiosResponse<any>> {
    return this.httpService
      .post(
        `${this.baseProfileEndpoint}/${profileId}/verify?ignoreMinLength=true`,
        audioFile,
      )
      .toPromise();
  }

  // TODO Investigate and fix error
  getPhrases(locale: string): Promise<AxiosResponse<any>> {
    return this.httpService
      .get(`${this.basePhraseEndpoint}/${locale}`)
      .toPromise();
  }
}
