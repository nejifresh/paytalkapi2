import { Controller } from '@nestjs/common';
import { TextDependentService } from './text-dependent.service';

@Controller('speaker/dependent')
export class TextDependentController {
  constructor(private readonly textDependentService: TextDependentService) {}
}
