import { Test, TestingModule } from '@nestjs/testing';
import { TextDependentController } from './text-dependent.controller';

describe('TextDependentController', () => {
  let controller: TextDependentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TextDependentController],
    }).compile();

    controller = module.get<TextDependentController>(TextDependentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
