import { Test, TestingModule } from '@nestjs/testing';
import { TextDependentService } from './text-dependent.service';

describe('TextDependentService', () => {
  let service: TextDependentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TextDependentService],
    }).compile();

    service = module.get<TextDependentService>(TextDependentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
