import { HttpModule, Module } from '@nestjs/common';
import { TextDependentController } from './text-dependent/text-dependent.controller';
import { TextIndependentController } from './text-independent/text-independent.controller';
import { TextIndependentService } from './text-independent/text-independent.service';
import { TextDependentService } from './text-dependent/text-dependent.service';
import { CommonsModule } from '../commons/commons.module';


@Module({
  imports: [
    HttpModule.register({
      baseURL: process.env.azureBaseURL,
      headers: {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': process.env.azureSubscriptionKey,
      },
    }),
    CommonsModule,
  ],
  controllers: [TextDependentController, TextIndependentController],
  providers: [TextIndependentService, TextDependentService],
})
export class VoiceRecognitionModule {}
