import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Res,
  Query,
  UsePipes,
} from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { Response } from 'express';
import { DTOValidationPipe } from '../commons/pipes/dtoValidation.pipe';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOperation,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import { Reservation } from './entities/reservation.entity';

@Controller('reservations')
@ApiBearerAuth()
@ApiTags('Reservations')
export class ReservationsController {
  constructor(private readonly reservationsService: ReservationsService) {}

  @Post()
  @ApiOperation({ summary: 'Create Reservations' })
  @ApiCreatedResponse({
    description: 'Reservation',
    type: Reservation,
  })
  @UsePipes(DTOValidationPipe)
  create(
    @Body() createReservationDto: CreateReservationDto,
    @Res() response: Response,
  ) {
    return this.reservationsService.create(createReservationDto, response);
  }

  @Get()
  @ApiOperation({
    summary: 'Get All Reservations or Filter by Reservation Attribute',
  })
  @ApiCreatedResponse({
    description: 'Reservation',
    type: Reservation,
  })
  findAll(
    @Res() response: Response,
    @Query('propName') propName?: string,
    @Query('propVal') propVal?: string,
  ) {
    if (propName && propVal) {
      const filter = {
        propName,
        propVal,
      };

      return this.reservationsService.findAll(response, filter);
    } else {
      return this.reservationsService.findAll(response);
    }
  }

  @Get(':reservationCode')
  @ApiOperation({ summary: 'Get Rservation by Reservation Code' })
  @ApiCreatedResponse({
    description: 'Reservation',
    type: Reservation,
  })
  findOne(
    @Param('reservationCode') reservationCode: string,
    @Res() response: Response,
  ) {
    return this.reservationsService.findOne(reservationCode, response);
  }

  @Put('cancel/:reservationCode')
  @ApiOperation({ summary: 'Cancel Reservation' })
  @ApiCreatedResponse({
    description: 'Reservation',
    type: Reservation,
  })
  cancel(
    @Param('reservationCode') reservationCode: string,
    @Res() response: Response,
  ) {
    return this.reservationsService.cancel(reservationCode, response);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reservationsService.remove(+id);
  }
}
