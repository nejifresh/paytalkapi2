import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { UpdateReservationDto } from './dto/update-reservation.dto';
import { firebaseAdmin, adminTypes } from '../commons/firebaseConfig';
import { FirebaseError } from 'firebase-admin';
import { Response } from 'express';
import { Reservation, ReservationStatus } from './entities/reservation.entity';
import cryptoRandomString from 'crypto-random-string';

@Injectable()
export class ReservationsService {

  async create(
    createReservationDto: CreateReservationDto,
    response: Response,
  ): Promise<Response> {
    let reservationCode: string;
    let documentExists: boolean;

    do {
      reservationCode = this._generateReservationCode();

      documentExists = (
        await firebaseAdmin
          .firestore()
          .doc(`reservations/${reservationCode}`)
          .get()
      ).exists;
    } while (documentExists);

    let durationMins;

    if(createReservationDto.duration){

      durationMins = this._convertToMins(createReservationDto.duration);
    }else {

       durationMins = 60
    }


    const reservation: Reservation = {
      customerId: createReservationDto.customerId,
      dateCreated: adminTypes.firestore.Timestamp.fromDate(new Date()),
      dateTimeReserved: adminTypes.firestore.Timestamp.fromDate(
        new Date(createReservationDto.dateTimeReserved),
      ),
      guests: createReservationDto.guests,
      owner: createReservationDto.owner,
      profile: createReservationDto.profile,
      status: ReservationStatus.pending,
      reservationCode,
      duration: durationMins,
    };

    try {
      await firebaseAdmin
        .firestore()
        .doc(`reservations/${reservationCode}`)
        .set(reservation);
    } catch (error) {
      throw new HttpException(error, 500);
    }

    return response.status(HttpStatus.OK).json({ reservationCode });
  }

  async findAll(
    response: Response,
    filter?: { propName: string; propVal: string },
  ): Promise<Response> {
    const data = [];
    let snapshot;

    if (filter) {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('reservations')
        .where(filter.propName, '==', filter.propVal)
        .get();
    } else {
      snapshot = await firebaseAdmin
        .firestore()
        .collection('reservations')
        .get();
    }

    snapshot.docs.map(res => {
      data.push(res.data() as Reservation);
    });

    return response.status(HttpStatus.OK).json(data);
  }

  async findOne(
    reservationCode: string,
    response: Response,
  ): Promise<Response> {
    const snapshot = await firebaseAdmin
      .firestore()
      .collection(`reservations`)
      .where('reservationCode', '==', reservationCode)
      .get();

    const data = snapshot.docs[0].data() as Reservation;

    return response.status(HttpStatus.OK).json(data);
  }

  async cancel(reservationCode: string, response: Response) {
    await firebaseAdmin
      .firestore()
      .doc(`reservations/${reservationCode}`)
      .update({
        status: ReservationStatus.cancelled,
        cancelDate: adminTypes.firestore.Timestamp.fromDate(new Date()),
      });
    return response
      .status(HttpStatus.OK)
      .send({ message: 'Your reservation was successfully cancelled' });
  }

  remove(id: number) {
    return `This action removes a #${id} reservation`;
  }

  private _generateReservationCode() {
    const reservationCode = `RV${cryptoRandomString({
      length: 5,
      type: 'numeric',
    })}`;

    return reservationCode;
  }

  private _convertToMins(duration: { amount: number; unit: string }) {
    if (duration.unit === 'h') {
      return duration.amount * 60;
    } else if (duration.unit === 'dy') {
      return duration.amount * 60 * 24;
    }
  }
}
