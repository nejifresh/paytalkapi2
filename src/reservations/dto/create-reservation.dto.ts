import { IsDefined } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateReservationDto {

  @ApiProperty()
  @IsDefined()
  customerId: string;

  @ApiProperty()
  @IsDefined()
  dateTimeReserved: any;

  @ApiProperty()
  @IsDefined()
  guests: number;

  @ApiProperty()
  @IsDefined()
  owner: string;

  @ApiProperty()
  @IsDefined()
  profile: string;


  @ApiProperty()
  duration: Duration;
}

interface Duration {
  amount: number;
  unit: string;
}
