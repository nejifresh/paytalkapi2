export class Reservation {
  customerId: string;
  dateCreated: firebase.default.firestore.Timestamp;
  dateTimeReserved: firebase.default.firestore.Timestamp;
  guests: number;
  owner: string;
  profile: string;
  reservationCode: string;
  status: ReservationStatus;
  duration: number;
}

export enum ReservationStatus {
  cancelled = 'CANCELLED',
  pending = 'PENDING',
  confirmed = 'CONFIRMED',
  fulfilled = 'FULFILLED',
}
